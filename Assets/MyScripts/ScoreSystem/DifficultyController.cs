﻿using UnityEngine;
using System.Collections;

public class DifficultyController : MonoBehaviour 
{
	public static DifficultyController DifficultySystemScript;

	public int BGColorChangeScore = 200;	// bg color changes after gain this score value

	private bool difficultyUp = false;	// add new colored shapes to a game
//	private int difficultyLevel = 0;
	private int lastDifficultyUpScoreValue = 0;

	public delegate void DifficultyControllerEvents (bool newShapesAdded);
	public static event DifficultyControllerEvents OnDifficultyLevelUp;


	void Awake ()
	{
		DifficultySystemScript = this;
	}


	public void CheckDifficultyUp (int currentGameScore)
	{
		if (currentGameScore - lastDifficultyUpScoreValue >= BGColorChangeScore)
		{
//			++difficultyLevel;
			lastDifficultyUpScoreValue = currentGameScore;

			if (difficultyUp)
			{
				MyGameController.gameController.AddDifficultyLevel ();	// might be passed to event, but should be done before references to prefabs refresh
			}

			if (OnDifficultyLevelUp != null)
			{				
				OnDifficultyLevelUp (difficultyUp);
			}

			difficultyUp = !difficultyUp;
//			Debug.Log ("Difficulty up : " + difficultyLevel);
			// bg color transition
		}
	}
}
