﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ScoreController : MonoBehaviour 
{
	public delegate void ScoreControllerEvent ();
	public static event ScoreControllerEvent OnScoreAdd;

	public static ScoreController ScoreScript;
	public static int WaveNumber;	// waves of new sets of combinations // set from MyGameController.cs

	public int OneLayer3PieceScore = 100;	// score for 3 candies combo
	public int OneLayer4PieceScore = 200;
	public int OneLayer5PieceScore = 400;
	public int OneLayer6PieceScore = 600;
	public int OneLayer7PieceScore = 1000;
	public int OneLayer89PieceScore = 2000;
	public float ThreeAxisMultiplier = 3;
	public float NextComboMultiplier = 1.5f;
	public float NextWaveMultiplier = 1.2f;
	public TotalScoreController TotalScoreText;
//	public int TwoAxisScore;
//	public int ThreeAxisScore;

	private int currentScore = 0;
	private int waveScore = 0;
	private int layerLength;
	private int layerColumnLength;
//	private List <FloatingTextPointController[]> fstLayersPoints; // link from ComboManager.cs

	void Awake () 
	{
		ScoreScript = this;
	}


	void Start ()
	{
		layerLength = ComboManager.LayerLength;
		layerColumnLength = (int) Mathf.Sqrt (layerLength);
//		fstLayersPoints = GetComponent <ComboManager>().LayersFST;

		if (NextComboMultiplier > 1) NextComboMultiplier -= 1;
		if (NextWaveMultiplier > 1) NextWaveMultiplier -= 1;
	}

	// 2nd argument - amount of candies in the combo
	public int AddOneLayerScore (int comboNumber, int candiesCount)
	{
		int waveNumber = WaveNumber;
		int cn = comboNumber;
		int comboScore = 0;

		switch (candiesCount)
		{
		case 3:
			comboScore = OneLayer3PieceScore;
			break;

		case 4:
			comboScore = OneLayer4PieceScore;
			break;

		case 5:
			comboScore = OneLayer5PieceScore;
			break;

		case 6:
			comboScore = OneLayer6PieceScore;
			break;

		case 7:
			comboScore = OneLayer7PieceScore;
			break;

		case 8:
			comboScore = OneLayer89PieceScore;
			break;

		case 9:
			comboScore = OneLayer89PieceScore;
			break;

		default:
			break;
		}

		while (waveNumber > 0)
		{
			comboScore += Mathf.RoundToInt (comboScore * NextWaveMultiplier);
			--waveNumber;
		}

		while (comboNumber > 0)
		{
			comboScore += Mathf.RoundToInt (comboScore * NextComboMultiplier);
			--comboNumber;
		}

		currentScore += comboScore;
//		Debug.Log ("wave" + WaveNumber + " combo" + cn + " Add Single : " + comboScore);
		return comboScore;
	}


	public int AddThreeLayerScore (int comboNumber, int candiesCount)
	{
		int waveNumber = WaveNumber;
		int cn = comboNumber;
		int comboScore = 0;

		switch (candiesCount)
		{
		case 3:
			comboScore = (int) (OneLayer3PieceScore * ThreeAxisMultiplier);
			break;

		case 4:
			comboScore = (int) (OneLayer4PieceScore * ThreeAxisMultiplier);
			break;

		case 5:
			comboScore = (int) (OneLayer5PieceScore * ThreeAxisMultiplier);
			break;

		case 6:
			comboScore = (int) (OneLayer6PieceScore * ThreeAxisMultiplier);
			break;

		case 7:
			comboScore = (int) (OneLayer7PieceScore * ThreeAxisMultiplier);
			break;

		case 8:
			comboScore = (int) (OneLayer89PieceScore * ThreeAxisMultiplier);
			break;

		case 9:
			comboScore = (int) (OneLayer89PieceScore * ThreeAxisMultiplier);
			break;

		default:
			break;
		}

		while (waveNumber > 0)
		{
			comboScore += Mathf.RoundToInt (comboScore * NextWaveMultiplier);
			--waveNumber;
		}

		while (comboNumber > 0)
		{
			comboScore += Mathf.RoundToInt (comboScore * NextComboMultiplier);
			--comboNumber;
		}

		currentScore += comboScore;
//		Debug.Log ("wave" + WaveNumber + " combo" + cn + " Add Triple : " + comboScore);
		return comboScore;
	}

	// count the score for combos
	public void ComboScoreCounter (List <List <int>> ListOfCombinationsToDestroy)
	{
		bool atXfl, atYfl, atXml, atYml, atXbl, atYbl; // at X front layer etc.
		bool singleFL, singleML, singleBL;	// single element in the front layer etc.
		int listIndex; 		// total layer index
		int frontLayerCount;
		int middleLayerCount;
		int backLayerCount;
		int layerIndex;
		int comboIndex = 0;
		int comboNumber = 0;
		int comboScore = 0;
		int randomIndexFloatingScore = 0;
		int randomLayerIndex;

		waveScore = 0;

		while (comboIndex < ListOfCombinationsToDestroy.Count)
		{
			frontLayerCount = ListOfCombinationsToDestroy[comboIndex].Count;
			middleLayerCount = ListOfCombinationsToDestroy[comboIndex + 1].Count;
			backLayerCount = ListOfCombinationsToDestroy[comboIndex + 2].Count;

			if (middleLayerCount == 0)	// single layer combo
			{
//				Debug.Log ("MLC = " + middleLayerCount);
				if (frontLayerCount > 2)	// for sure
				{					
					comboScore = AddOneLayerScore (comboNumber, frontLayerCount);
					randomIndexFloatingScore = Random.Range (0, frontLayerCount); 
					randomIndexFloatingScore = ListOfCombinationsToDestroy[comboIndex][randomIndexFloatingScore];
//					fstLayersPoints[0][randomIndexFloatingScore].Score = comboScore; 
				}
				else if (backLayerCount > 2)
				{
					comboScore = AddOneLayerScore (comboNumber, backLayerCount);
					randomIndexFloatingScore = Random.Range (0, backLayerCount); 
					randomIndexFloatingScore = ListOfCombinationsToDestroy[comboIndex + 2][randomIndexFloatingScore];
//					fstLayersPoints[2][randomIndexFloatingScore].Score = comboScore; 
				}
			}
			else //if (secondLayerCount == 1 && firstLayerCount == 1 && zeroLayerCount == 1)	// 2-3 layers combo
			{
				if (frontLayerCount == 0 && backLayerCount == 0)
				{
					comboScore = AddOneLayerScore (comboNumber, middleLayerCount);
					randomIndexFloatingScore = Random.Range (0, middleLayerCount); 
					randomIndexFloatingScore = ListOfCombinationsToDestroy[comboIndex + 1][randomIndexFloatingScore];
//					fstLayersPoints[1][randomIndexFloatingScore].Score = comboScore;
				}
				else
				{	
					singleFL = singleML = singleBL = atXfl = atXml = atXbl = atYfl = atYml = atYbl = false;
					// detect crosslayer combo: flat or not
					for (layerIndex = 0; layerIndex < 3; ++layerIndex)	// 3 - number of layers
					{
						switch (layerIndex)
						{
						case 0:
							if (frontLayerCount < 2) 
							{
								if (frontLayerCount == 1)
								{
									singleFL = true;
								}
							}
							else
							{
								listIndex = comboIndex + layerIndex;
								atXfl = CheckXAxisInTheLayer (ListOfCombinationsToDestroy[listIndex]);
								atYfl = CheckYAxisInTheLayer (ListOfCombinationsToDestroy[listIndex]);
							}
							break;

						case 1:
							if (middleLayerCount < 2) 
							{
								if (middleLayerCount == 1)
								{
									singleML = true;
								}
							}
							else
							{
								listIndex = comboIndex + layerIndex;
								atXml = CheckXAxisInTheLayer (ListOfCombinationsToDestroy[listIndex]);
								atYml = CheckYAxisInTheLayer (ListOfCombinationsToDestroy[listIndex]);
							}
							break;

						case 2:
							if (backLayerCount < 2) 
							{
								if (backLayerCount == 1)
								{
									singleBL = true;
								}
							}
							else
							{
								listIndex = comboIndex + layerIndex;
								atXbl = CheckXAxisInTheLayer (ListOfCombinationsToDestroy[listIndex]);
								atYbl = CheckYAxisInTheLayer (ListOfCombinationsToDestroy[listIndex]);
							}
							break;
						}
					}
					// TODO check
					if (((singleML || (atXml && !atYml) || (!atXml && atYml)) && ((atXfl && atYfl) || (atXbl && atYbl)) || (atXfl && atYbl) || (atXbl && atYfl)) ||
						((atXml && atYml) && (singleFL || singleBL || atXfl || atXbl || atYfl || atYbl)))
					{
						comboScore = AddThreeLayerScore (comboNumber, frontLayerCount + middleLayerCount + backLayerCount);
						// TOFIX bug // TODO: random index of the layer (it's possible to be 1-3 layers with a combo
						randomLayerIndex = GetRandomNotEmptyLayerIndex (frontLayerCount, backLayerCount);

						switch (randomLayerIndex)
						{
						case 0:
							randomIndexFloatingScore = Random.Range (0, frontLayerCount); 
							break;

						case 1:
							randomIndexFloatingScore = Random.Range (0, middleLayerCount); 
//							randomIndexFloatingScore = ListOfCombinationsToDestroy[comboIndex + 1][randomIndexFloatingScore];
//							fstLayersPoints[1][randomIndexFloatingScore].Score = comboScore;
							break;

						case 2:
							randomIndexFloatingScore = Random.Range (0, backLayerCount); 
							break;
						}
						randomIndexFloatingScore = ListOfCombinationsToDestroy[comboIndex + randomLayerIndex][randomIndexFloatingScore];
//						fstLayersPoints[randomLayerIndex][randomIndexFloatingScore].Score = comboScore;

					}
					else
					{
						// TODO: random index of the layer (it's possible 1-3 layers with a combo pieces
						comboScore = AddOneLayerScore (comboNumber, frontLayerCount + middleLayerCount + backLayerCount);
						randomLayerIndex = GetRandomNotEmptyLayerIndex2D (frontLayerCount, middleLayerCount, backLayerCount);

						switch (randomLayerIndex)
						{
						case 0:
							randomIndexFloatingScore = Random.Range (0, frontLayerCount); 
							break;

						case 1:
							randomIndexFloatingScore = Random.Range (0, middleLayerCount); 
							break;

						case 2:
							randomIndexFloatingScore = Random.Range (0, backLayerCount); 
							break;
						}
						randomIndexFloatingScore = ListOfCombinationsToDestroy[comboIndex + randomLayerIndex][randomIndexFloatingScore];
//						fstLayersPoints[randomLayerIndex][randomIndexFloatingScore].Score = comboScore;

//						if (frontLayerCount > 0)
//						{							
////							comboScore = AddOneLayerScore (comboNumber, frontLayerCount + middleLayerCount + backLayerCount);
//							randomIndexFloatingScore = Random.Range (0, frontLayerCount); 
//							randomIndexFloatingScore = ListOfCombinationsToDestroy[comboIndex][randomIndexFloatingScore];
//							fstLayersPoints[0][randomIndexFloatingScore].Score = comboScore; 
//						}
//						else if (middleLayerCount > 0)
//						{							
////							comboScore = AddOneLayerScore (comboNumber, middleLayerCount);
//							randomIndexFloatingScore = Random.Range (0, middleLayerCount); 
//							randomIndexFloatingScore = ListOfCombinationsToDestroy[comboIndex + 1][randomIndexFloatingScore];
//							fstLayersPoints[1][randomIndexFloatingScore].Score = comboScore;
//						}
//						else if (backLayerCount > 0)
//						{
////							comboScore = AddOneLayerScore (comboNumber, backLayerCount);
//							randomIndexFloatingScore = Random.Range (0, backLayerCount); 
//							randomIndexFloatingScore = ListOfCombinationsToDestroy[comboIndex + 2][randomIndexFloatingScore];
//							fstLayersPoints[2][randomIndexFloatingScore].Score = comboScore; 
//						}
//						else
//						{
//							Debug.Log ("ERROR");
//						}
					}
				}
			}

			waveScore += comboScore;
			++comboNumber;
			comboIndex += 3;
		}

		if (OnScoreAdd != null)
		{
			OnScoreAdd ();
		}

		DifficultyController.DifficultySystemScript.CheckDifficultyUp (currentScore);
		TotalScoreText.AddScore (waveScore);
	}

	// only for 2+ elements in the list
	private bool CheckXAxisInTheLayer (List <int> indexesList)
	{
		bool gotElement = false;
		int indexToCheck;
//		int layerHeight = (int) Mathf.Sqrt (Layer1.Length);

		foreach (int index in indexesList)
		{
			indexToCheck = index - 1;
			if (indexToCheck >= 0  &&  index % layerColumnLength != 0 && indexesList.Contains (indexToCheck))
			{
				gotElement = true;
				break;
			}

			indexToCheck = index + 1;
			if (indexToCheck < layerLength  &&  index % layerColumnLength != 0 && indexesList.Contains (indexToCheck))
			{
				gotElement = true;
				break;
			}				
		}

		return gotElement;
	}

	// only for 2+ elements in the list
	private bool CheckYAxisInTheLayer (List <int> indexesList)
	{
		bool gotElement = false;
		int indexToCheck;
//		int layerHeight = (int) Mathf.Sqrt (Layer1.Length);

		foreach (int index in indexesList)
		{
			indexToCheck = index - layerColumnLength;
			if (indexToCheck >= 0  &&  index % layerColumnLength != 0 && indexesList.Contains (indexToCheck))
			{
				gotElement = true;
				break;
			}

			indexToCheck = index + layerColumnLength;
			if (indexToCheck < layerLength  &&  index % layerColumnLength != 0 && indexesList.Contains (indexToCheck))
			{
				gotElement = true;
				break;
			}				
		}

		return gotElement;
	}

	// 3D combo proof - middleLayer is always contains some elements
	private int GetRandomNotEmptyLayerIndex (int frontLayerCount, int backLayerCount)
	{
		if (frontLayerCount > 0 && backLayerCount > 0)
		{
			return Random.Range (0, 3);
		}
		else if (frontLayerCount > 0)
		{
			return Random.Range (0, 2);
		}
		else
		{
			return (Random.Range (0, 2) + 1);
		}

		return -1; // error
	}

	// 2D combo - 1-3 layers may contain combo pieces
	private int GetRandomNotEmptyLayerIndex2D (int frontLayerCount, int middleLayerCount, int backLayerCount)
	{
		if (frontLayerCount > 0 && backLayerCount > 0)
		{
			return Random.Range (0, 3);
		}
		else if (frontLayerCount > 0 && middleLayerCount > 0)
		{
			return Random.Range (0, 2);
		}
		else if (middleLayerCount > 0 && backLayerCount > 0)
		{
			return (Random.Range (0, 2) + 1);
		}
		else if (frontLayerCount > 0) // for sure
		{
			return 0;
		}
		else if (middleLayerCount > 0) // for sure
		{
			return 1;
		}
		else if (backLayerCount > 0) // for sure
		{
			return 1;
		}

		return -1; // error
	}

	public int GetCurrentScore ()
	{
		return currentScore;
	}
//
//	// 2nd argument - amount of candies in the combo
//	public void AddOneAxisScore (int comboNumber, int candiesCount)
//	{
//		int comboScore;
//
//		while (comboNumber > 0)
//		{
//			comboScore += Mathf.RoundToInt (comboScore * 0.5f);
//			--comboNumber;
//		}
//
//		currentScore += comboScore;
//		Debug.Log ("Add One : " + comboScore);
//	}
//
//
//	public void Add2AxisScore (int comboNumber)
//	{
//		int comboScore = TwoAxisScore;
//
//
//
//		while (comboNumber > 0)
//		{
//			comboScore += Mathf.RoundToInt (comboScore * 0.5f);
//			--comboNumber;
//		}
//		currentScore += comboScore;
//		Debug.Log ("Add 2x : " + comboScore);
//	}
//
//
//	public void Add3AxisScore (int comboNumber)
//	{
//		int comboScore = ThreeAxisScore;
//
//		while (comboNumber > 0)
//		{
//			comboScore += Mathf.RoundToInt (comboScore * 0.5f);
//			--comboNumber;
//		}
//
//		currentScore += comboScore;
//		Debug.Log ("Add 3x : " + comboScore);
//	}
}
