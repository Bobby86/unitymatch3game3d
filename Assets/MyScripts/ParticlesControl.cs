﻿using UnityEngine;
using System.Collections;


public class ParticlesControl : MonoBehaviour 
{
	public float PlaybackSpeed = 1f;

	// Use this for initialization
	void Start () 
	{
		GetComponent <ParticleSystem>().playbackSpeed = PlaybackSpeed;	
	}
}
