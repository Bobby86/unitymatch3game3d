﻿using UnityEngine;
using System.Collections;


public class CameraChangeBGColor : MonoBehaviour 
{
//	public Color32 StartBGColor;
	public float ColorTransitionDuration = 1f;
	public Color32[] DifficultyBGColors;

	private bool changeColor = false;
	private int currentColorIndex = 0;
	private float lerpValue;
	private Color32 currentColor;
	private Camera thisCam;

	void OnEnable ()
	{
		DifficultyController.OnDifficultyLevelUp += ChangeColor;
	}

	void OnDisable ()
	{
		DifficultyController.OnDifficultyLevelUp -= ChangeColor;
	}

	void Start ()
	{
		thisCam = GetComponent <Camera> ();
	}

	void LateUpdate ()
	{
		if (changeColor)
		{
			lerpValue += Time.deltaTime;
			thisCam.backgroundColor = Color.Lerp (thisCam.backgroundColor, currentColor, lerpValue / ColorTransitionDuration);

			if (thisCam.backgroundColor == currentColor)
			{
				changeColor = false;
			}
		}	
	}

	private void ChangeColor (bool newColoredShapeAdded)
	{
		try
		{
			currentColor = DifficultyBGColors[currentColorIndex];
		}
		catch (System.IndexOutOfRangeException ex)
		{
			Debug.Log ("CameraChangeBGColor.cs : BGcolorIndexError");
			return;
		}

		lerpValue = 0;
		changeColor = true;
		currentColorIndex = (++currentColorIndex) % DifficultyBGColors.Length;
	}
}
