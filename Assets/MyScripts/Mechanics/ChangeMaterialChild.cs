﻿using UnityEngine;
using System.Collections;


public class ChangeMaterialChild : MonoBehaviour 
{
	#region Public References
	public static float SpeedOfLightUpMaterial;

	public MeshRenderer CandyRenderer; 
	public Collider ThisCollider;
	public Material NormalMaterial;
	public Material LightenMaterial;
	public Material FadedMaterial;
	public Material OutlineMaterial;
	#endregion

	#region Private References
	private bool lightUp = false;
	private float outlineValue;
	private float lerpValue = 0f;
	#endregion


	#region Unity Callbacks
	void Start () 
	{		
		outlineValue = CandyRenderer.materials[1].GetFloat ("_Outline");	
	}


	void LateUpdate ()
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

		if (lightUp)
		{
			lerpValue += Time.deltaTime;
			CandyRenderer.material.Lerp (CandyRenderer.material, LightenMaterial, SpeedOfLightUpMaterial * Time.deltaTime);
		}
	}
	#endregion


	#region Public Methods
	public void ChangeOpacity (bool toNormal)
	{		
		if (toNormal)
		{
			CandyRenderer.material = NormalMaterial;
			CandyRenderer.materials[1].SetFloat ("_Outline", outlineValue);
			ThisCollider.enabled = true;
		}
		else
		{
			CandyRenderer.material = FadedMaterial;
			CandyRenderer.materials[1].SetFloat ("_Outline", 0f); 
			ThisCollider.enabled = false;
		}
	}


	public void LightUp ()
	{
		if (CandyRenderer.material != NormalMaterial)
		{
			CandyRenderer.material = NormalMaterial;
		}

		lerpValue = 0f;
		//		timeAtStart = Time.time;
		lightUp = true;
//		LightParticles.SetActive (true);
	}
	#endregion
}
