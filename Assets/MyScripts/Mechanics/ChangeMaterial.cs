﻿using UnityEngine;
using System.Collections;


public class ChangeMaterial : MonoBehaviour 
{
	#region Public References
	public static float SpeedOfLightUpMaterial;
	public static float AlphaChangeDuration;
//	public float TimeOfSpawn = 0.2f;
//	public float TimeOfDespawn = 0.3f;
//	public float TimeOfInlight = 0.3f;
//	public float InlightSpeed = 5f;
	public float MinAlphaValue = 0.2f;
	public MeshRenderer CandyRenderer;
	public Collider ThisCollider;
	public Material NormalMaterial;
//	public Material LightenMaterial;
//	public Material FadedMaterial;
//	public Material OutlineMaterial;
//	public Material SpawnMaterial;
//	public Material DestroyMaterial;
	public GameObject LightParticles;
//	public ChangeMaterialChild[] ChildrenMatScripts;
//	public GameObject DeathParticles;
	#endregion


	#region Private References
	private bool destroy = false;
//	private bool child = true;
	private bool fadeOut = false;
	private bool fadeIn = false;
//	private bool fakeThere = false;
//	private bool fakeBack = false;
	private bool lightUp = false;
//	private bool spawn = false;
//	private float timeOfDespawn;
//	private float timeOfInlight;
//	private float outlineValue;
	private float lerpValue = 0f;
	private float stepAlphaChange;
	private float alphaFakeChangeDuration;
	private float alphaChangeCurrentDuration;
//	private Collider childCollider;
//	private float timeAtStart;
	private Color transparent;
	private Color normalColor;
	private Color changeToColor;
//	private Color currentColor;
	#endregion


	#region Unity Callbacks
	void Awake ()
	{
		CandyRenderer.material.color = NormalMaterial.color;
		normalColor = NormalMaterial.color;
		transparent = NormalMaterial.color;
		normalColor.a = 1f;
		transparent.a = MinAlphaValue;
		//		normalColor = CandyRenderer.material.color;
//		transparent = CandyRenderer.material.color;
//		transparent.a = MinAlphaValue;
//		CandyRenderer.material.color = transparent;
	}


//	void Start ()
//	{
//		timeOfDespawn = MyGameController.TimeOfDespawnCandy;
//		timeOfInlight = MyGameController.TimeOfInlightCandy;

//		CandyRenderer.material.color = transparent;

//		transparent = LightenMaterial.color;
//		if (gameObject.name.Contains ("Cube"))
//		{
//			child = false;
//		}
//		else
//		{
//			childCollider = GetComponent <Collider> ();	
//		}

//		outlineValue = CandyRenderer.materials[1].GetFloat ("_Outline");
//		outlineMat = CandyRenderer.materials[1];
//	}


	void FixedUpdate ()
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

		if (fadeOut || fadeIn)
		{
			lerpValue += Time.fixedDeltaTime;
			stepAlphaChange = lerpValue / alphaChangeCurrentDuration;
//			CandyRenderer.material.Lerp (CandyRenderer.material, FadedMaterial, SpeedOfLightUpMaterial * Time.deltaTime);
			CandyRenderer.material.color = Color.Lerp (CandyRenderer.material.color, changeToColor, stepAlphaChange);//SpeedOfLightUpMaterial * lerpValue);
		}
	}


	void LateUpdate ()
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

		if (fadeOut)
		{
			if (CandyRenderer.material.color.a <= MinAlphaValue)
			{
				CandyRenderer.material.color = transparent;

				if (lightUp)
				{					
					this.enabled = false;
				}

				if (destroy)
				{
					Destroy (gameObject);
				}
//				if (fakeThere || fakeBack)
//				{
//					ChangeOpacity (true);
//				}

				fadeOut = false; //TOFIX new variable
			}
//			if (Time.time - timeAtStart > timeOfInlight)
//			{
//				Destroy (gameObject);
//			}
		}
		else if (fadeIn)
		{
			if (CandyRenderer.material.color.a >= normalColor.a)
			{
				CandyRenderer.material.color = normalColor;

				if (lightUp)
				{
					this.enabled = false;
				}

//				if (fakeThere)
//				{
//					fakeThere = false;
//					fakeBack = true;
//					ChangeOpacity (false);
//				}
//				else if (fakeBack)
//				{
//					fakeBack = false;
//					alphaChangeCurrentDuration = AlphaChangeDuration;
//				}

				ThisCollider.enabled = true;
				fadeIn = false; //TOFIX new variable
			}
		}
//		if (destroy)
//		{
//			lerpValue += Time.deltaTime;
//		CandyRenderer.material.color = Color.Lerp (CandyRenderer.material.color, transparent, SpeedOfLightUpMaterial * lerpValue);
////			Debug.Log (CandyRenderer.material.color.a);
//		}

//		if (spawn)
//		{
//			lerpValue += Time.deltaTime;
//			CandyRenderer.material.color = Color.Lerp (CandyRenderer.material.color, NormalMaterial.color, TimeOfSpawn * lerpValue);
//
//			if (CandyRenderer.material.color.a == 1)
//			{
//				spawn = false;
//				CandyRenderer.material = NormalMaterial;
//			}
//		}
	}
	#endregion

//	public void DestroyCandy ()
//	{
//		Destroy (gameObject);
//	}
//
//
//	public void DestroyCandy (float delay)
//	{
//		Destroy (gameObject, delay);
//	}

	#region Public Methods
	// 3D layouts model // change opacity of a child by the axis
	public void ChangeOpacity (bool toNormal, bool speedUpToFakeSwap = false)//, int axis)
	{			
		//		ChildrenMatScripts[axis].ChangeOpacity (toNormal);
//		if (!(fakeBack || fakeThere))
//		{
//			alphaChangeCurrentDuration = AlphaChangeDuration;	
//		}
		if (speedUpToFakeSwap)
		{
			alphaChangeCurrentDuration = AlphaChangeDuration / 10;
		}
		else
		{
			alphaChangeCurrentDuration = AlphaChangeDuration;
		}

		lerpValue = 0f;

		if (toNormal)
		{
//			CandyRenderer.material = NormalMaterial;
//			CandyRenderer.materials[1].SetFloat ("_Outline", outlineValue);
//			CandyRenderer.material.color = normalColor;
			changeToColor = normalColor;
			fadeOut = false;
			fadeIn = true;
//			ThisCollider.enabled = true;
		}
		else
		{
//			CandyRenderer.material = FadedMaterial;
//			CandyRenderer.materials[1].SetFloat ("_Outline", 0f); 
//			CandyRenderer.material.color = transparent;
			changeToColor = transparent;
			fadeIn = false;
			ThisCollider.enabled = false;
			fadeOut = true;
		}
	}


	public void DeleteThis ()
	{
		lerpValue = 0f;

		changeToColor = transparent;
		fadeIn = false;
		ThisCollider.enabled = false;
		fadeOut = true;
		destroy = true;
	}
//	public void FakeSwapAlphaChanges ()
//	{
//		alphaChangeCurrentDuration = AlphaChangeDuration / 3;
//		fakeThere = true;
//		ChangeOpacity (false);
//	}

	// Destroy a candy with effects
//	public void DespawnEffect ()
//	{
////		transform.parent = transform.parent.parent;
//		Destroy (gameObject, timeOfDespawn + 0.1f);
////		spawn = false;
////		CandyRenderer.material = DestroyMaterial;
//		lerpValue = 0f;
//		destroy = true;
//	}


	public void LightUp ()
	{		
		if (CandyRenderer.material!= NormalMaterial)
		{
			CandyRenderer.material = NormalMaterial;
		}

//		ChildrenMatScripts[0].LightUp ();
//		ChildrenMatScripts[1].LightUp ();
//		ChildrenMatScripts[2].LightUp ();

		lerpValue = 0f;
//		timeAtStart = Time.time;
		lightUp = true;
		LightParticles.SetActive (true);
		LightParticles.transform.parent = transform.root;
//		ParticleSystem lightPS = LightParticles.GetComponent <ParticleSystem> ();
		destroy = true;

//		if (lightPS.isStopped)
//		{
//			lightPS.Play ();
//		}
	}

//	public void SpawnEffect ()
//	{
//		spawn = true;
//	}

//	void OnDestroy ()
//	{
//		DeathParticles.SetActive (true);
//		DeathParticles.GetComponent <DestroyParticles> ().enabled = true;
//		DeathParticles.transform.parent = transform.root;
//	}
	#endregion
}
