﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class CandyPoints2DLayoutModel : MonoBehaviour 
{
	// current active layer (0-3) (camera) in the certain axis
	// variables will change from InputController
	public static int X1CurrentLayer;
	public static int YICurrentLayer;
	public static int ZACurrentLayer;

	[HideInInspector] public int XLayerLength;
	[HideInInspector] public int YLayerLength;
	[HideInInspector] public int ZLayerLength;

//	public float CandySpawnDelay = 0.2f; // each candy spawn delay (.1f)
//	public float MoveDownLayerDelay = 0.1f;	// affect MoveLikeTetris ()


	public Transform[] Layer1;
	public Transform[] Layer2;
	public Transform[] Layer3;

	//old part

	// D-B-C-A
	public Transform[] X1Layer;	
	public Transform[] X2Layer;	
	public Transform[] X3Layer;	
//	public Transform[] X4Layer;	

	// 1-2-3-4
	public Transform[] AZ1Layer;	
	public Transform[] BZ2Layer;	
	public Transform[] CZ3Layer;	
//	public Transform[] DZ4Layer;	

	// IV-III-II-I
	public Transform[] IY1Layer;	
	public Transform[] IIY2Layer;	
	public Transform[] IIIY3Layer;	
//	public Transform[] IVY4Layer;	

	// storage of each axis layers
	public List <Transform[]> X1Layers = new List <Transform[]> ();
	public List <Transform[]> YILayers = new List <Transform[]> ();
	public List <Transform[]> ZALayers = new List <Transform[]> ();

	private int layerLength; // length of current strings/columns of the layer
//	private int candiesLength; // amount of available candies
//	private int[,] candiesTypesTable;
//	private List <int> candiesTypesList = new List<int> ();
//	private List <int> indexesOfCandiesToDestroy = new List <int> ();
//	private LayersButtonsLogic camerasStorageButtonsScript;
//	private ChangeMaterial[] X1ChangeMat;
//	private ChangeMaterial[] X2ChangeMat;
//	private ChangeMaterial[] X3ChangeMat;
//	private ChangeMaterial[] AZ1ChangeMat;
//	private ChangeMaterial[] BZ2ChangeMat;
//	private ChangeMaterial[] CZ3ChangeMat;
//	private ChangeMaterial[] IY1ChangeMat;
//	private ChangeMaterial[] IIY2ChangeMat;
//	private ChangeMaterial[] IIIY3ChangeMat;
//
//	private List <ChangeMaterial[]> X1ChngeMatScripts = new List<ChangeMaterial[]> ();
//	private List <ChangeMaterial[]> YIChngeMatScripts = new List<ChangeMaterial[]> ();
//	private List <ChangeMaterial[]> ZAChngeMatScripts = new List<ChangeMaterial[]> ();


	void Awake ()
	{
		X1CurrentLayer = 0;
		YICurrentLayer = 0;
		ZACurrentLayer = 0;

		XLayerLength = (int) Mathf.Sqrt (X1Layer.Length);
		YLayerLength = (int) Mathf.Sqrt (IY1Layer.Length);
		ZLayerLength = (int) Mathf.Sqrt (AZ1Layer.Length);
	}


	void Start ()
	{
		// wrong numeration (X, Y layers) in the editor
//		X1Layers.Add (X4Layer);
		X1Layers.Add (X3Layer);
		X1Layers.Add (X2Layer);
		X1Layers.Add (X1Layer);
//		YILayers.Add (IVY4Layer);
		YILayers.Add (IIIY3Layer);
		YILayers.Add (IIY2Layer);
		YILayers.Add (IY1Layer);
		ZALayers.Add (AZ1Layer);
		ZALayers.Add (BZ2Layer);
		ZALayers.Add (CZ3Layer);
//		ZALayers.Add (DZ4Layer);

//		candiesLength = GameObject.FindWithTag ("GameController").GetComponent <MyGameController> ().CandiesPrefabs.Length;
//		camerasStorageButtonsScript = GetComponent <LayersButtonsLogic> ();

//		DragScript.candiesStorage = this;

//		for (int i = 0; i < 3; ++i)
//		{
//			MyGameController.gameController.TurnOnButton (i, 0);
//		}

		// warning: for the same layers length only
//		X1ChangeMat = InitMaterialScriptsArrays (X1Layer);
//		X2ChangeMat = InitMaterialScriptsArrays (X2Layer);
//		X3ChangeMat = InitMaterialScriptsArrays (X3Layer);
//		AZ1ChangeMat = InitMaterialScriptsArrays (AZ1Layer);
//		BZ2ChangeMat = InitMaterialScriptsArrays (BZ2Layer);
//		CZ3ChangeMat = InitMaterialScriptsArrays (CZ3Layer);
//		IY1ChangeMat = InitMaterialScriptsArrays (IY1Layer);
//		IIY2ChangeMat = InitMaterialScriptsArrays (IIY2Layer);
//		IIIY3ChangeMat = InitMaterialScriptsArrays (IIIY3Layer);
//
//		X1ChngeMatScripts.Add (X3ChangeMat);
//		X1ChngeMatScripts.Add (X2ChangeMat);
//		X1ChngeMatScripts.Add (X1ChangeMat);
//		YIChngeMatScripts.Add (IIIY3ChangeMat);
//		YIChngeMatScripts.Add (IIY2ChangeMat);
//		YIChngeMatScripts.Add (IY1ChangeMat);
//		ZAChngeMatScripts.Add (AZ1ChangeMat);
//		ZAChngeMatScripts.Add (BZ2ChangeMat);
//		ZAChngeMatScripts.Add (CZ3ChangeMat);
	}


//	private ChangeMaterial[] InitMaterialScriptsArrays (Transform[] layer)
//	{
//		ChangeMaterial[] matScripts = new ChangeMaterial[layer.Length];
//
//		for (int pointIndex = 0; pointIndex < layer.Length; ++pointIndex)
//		{
//			matScripts[pointIndex] = layer[pointIndex].GetChild (0).GetComponent <ChangeMaterial> ();
//		}
//
//		return matScripts;
//	}

	// axisNumber (0-2) : 0 - X, 2 - Y, 3 - Z 
	// determine a layer and run execution
	// works only for 3 layer for each axis
//	public bool ExecuteLayer (int axisNumber, int layerNuber)
//	{
//		bool returnValue = false;	// true if something was destroyed
////		int layerNumber01 = (layerNuber + 1) % 3;
////		int layerNumber02 = (layerNuber + 2) % 3;
//		int firstNextLayerIndex = 0;
//		int secondNextLayerIndex = 0;
//		List<int> parentLayer01CandiesToDestroy;
//		List<int> parentLayer02CandiesToDestroy;
//		List<int> indexesOfCandiesToDestroy =  new List<int>();
//		List<int> neighbourLayer01CandiesToDestroy = new List<int>();
//		List<int> neighbourLayer02CandiesToDestroy = new List<int>();
//		List<int> tmpList;
//
//		switch (axisNumber)
//		{
//		case 0:
//			layerLength = XLayerLength;
////			candiesTypesTable = InitCandiesTypesTable (X1Layers[layerNuber]);
//			FillCandiesTypesList (X1Layers[layerNuber]);
//
//			switch (layerNuber)
//			{
//			case 0:
//				for (int candyType = 0; candyType < candiesLength; ++candyType)
//				{
//					tmpList = FillDestroyList4MainLayer (candyType, indexesOfCandiesToDestroy);
//
//					if (tmpList.Count > 2)
//					{
//						tmpList = CheckNeighbourLayer (candyType, X1Layers[1], tmpList);
//
//						if (tmpList.Count > 0)
//						{
//							neighbourLayer01CandiesToDestroy.AddRange (tmpList);
//							tmpList = CheckNeighbourLayer (candyType, X1Layers[2], tmpList);
//
//							if (tmpList.Count > 0)
//							{
//								neighbourLayer02CandiesToDestroy.AddRange (tmpList);
//							}
//						}
//					}
//				}
//				firstNextLayerIndex = 1;
//				secondNextLayerIndex = 2;
//				break;
//
//			case 1:
//				List<int> tmpList2;
//
//				for (int candyType = 0; candyType < candiesLength; ++candyType)
//				{
//					tmpList = FillDestroyList4MainLayer (candyType, indexesOfCandiesToDestroy);
//
//					if (tmpList.Count > 2)
//					{
//						tmpList2 = CheckNeighbourLayer (candyType, X1Layers[0], tmpList);
//
//						if (tmpList2.Count > 0)
//						{
//							neighbourLayer01CandiesToDestroy.AddRange (tmpList2);
//						}
//
//						tmpList2 = CheckNeighbourLayer (candyType, X1Layers[2], tmpList);
//
//						if (tmpList2.Count > 0)
//						{
//							neighbourLayer02CandiesToDestroy.AddRange (tmpList2);
//						}
//					}
//				}
//				firstNextLayerIndex = 0;
//				secondNextLayerIndex = 2;
//				break;
//
//			case 2:
//				for (int candyType = 0; candyType < candiesLength; ++candyType)
//				{
//					tmpList = FillDestroyList4MainLayer (candyType, indexesOfCandiesToDestroy);
//
//					if (tmpList.Count > 2)
//					{
//						tmpList = CheckNeighbourLayer (candyType, X1Layers[1], tmpList);
//
//						if (tmpList.Count > 0)
//						{
//							neighbourLayer01CandiesToDestroy.AddRange (tmpList);
//							tmpList = CheckNeighbourLayer (candyType, X1Layers[0], tmpList);
//
//							if (tmpList.Count > 0)
//							{
//								neighbourLayer02CandiesToDestroy.AddRange (tmpList);
//							}
//						}
//					}
//				}
//				firstNextLayerIndex = 1;
//				secondNextLayerIndex = 0;
//				break;
//			}
//			//				FillRemoveList ();
//
//			if (indexesOfCandiesToDestroy.Count > 2)
//			{
//				ChangeLayersOpacity (0, layerNuber);
//
////				if (layerNuber != X1CurrentLayer)
////				{
////					camerasStorageButtonsScript.XLayerJump (layerNuber);
////				}
//
//				for (int i = 0; i < indexesOfCandiesToDestroy.Count; ++i)
//				{
//					X1Layers[layerNuber][indexesOfCandiesToDestroy[i]].GetComponent <CandyPoint> ().DespawnCandy ();
//					StartCoroutine (MoveLikeTetris (-1, -1, layerLength, MoveDownLayerDelay, X1Layers[layerNuber], new Vector3(0f, -7f, 0f)));
//				}
//				for (int i = 0; i < neighbourLayer01CandiesToDestroy.Count; ++i)
//				{
//					X1Layers[firstNextLayerIndex][neighbourLayer01CandiesToDestroy[i]].GetComponent <CandyPoint> ().DespawnCandy ();
//					StartCoroutine (MoveLikeTetris (-1, -1, layerLength, MoveDownLayerDelay, X1Layers[firstNextLayerIndex], new Vector3(0f, -7f, 0f)));
//				}
//				for (int i = 0; i < neighbourLayer02CandiesToDestroy.Count; ++i)
//				{
//					X1Layers[secondNextLayerIndex][neighbourLayer02CandiesToDestroy[i]].GetComponent <CandyPoint> ().DespawnCandy ();
//					StartCoroutine (MoveLikeTetris (-1, -1, layerLength, MoveDownLayerDelay, X1Layers[secondNextLayerIndex], new Vector3(0f, -7f, 0f)));
//				}
//
//				returnValue = true;
////				StartCoroutine (MoveLikeTetris (-1, -1, layerLength, MoveDownLayerDelay, X1Layers[layerNuber], new Vector3(0f, -7f, 0f)));
//			}
//			break;
//
//		case 1:
//			layerLength = YLayerLength;
////			candiesTypesTable = InitCandiesTypesTable (YILayers[layerNuber]);
//			FillCandiesTypesList (YILayers[layerNuber]);
//
//			switch (layerNuber)
//			{
//			case 0:
//				for (int candyType = 0; candyType < candiesLength; ++candyType)
//				{
//					tmpList = FillDestroyList4MainLayer (candyType, indexesOfCandiesToDestroy);
//
//					if (tmpList.Count > 2)
//					{
//						tmpList = CheckNeighbourLayer (candyType, YILayers[1], tmpList);
//
//						if (tmpList.Count > 0)
//						{
//							neighbourLayer01CandiesToDestroy.AddRange (tmpList);
//							tmpList = CheckNeighbourLayer (candyType, YILayers[2], tmpList);
//
//							if (tmpList.Count > 0)
//							{
//								neighbourLayer02CandiesToDestroy.AddRange (tmpList);
//							}
//						}
//					}
//				}
//				firstNextLayerIndex = 1;
//				secondNextLayerIndex = 2;
//				break;
//
//			case 1:
//				List<int> tmpList2;
//
//				for (int candyType = 0; candyType < candiesLength; ++candyType)
//				{
//					tmpList = FillDestroyList4MainLayer (candyType, indexesOfCandiesToDestroy);
//
//					if (tmpList.Count > 2)
//					{
//						tmpList2 = CheckNeighbourLayer (candyType, YILayers[0], tmpList);
//
//						if (tmpList2.Count > 0)
//						{
//							neighbourLayer01CandiesToDestroy.AddRange (tmpList2);
//						}
//
//						tmpList2 = CheckNeighbourLayer (candyType, YILayers[2], tmpList);
//
//						if (tmpList2.Count > 0)
//						{
//							neighbourLayer02CandiesToDestroy.AddRange (tmpList2);
//						}
//					}
//				}
//				firstNextLayerIndex = 0;
//				secondNextLayerIndex = 2;
//				break;
//
//			case 2:
//				for (int candyType = 0; candyType < candiesLength; ++candyType)
//				{
//					tmpList = FillDestroyList4MainLayer (candyType, indexesOfCandiesToDestroy);
//
//					if (tmpList.Count > 2)
//					{
//						tmpList = CheckNeighbourLayer (candyType, YILayers[1], tmpList);
//
//						if (tmpList.Count > 0)
//						{
//							neighbourLayer01CandiesToDestroy.AddRange (tmpList);
//							tmpList = CheckNeighbourLayer (candyType, YILayers[0], tmpList);
//
//							if (tmpList.Count > 0)
//							{
//								neighbourLayer02CandiesToDestroy.AddRange (tmpList);
//							}
//						}
//					}
//				}
//				firstNextLayerIndex = 1;
//				secondNextLayerIndex = 0;
//				break;
//			}
////			for (int candyType = 0; candyType < candiesLength; ++candyType)
////			{			
////				FillDestroyList4MainLayer (candyType);
////			}
//			//				FillRemoveList ();
//
//			if (indexesOfCandiesToDestroy.Count > 2)
//			{
//				ChangeLayersOpacity (1, layerNuber);
//
//				for (int i = 0; i < indexesOfCandiesToDestroy.Count; ++i)
//				{
//					YILayers[layerNuber][indexesOfCandiesToDestroy[i]].GetComponent <CandyPoint> ().DespawnCandy ();
////					StartCoroutine (MoveLikeTetris (-1, -1, layerLength, MoveDownLayerDelay, X1Layers[layerNuber], new Vector3(0f, -7f, 0f)));
//				}
//				for (int i = 0; i < neighbourLayer01CandiesToDestroy.Count; ++i)
//				{
//					YILayers[firstNextLayerIndex][neighbourLayer01CandiesToDestroy[i]].GetComponent <CandyPoint> ().DespawnCandy ();
////					StartCoroutine (MoveLikeTetris (-1, -1, layerLength, MoveDownLayerDelay, X1Layers[firstNextLayerIndex], new Vector3(0f, -7f, 0f)));
//				}
//				for (int i = 0; i < neighbourLayer02CandiesToDestroy.Count; ++i)
//				{
//					YILayers[secondNextLayerIndex][neighbourLayer02CandiesToDestroy[i]].GetComponent <CandyPoint> ().DespawnCandy ();
////					StartCoroutine (MoveLikeTetris (-1, -1, layerLength, MoveDownLayerDelay, X1Layers[secondNextLayerIndex], new Vector3(0f, -7f, 0f)));
//				}
//
////				if (layerNuber != YICurrentLayer)
////				{
////					camerasStorageButtonsScript.YLayerJump (layerNuber);
////				}
//
////				for (int i = 0; i < indexesOfCandiesToDestroy.Count; ++i)
////				{					
////					YILayers[layerNuber][indexesOfCandiesToDestroy[i]].GetComponent <CandyPoint> ().DespawnCandy ();
////				}
//				returnValue = true;
////
//				for (int i = 0; i < X1Layers.Count - 1; ++i)	// move down from top of 3d view cube
//				{
//					StartCoroutine (MoveLikeTetris (-1, layerNuber, XLayerLength, MoveDownLayerDelay, X1Layers[i], new Vector3(0f, -7f, 0f)));
//				}
//				StartCoroutine (MoveLikeTetris (1, layerNuber, XLayerLength, MoveDownLayerDelay, X1Layers[X1Layers.Count - 1], new Vector3(0f, -7f, 0f)));
////				StartCoroutine (MoveLikeTetris (layerLength, .1f, YILayers[layerNuber], new Vector3(0f, 0f, -7f)));
//			}
//			break;
//
//		case 2:
//			layerLength = ZLayerLength;
////			candiesTypesTable = InitCandiesTypesTable (ZALayers[layerNuber]);
//			FillCandiesTypesList (ZALayers[layerNuber]);
//
//			switch (layerNuber)
//			{
//			case 0:
//				for (int candyType = 0; candyType < candiesLength; ++candyType)
//				{
//					tmpList = FillDestroyList4MainLayer (candyType, indexesOfCandiesToDestroy);
//
//					if (tmpList.Count > 2)
//					{
//						tmpList = CheckNeighbourLayer (candyType, ZALayers[1], tmpList);
//
//						if (tmpList.Count > 0)
//						{
//							neighbourLayer01CandiesToDestroy.AddRange (tmpList);
//							tmpList = CheckNeighbourLayer (candyType, ZALayers[2], tmpList);
//
//							if (tmpList.Count > 0)
//							{
//								neighbourLayer02CandiesToDestroy.AddRange (tmpList);
//							}
//						}
//					}
//				}
//				firstNextLayerIndex = 1;
//				secondNextLayerIndex = 2;
//				break;
//
//			case 1:
//				List<int> tmpList2;
//
//				for (int candyType = 0; candyType < candiesLength; ++candyType)
//				{
//					tmpList = FillDestroyList4MainLayer (candyType, indexesOfCandiesToDestroy);
//
//					if (tmpList.Count > 2)
//					{
//						tmpList2 = CheckNeighbourLayer (candyType, ZALayers[0], tmpList);
//
//						if (tmpList2.Count > 0)
//						{
//							neighbourLayer01CandiesToDestroy.AddRange (tmpList2);
//						}
//
//						tmpList2 = CheckNeighbourLayer (candyType, ZALayers[2], tmpList);
//
//						if (tmpList2.Count > 0)
//						{
//							neighbourLayer02CandiesToDestroy.AddRange (tmpList2);
//						}
//					}
//				}
//				firstNextLayerIndex = 0;
//				secondNextLayerIndex = 2;
//				break;
//
//			case 2:
//				for (int candyType = 0; candyType < candiesLength; ++candyType)
//				{
//					tmpList = FillDestroyList4MainLayer (candyType, indexesOfCandiesToDestroy);
//
//					if (tmpList.Count > 2)
//					{
//						tmpList = CheckNeighbourLayer (candyType, ZALayers[1], tmpList);
//
//						if (tmpList.Count > 0)
//						{
//							neighbourLayer01CandiesToDestroy.AddRange (tmpList);
//							tmpList = CheckNeighbourLayer (candyType, ZALayers[0], tmpList);
//
//							if (tmpList.Count > 0)
//							{
//								neighbourLayer02CandiesToDestroy.AddRange (tmpList);
//							}
//						}
//					}
//				}
//				firstNextLayerIndex = 1;
//				secondNextLayerIndex = 0;
//				break;
//			}
////			for (int candyType = 0; candyType < candiesLength; ++candyType)
////			{
////				FillDestroyList4MainLayer (candyType);
////			}
//			//				FillRemoveList ();
//
//			if (indexesOfCandiesToDestroy.Count > 2)
//			{
//				ChangeLayersOpacity (2, layerNuber);
//
////				if (layerNuber != ZACurrentLayer)
////				{					
////					camerasStorageButtonsScript.ZLayerJump (layerNuber);
////				}
//				for (int i = 0; i < indexesOfCandiesToDestroy.Count; ++i)
//				{
//					ZALayers[layerNuber][indexesOfCandiesToDestroy[i]].GetComponent <CandyPoint> ().DespawnCandy ();
//					StartCoroutine (MoveLikeTetris (-1, -1, layerLength, MoveDownLayerDelay, ZALayers[layerNuber], new Vector3(0f, -7f, 0f)));
//				}
//				for (int i = 0; i < neighbourLayer01CandiesToDestroy.Count; ++i)
//				{
//					ZALayers[firstNextLayerIndex][neighbourLayer01CandiesToDestroy[i]].GetComponent <CandyPoint> ().DespawnCandy ();
//					StartCoroutine (MoveLikeTetris (-1, -1, layerLength, MoveDownLayerDelay, ZALayers[firstNextLayerIndex], new Vector3(0f, -7f, 0f)));
//				}
//				for (int i = 0; i < neighbourLayer02CandiesToDestroy.Count; ++i)
//				{
//					ZALayers[secondNextLayerIndex][neighbourLayer02CandiesToDestroy[i]].GetComponent <CandyPoint> ().DespawnCandy ();
//					StartCoroutine (MoveLikeTetris (-1, -1, layerLength, MoveDownLayerDelay, ZALayers[secondNextLayerIndex], new Vector3(0f, -7f, 0f)));
//				}
//
//				returnValue = true;
////				for (int i = 0; i < indexesOfCandiesToDestroy.Count; ++i)
////				{
////					ZALayers[layerNuber][indexesOfCandiesToDestroy[i]].GetComponent <CandyPoint> ().DespawnCandy ();
////				}
////				returnValue = true;
////				StartCoroutine (MoveLikeTetris (-1, -1, layerLength, MoveDownLayerDelay, ZALayers[layerNuber], new Vector3(0f, -7f, 0f)));
//			}
//			break;
//
//		default:
//			break;
//		}
//
//		indexesOfCandiesToDestroy.Clear ();
//
//		return returnValue;
//	}
//
//	// L-mechanics
//	private List<int> FillDestroyList4MainLayer (int candyType, List<int> indexesOfCandiesToDestroy)
//	{
//		int item;
//		int currentItemIndex = 0;	// in currentCheck
//		int itemStorage;
//		int addCounter; 
//		List <int> indexesToCheck = new List<int>();
//		List <int> currentCheck = new List<int>();
//		List <int> listToCheckNeighbourLayer = new List<int>();
//
//		for (int i = 0; i < candiesTypesList.Count; ++i)
//		{
//			if (candyType == candiesTypesList[i])
//			{
//				indexesToCheck.Add (i);
//			}
//		}
//
//		if (indexesToCheck.Count < 3)
//		{
//			return listToCheckNeighbourLayer;
//		}
//
//		while (indexesToCheck.Count != 0 || currentCheck.Count != 0)
//		{
//			if (currentCheck.Count == 0)
//			{
//				item = indexesToCheck[indexesToCheck.Count - 1];
//				currentCheck.Add (item);
//				indexesToCheck.RemoveAt (indexesToCheck.Count - 1);
//				currentItemIndex = 0;
//			}
//			else
//			{
//				++currentItemIndex;
//
//				if (currentItemIndex > currentCheck.Count - 1)
//				{
//					if (currentCheck.Count > 2)
//					{
//						indexesOfCandiesToDestroy.AddRange (currentCheck);
//						listToCheckNeighbourLayer.AddRange (currentCheck);
//					}
//					currentCheck.Clear ();
//					continue;
//				}
//				item = currentCheck[currentItemIndex];
//			}
//			addCounter = 0;
//
//			for (int i = indexesToCheck.Count - 1; i >= 0; --i)
//			{
//				itemStorage = indexesToCheck[i];
//
//				if ((item % layerLength != 0 && item - 1 == itemStorage) || ((item + 1) % layerLength != 0 && item + 1 == itemStorage) ||
//					(item - layerLength >= 0 && item - layerLength == itemStorage) || (item + layerLength < layerLength * layerLength && item + layerLength == itemStorage))
//				{
//					currentCheck.Add (itemStorage);
//					++addCounter;
//				}
//			}
//
//			if (addCounter == 0)
//			{
//				if (currentCheck.Count > 2 && (currentItemIndex == currentCheck.Count - 1 || indexesToCheck.Count == 0))
//				{
//					indexesOfCandiesToDestroy.AddRange (currentCheck);
//					listToCheckNeighbourLayer.AddRange (currentCheck);
//					currentCheck.Clear ();
//				}
//			}
//			else
//			{
//				indexesToCheck.RemoveAll (it => currentCheck.Contains (it));
//			}
//		}
//
//		return listToCheckNeighbourLayer;
//	}
//
//	// check neighbour layer(Transform[] layer from arguments) for parent layer combo continue
//	private List<int> CheckNeighbourLayer (int candyType, Transform[] layer, List <int> parentLayerComboIndexes)
//	{		
//		List<int> candiesTypesLocalList = NewCandiesTypesList (candyType, layer);
//		List<int> candiesToDestroyList = new List<int> ();
//
////		candiesToDestroyList = candiesTypesLocalList.RemoveAll (it => !candiesTypesLocalList.Contains (it));
//		foreach (int candyIndex in candiesTypesLocalList)
//		{
//			if (parentLayerComboIndexes.Contains (candyIndex))
//			{
//				candiesToDestroyList.Add (candyIndex);
//			}
//		}
//		if (candiesToDestroyList.Count == 0)
//		{
//			return candiesToDestroyList;
//		}
//		candiesTypesLocalList.RemoveAll (it => candiesToDestroyList.Contains (it));
//
//		foreach (int candyIndex in candiesToDestroyList)
//		{
//			if (candyIndex - 1 >= 0 && candyIndex % layerLength != 0 && !candiesToDestroyList.Contains (candyIndex - 1) && candiesTypesLocalList.Contains (candyIndex - 1))
//			{
//				candiesToDestroyList.Add (candyIndex - 1);
//			}
//			if (candyIndex + 1 < layer.Length && (candyIndex + 1) % layerLength != 0 && !candiesToDestroyList.Contains (candyIndex + 1) && candiesTypesLocalList.Contains (candyIndex + 1))
//			{
//				candiesToDestroyList.Add (candyIndex + 1);
//			}
//			if (candyIndex - layerLength >= 0 && !candiesToDestroyList.Contains (candyIndex - layerLength) && candiesTypesLocalList.Contains (candyIndex - layerLength))
//			{
//				candiesToDestroyList.Add (candyIndex - layerLength);
//			}
//			if (candyIndex + layerLength < layer.Length && !candiesToDestroyList.Contains (candyIndex + layerLength) && candiesTypesLocalList.Contains (candyIndex + layerLength))
//			{				
//				candiesToDestroyList.Add (candyIndex + layerLength);
//			}
//		}
//
//		return candiesToDestroyList;
//	}
//
//	// move down and spawn new candies in certain layer
//	IEnumerator MoveLikeTetris (int axis, int layerNumber, int layerHeight, float delay, Transform[] layer, Vector3 offset)
//	{
//		int i;
//
//		yield return new WaitForSeconds (delay);
//
//		for (int j = layer.Length - 1; j >= layerHeight; --j)
//		{
//			if (layer[j].childCount == 0)
//			{
//				i = j - layerHeight;
//
//				while (i >= 0)
//				{
//					if (layer[i].childCount != 0)
//					{
//						//change opacity when move down to active layer
//						if (layerNumber == (int)(j / layerHeight))// && layerNumber + layerHeight > j)
//						{
////							Debug.Log ("LN = " + layerNumber + " | j = " + i);
//							layer[i].GetComponent <CandyPoint> ().MoveDownCandy (true, false, layer[j]);
//						}
//						else
//						{
//							layer[i].GetComponent <CandyPoint> ().MoveDownCandy (false, false, layer[j]);
//						}
////						layer[i].GetComponent <CandyPoint> ().MoveDownCandy (false, layer[j]);
//						break;
//					}
//					i -= layerHeight;
//				}
//			}
//		}
//
//		if (layerNumber <= 0)
//		{
//			for (i = 0; i < layer.Length; ++i)
//			{
//				if (layer[i].childCount == 0)
//				{
//					layer[i].GetComponent <CandyPoint> ().SpawnCandy (false, false, CandySpawnDelay);//, offset);
//				}
//			}
//		}
//		else
//		{
//			for (i = 0; i < layer.Length; ++i)
//			{
//				if (layer[i].childCount == 0)
//				{
//					layer[i].GetComponent <CandyPoint> ().SpawnCandy (true, false, CandySpawnDelay);//, offset);
//				}
//			}
//		}
//
////		if (axis >= 0 && layerNumber > 0)
////		{
////			ChangeLayersOpacity (axis, layerNumber);
////		}
//	}
//
//
//	private void FillCandiesTypesList (Transform[] layer)
//	{
//		candiesTypesList.Clear ();
//
//		for (int i = 0; i < layer.Length; ++i)
//		{
//			candiesTypesList.Add (layer[i].GetComponent <CandyPoint> ().CandyType);
//		}
//	}
//
//	// add only candy == candyType indexes, to check neighbour layers
//	private List<int> NewCandiesTypesList (int candyType, Transform[] layer)
//	{
//		int candyTypeLocal;
//		List<int> filledList = new List<int> ();
//
//		for (int i = 0; i < layer.Length; ++i)
//		{
//			candyTypeLocal = layer[i].GetComponent <CandyPoint> ().CandyType;
//
//			if (candyTypeLocal == candyType)
//			{
//				filledList.Add (i);
//			}
//		}
//
//		return filledList;
//	}
//
//
//	private int[,] InitCandiesTypesTable (Transform[] layer)
//	{
//		int[,] candiesTypesTable = new int[layerLength, layerLength];
//		string candyName;
//		int candyType;
//		int i = 0; // index of strings of candiesTypesTable
//
//		for (int k = 0; k < Mathf.Pow (layerLength, 2); ++k) // layer elements
//		{
//			candyType = layer[k].GetComponent <CandyPoint> ().CandyType;
//
//			if (k < layerLength)
//			{
//				candiesTypesTable [0, k] = candyType;
//				continue;
//			}
//
//			if (k % layerLength == 0)
//			{
//				++i;
//			}
//
//			candiesTypesTable [i, k % layerLength] = candyType;
//		}
//		return candiesTypesTable;
//	}
//
//
//	private void InitArray (int value, int[,] table)
//	{
//		for (int i = 0; i < layerLength; ++i)
//		{
//			for (int j = 0; j < layerLength; ++j)
//			{				
//				table[i, j] = value;
//			}
//		}
//	}
//
//
//	public void InitBoolArray (bool value, bool[] array)
//	{
//		for (int i = 0; i < array.Length; ++i)
//		{
//			array[i] = value;
//		}
//	}


	// // // HELPERS

	public int FindItemIndex (int axis, Transform item)
	{
		int itemIndex = -1;

		switch (axis)
		{
		case 0:
			itemIndex = System.Array.IndexOf (X1Layers[X1CurrentLayer], item);
			break;

		case 1:
			itemIndex = System.Array.IndexOf (YILayers[YICurrentLayer], item);
			break;

		case 2:
			itemIndex = System.Array.IndexOf (ZALayers[ZACurrentLayer], item);
			break;
		}

		return itemIndex;
	}

	// for 3d swap
	public int FindItemIndex (int axis, int layerIndex, Transform item)
	{
		int itemIndex = -1;

		switch (axis)
		{
		case 0:
			itemIndex = System.Array.IndexOf (X1Layers[layerIndex], item);
			break;

		case 1:
			itemIndex = System.Array.IndexOf (YILayers[layerIndex], item);
			break;

		case 2:
			itemIndex = System.Array.IndexOf (ZALayers[layerIndex], item);
			break;
		}

		return itemIndex;
	}

	// 
	public Transform[] GetLayer (int axisNumber)
	{
		switch (axisNumber)
		{
		case 0:
			return X1Layers[X1CurrentLayer];
			break;

		case 1:
			return YILayers[YICurrentLayer];
			break;

		case 2:
			return ZALayers[ZACurrentLayer];
			break;
		}

		return null;
	}


	// for 3d swap
	public Transform[] GetLayer (int axisNumber, int layerIndex)
	{
		switch (axisNumber)
		{
		case 0:
			return X1Layers[layerIndex];
			break;

		case 1:
			return YILayers[layerIndex];
			break;

		case 2:
			return ZALayers[layerIndex];
			break;
		}

		return null;
	}


	public int GetCurrentLayerByAxis (int axis)
	{
		switch (axis)
		{
		case 0:
			return X1CurrentLayer;
			break;

		case 1:
			return YICurrentLayer;
			break;

		case 2:
			return ZACurrentLayer;
			break;
		}

		return -1;
	}

	// set mainLayer's opacity to 1, other layers to <1 
//	public void ChangeLayersOpacity (int axis, int mainLayer)
//	{
//		switch (axis)
//		{
//		case 0:
//			for (int i = 0; i < X1Layers.Count; ++i)
//			{
//				if (i != mainLayer)
//				{
//					foreach (Transform candyPoint in X1Layers[i])
//					{
//					candyPoint.GetChild (0).GetComponent <ChangeMaterial>().ChangeOpacity (false); // set material to faded
//					}
//				}
//			}
//			foreach (Transform candyPoint in X1Layers[mainLayer])
//			{
//				candyPoint.GetChild (0).GetComponent <ChangeMaterial>().ChangeOpacity (true); // set material to normal
//			}
//			break;
//
//		case 1:
//			for (int i = 0; i < YILayers.Count; ++i)
//			{
//				if (i != mainLayer)
//				{
//					foreach (Transform candyPoint in YILayers[i])
//					{
//						candyPoint.GetChild (0).GetComponent <ChangeMaterial>().ChangeOpacity (false); // set material to faded
//					}
//				}
//			}
//			foreach (Transform candyPoint in YILayers[mainLayer])
//			{
//				candyPoint.GetChild (0).GetComponent <ChangeMaterial>().ChangeOpacity (true); // set material to normal
//			}
//			break;
//
//		case 2:
//			for (int i = 0; i < ZALayers.Count; ++i)
//			{
//				if (i != mainLayer)
//				{
//					foreach (Transform candyPoint in ZALayers[i])
//					{
//						candyPoint.GetChild (0).GetComponent <ChangeMaterial>().ChangeOpacity (false); // set material to faded
//					}
//				}
//			}
//			foreach (Transform candyPoint in ZALayers[mainLayer])
//			{
//				candyPoint.GetChild (0).GetComponent <ChangeMaterial>().ChangeOpacity (true); // set material to normal
//			}
//			break;
//		}
//	}


//	public void SetOpacityToFull ()
//	{
////		Debug.Log ("000 : " + Time.time);
//		foreach (Transform[] layer in X1Layers)
//		{
//			foreach (Transform candyPoint in layer)
//			{
//				if (candyPoint.childCount > 0)
//				{
//					candyPoint.GetChild (0).GetComponent <ChangeMaterial>().ChangeOpacity (true);
//				}
//			}
//		}
//	}
}
