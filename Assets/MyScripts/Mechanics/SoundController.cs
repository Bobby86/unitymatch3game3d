﻿using UnityEngine;
using System.Collections;


public class SoundController : MonoBehaviour 
{
	public static SoundController Instance;

	public ImageColorAlpha MusicIcon;
	public ImageColorAlpha SoundIcon;
	[Range (0f, 1f)]public float CombinationsVolume = 1f;
	[Range (0f, 1f)]public float ExplosionsVolume = 1f;
	[Range (-3f, 3f)]public float ExplosionsPitch = 1f;
	public AudioSource[] ExplosionsSounds;	// attached to MainCamera
	public AudioSource[] ComboSounds;		// alot of AudioSource because of sounds overlapse

	private bool changeIndex = false;	// maximum of 2 plays of the same sound in the row for length == 2 of ExplosionsSounds and different sound each time for length > 2
	private int previousIndex = -1;
	private bool soundOn = false;
	private bool musicOn = false;

	void Awake ()
	{
		Instance = this;
	}

	void Start ()
	{
		if (PlayerPrefs.GetFloat ("SoundVolume") > 0)
		{
			soundOn = true;
		}

		if (PlayerPrefs.GetFloat ("MusicVolume") > 0)
		{
			musicOn = true;
		}

//		Invoke ("SetIcons", 0.01f);

		foreach (AudioSource explosionSound in ExplosionsSounds)
		{
			explosionSound.volume = ExplosionsVolume;
			explosionSound.pitch = ExplosionsPitch;
		}

		foreach (AudioSource comboSound in ComboSounds)
		{
			comboSound.volume = CombinationsVolume;
		}
	}


	public void PlayRandomExplosionSound ()
	{
		if (!soundOn)
		{
			return;
		}

		int index;

		if (ExplosionsSounds.Length < 3)
		{
			if (changeIndex)
			{
				index = 1 - previousIndex;
				changeIndex = false;
			}
			else
			{
				index = Random.Range (0, ExplosionsSounds.Length);

				if (index == previousIndex)
				{
					changeIndex = true;
				}
			}
		}
		else
		{
			index = Random.Range (0, ExplosionsSounds.Length);

			while (index == previousIndex)
			{
				index = Random.Range (0, ExplosionsSounds.Length);
			}
		}

		ExplosionsSounds[index].Play ();
//		ExplosionsSounds[index].PlayDelayed (MyGameController.TimeOfInlightCandy);

		previousIndex = index;
//		Debug.Log (index);
	}


	public void PlayeNextComboSound (int comboNumber)
	{
		if (!soundOn)
		{
			return;
		}

		if (comboNumber > 4) comboNumber %= 5;

//		ComboSounds[comboNumber].Play ();
		StartCoroutine (PlaySoundOverTime (ComboSounds[comboNumber], 0.4f)); // files are too long, might be laggy at mobile platforms
	}


	private IEnumerator PlaySoundOverTime (AudioSource sound, float timeToPlay)
	{
		sound.Play ();
		yield return new WaitForSeconds (timeToPlay);
		sound.Stop ();
	}

	// buttons
	public void SoundButtonClick ()
	{
		soundOn = !soundOn;
		SoundIcon.FadeButton (soundOn);

		if (soundOn)
		{
			PlayerPrefs.SetFloat ("SoundVolume", 1f);
		}
		else
		{
			PlayerPrefs.SetFloat ("SoundVolume", 0f);
		}

		PlayerPrefs.Save ();
	}

	public void MusicButtonClick ()
	{
		musicOn = !musicOn;
		MusicIcon.FadeButton (musicOn);

		if (musicOn)
		{
			PlayerPrefs.SetFloat ("MusicVolume", 1f);
		}
		else
		{
			PlayerPrefs.SetFloat ("MusicVolume", 0f);
		}

		PlayerPrefs.Save ();
	}

	public void SetIcons ()
	{
		SoundIcon.InitializeColors (UIParameters.Instance.FullColor, UIParameters.Instance.FadedColor, soundOn);
		MusicIcon.InitializeColors (UIParameters.Instance.FullColor, UIParameters.Instance.FadedColor, musicOn);
	}
}
