﻿using UnityEngine;
using System.Collections;

public class ChangeMaterialGoalCandy : MonoBehaviour 
{
	#region Public References
	public static float SpeedOfLightUpMaterial;

	public Material NormalMaterial;
	public Material FadedMaterial;
	public Material OutlineMaterial;
	#endregion


	#region Private References
	private float outlineValue;
	private MeshRenderer candyRenderer;
	#endregion


	#region Unity Callbacks
	void Start ()
	{
		candyRenderer = GetComponent <MeshRenderer> ();
		outlineValue = candyRenderer.materials[1].GetFloat ("_Outline");
	}
	#endregion


	#region Public References
	public void ChangeOpacity (bool toNormal)
	{
		if (toNormal)
		{
			candyRenderer.material = NormalMaterial;
			candyRenderer.materials[1].SetFloat ("_Outline", outlineValue);
		}
		else
		{
			candyRenderer.material = FadedMaterial;
			candyRenderer.materials[1].SetFloat ("_Outline", 0f); 
		}
	}
	#endregion
}
