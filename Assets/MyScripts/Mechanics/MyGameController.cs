﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public enum GameState
{
	GamePlay,
	GamePlayNoControl,
	Pause,
	GameOver
}


// store candies preffabs
public class MyGameController : MonoBehaviour 
{
	public static bool Execution;
	public static bool NextComboWaveCheck = false;
	public static MyGameController gameController;
	public static GameState GState;

	public delegate void GameEvents ();
	public static event GameEvents GameOverEvent;
//	public static float TimeOfDespawnCandy = 0.15f;
//	public static float TimeUntilExplosionAfterLastCombo;	// Time of material changes to glow

	public int ShapesCount = 2; // spere, cube,
	public float DistanceBetweenPoints = 2.5f;
	public float DelayLayersCheck = 1.1f;	// delay between layers check
	public float SwapCandiesMoveSpeed = 15f;
	public float SpeedOfLightUpMaterial = 1f;
//	public float DelayBetweenLastComboAndExplosion = 0.3f;
//	public float SSASpeed = 2f;
	public float NotSimpleScaleFactor = 0.1f;
	public float NotSimpleScaleSpeed = 2f;
//	public float CandyMinAlphaValue = 0.1f;
	public float CandyAlphaChangeDuration = 0.5f;

	[SerializeField]private float gameOverDelay = 1f;

	public CameraRotator CameraRotationScript;
	public Vector3 AddScale = new Vector3 (0.1f, 0.1f, 0.1f);
	public List<GameObject> SpherePrefabs;	// blue, green, red, yellow, orange, purple
	public List<GameObject> CubePrefabs;

	/*	Difficulty system	*/
	public List<GameObject> DifficultyUpSpheres;	// should have reversed order by colors in the inspector
	public List<GameObject> DifficultyUpCubes;
	/*	***	*/

//	public Camera[] XLCameras;	// 2d cameras for X-layer
//	public Camera[] YLCameras;	// 2d cameras for Y-layer
//	public Camera[] ZLCameras;	// 2d cameras for Z-layer
//	public ImageController[] ButtonsIC;
//	public ImageController[] WorldButtonsIC;

//	private bool goalFlag;
	private bool fakeCombosBeenFoundFlag; 
	private bool executeFlag = false;
	private bool executeFlagForUpdate = false;
	private bool checkExecution = false;
//	private bool comboWasInOtherLayer = false;
	private bool axisWasChanged = false;
	private bool checkFromList = false;
	private bool checkMoveDownFlag = true;
	private int firstExecutedLayer;
	private int currentEnabledCube = -1;
	private int axisNumber = 0;
	private float firstDelay = 0.3f;
	private float currentDelay;
	private int[] checkLayers;
	private List <int> lastLayersToCheck = new List<int> ();
//	private LayersOpacityController opacityScript;
//	private CandyPoints2DLayoutModel executeCandiesScript;
	private ComboManager comboScript;
//	private SimpleRotation mainCameraSlowAutorotationScript;
//	private Vector3 normalButtonScale;
//	private float debugStartTime;


	void Awake ()
	{
		Random.seed = System.DateTime.Now.Second;
		MyGameController.gameController = this;
		MyGameController.Execution = false;
//		MyGameController.TimeUntilExplosionAfterLastCombo = DelayBetweenLastComboAndExplosion;
		DragScript.CurrentlyScaled = null;
		DragScript.SwapOnClick = false;
//		DragScript.readInput = true;
		DragScript.DistanceBetweenPoints = DistanceBetweenPoints;
		MyInputManager.ReadInput = true;
//		ChangeMaterial.MinAlphaValue = CandyMinAlphaValue;
		ChangeMaterial.AlphaChangeDuration = CandyAlphaChangeDuration;
		ChangeMaterial.SpeedOfLightUpMaterial = SpeedOfLightUpMaterial;
		ChangeMaterialChild.SpeedOfLightUpMaterial = SpeedOfLightUpMaterial;
//		SpriteSheetController.SSASpeed = SSASpeed;
//		UISpriteSheetController.SSASpeed = SSASpeed;
		NotSimpleScaler.ScaleFactor = NotSimpleScaleFactor;
		NotSimpleScaler.ScaleSpeed = NotSimpleScaleSpeed;
		SimpleScaler.AddScale = AddScale;
		GState = GameState.GamePlayNoControl;	// would be changed after spawn all elements
	}


	void Start ()
	{
		Random.seed = 42;
//		executeCandiesScript = GetComponent <CandyPoints2DLayoutModel> ();
		comboScript = GetComponent <ComboManager> ();
		checkLayers = new int[3];	// for each axis
		currentDelay = firstDelay;

		DragScript.SwapMovementStep = SwapCandiesMoveSpeed;
//		opacityScript = GetComponent <LayersOpacityController> ();
//		mainCameraSlowAutorotationScript = CameraRotationScript.GetComponent <SimpleRotation> ();
	}


	void Update ()
	{
		if (GState == GameState.Pause)
		{
			return;
		}

		if (Execution)
		{
			Execution = false;

			if (checkMoveDownFlag)
			{
//				Debug.Log (Time.time - debugStartTime);
//				debugStartTime = Time.time;
				++ScoreController.WaveNumber;
				executeFlagForUpdate = comboScript.FindAndExplodeACombo ();

				if (executeFlagForUpdate)
				{
					Invoke ("ExecutionDelay", DelayLayersCheck);
				}
				else
				{
//					Debug.Log (Time.time);
//					Debug.Log (comboScript.FindAFakeCombo ());
//					Debug.Log (Time.time);

//					goalFlag = goalScript.CheckTheGoal (true);
//					if (goalScript.CheckTheGoal (true)) // else game over -> GoalSystemController.cs
//					{	
						fakeCombosBeenFoundFlag = comboScript.FindAFakeCombo ();

						if (!fakeCombosBeenFoundFlag)
						{
//							Debug.Log ("There's no possible combo");
//							GameOver ();
							DragScript.readInput = false;
							MyInputManager.ReadInput = false;
							Invoke ("GameOver", gameOverDelay);
						}
						else
						{
//							Debug.Log ("Done");
						}
	//					mainCameraSlowAutorotationScript.enabled = true;
						DragScript.readInput = true;
//						MyInputManager.ReadInput = true;
	//					executeCandiesScript.ChangeLayersOpacity (MouseoverLayerDetection.HoverAxis, MouseoverLayerDetection.HoverLayer);
	//					CameraRotationScript.RotateCamera (MouseoverLayerDetection.HoverAxis, MouseoverLayerDetection.HoverLayer);
						executeFlag = false;
//					}
				}
			}
			else
			{
				Invoke ("ExecutionDelay", 0.1f);
			}
		}	
	}


	private void ExecutionDelay ()
	{
//		foreach (CandyPoint[] cpArray in candyPointsByLayers)
//		{
//			foreach (CandyPoint cp in cpArray)
//			{
//				if (!cp.CheckChildPosition ())
//				{
//					// TODO
//					checkMoveDownFlag = false;
//					Execution = true;
//					return;
//				}
//			}
//		}

		if (NextComboWaveCheck)	// changes in ComboManager instance
		{			
			NextComboWaveCheck = false;
			checkMoveDownFlag = true;
		}
		else
		{
			checkMoveDownFlag = false;
		}

		Execution = true;
	}


	public void ExecuteCombo ()
	{
		if (!executeFlag)
		{
//			debugStartTime = Time.time;
//			executeCandiesScript.SetOpacityToFull ();
//			mainCameraSlowAutorotationScript.enabled = false;
//			CameraRotationScript.RotateCamera ();		// to 0,0,0
			executeFlag = true;
			ScoreController.WaveNumber = 0;
			executeFlagForUpdate = comboScript.FindAndExplodeACombo ();

			if (executeFlagForUpdate)
			{
//				currentDelay = DelayLayersCheck;
				Invoke ("ExecutionDelay", DelayLayersCheck);
			}
			else
			{
//				Execution = false;
				executeFlag = false;
//				mainCameraSlowAutorotationScript.enabled = true;
				DragScript.readInput = true;
//				MyInputManager.ReadInput = true;
				Debug.Log ("000WTH");

//				currentDelay = firstDelay;
			}
		}
	}

	private void GameOver ()
	{
		GState = GameState.GameOver;

		if (GameOverEvent != null)
		{
			GameOverEvent ();
		}
	}

	public void AddDifficultyLevel ()
	{
		int lastIndexOfDifficultyLists = DifficultyUpSpheres.Count - 1;
		// add new color shapes
		if (DifficultyUpSpheres.Count > 0)
		{
			SpherePrefabs.Add (DifficultyUpSpheres[lastIndexOfDifficultyLists]);
			DifficultyUpSpheres.RemoveAt (lastIndexOfDifficultyLists);
		}

		if (DifficultyUpCubes.Count > 0)
		{
			CubePrefabs.Add (DifficultyUpCubes[lastIndexOfDifficultyLists]);
			DifficultyUpCubes.RemoveAt (lastIndexOfDifficultyLists);
		}
	}
}
