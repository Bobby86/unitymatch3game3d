﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

// 3-layers x 5 candies mechanics
public class ComboManager : MonoBehaviour 
{
	#region Public References
	public static int LayerLength; // amount of candies in the layer
	public static ComboManager Instance;

	public float DelayLightUpNextCombo = 0.1f;
	public float CandySpawnDelay = 0.1f; 
	[SerializeField]private float CubeSpawnDelay = 1f;
	public float StartSpawnDelay = 0.05f;
	public float NewCandiesSpawnDelay = 0.3f;
	public float MoveDownSpeed = 3f;	// speed of candy movement to it's new place
	public float MoveDownAcceleration = 4f;

	public Transform[] Layer1;
	public Transform[] Layer2;
	public Transform[] Layer3;
	[HideInInspector]public List <Transform[]> Layers = new List<Transform[]> ();
	#endregion


	#region Private References
	private int colorsLength; // amount of available colors
	private int shapesLength;	// amount of available shapes
	private float doubleSpeed;
	private SoundController soundController;
	private LayersOpacityController opacityScript;
	private CandyPoint[] candyPointsLayer01;
	private CandyPoint[] candyPointsLayer02;
	private CandyPoint[] candyPointsLayer03;
	private List <CandyPoint[]> candyPointsByLayers = new List<CandyPoint[]> ();

	private List <GameObject> candiesGOToDestroy = new List<GameObject> ();	// unparented candies

	private List <List <List <int>>> candiesTypesByLayersList;
	// Indexes of elements to destroy by the layers
	private List <CandyPoint> emptyCandyPoints = new List<CandyPoint> ();
	private List <List <int>> combinationsToDestroy = new List<List<int>> (); // ( (layer1 indexes of 1st combo), (layer 2 indexes of 1st combo), (layer 3 indexes of 1st combo) , (layer 1 indexs of 2nd combo), etc... )

	private ScoreController scoreScript;
	#endregion

	#region Unity Callbacks
	void Awake ()
	{
		Instance = this;
		DragScript.MoveDownSpeed = MoveDownSpeed;
		DragScript.MoveDownAcceleration = MoveDownAcceleration;
		CandyPoint.CandySpawnDelay = CandySpawnDelay;
		LayerLength = Layer1.Length;
	}


	void Start ()
	{
		Layers.Add (Layer1);
		Layers.Add (Layer2);
		Layers.Add (Layer3);

		colorsLength = MyGameController.gameController.SpherePrefabs.Count;
		shapesLength = MyGameController.gameController.ShapesCount;
		candiesTypesByLayersList = new List<List<List<int>>> (Layers.Count * shapesLength * colorsLength);
		candyPointsLayer01 = new CandyPoint[Layer1.Length];
		candyPointsLayer02 = new CandyPoint[Layer2.Length];
		candyPointsLayer03 = new CandyPoint[Layer3.Length];

		for (int i = 0; i < candyPointsLayer01.Length; ++i)
		{
			candyPointsLayer01[i] = Layer1[i].GetComponent <CandyPoint> ();
			candyPointsLayer02[i] = Layer2[i].GetComponent <CandyPoint> ();
			candyPointsLayer03[i] = Layer3[i].GetComponent <CandyPoint> ();
		}
		candyPointsByLayers.Add (candyPointsLayer01);
		candyPointsByLayers.Add (candyPointsLayer02);
		candyPointsByLayers.Add (candyPointsLayer03);

		doubleSpeed = MoveDownSpeed * 2;
		soundController = GetComponent <SoundController> ();
		opacityScript = GetComponent <LayersOpacityController> ();
		scoreScript = ScoreController.ScoreScript;

		StartCoroutine (SpawnNewCube ());
	}

	void OnEnable ()
	{
		DifficultyController.OnDifficultyLevelUp += DifficultyLevelUpAction;
	}

	void OnDisable ()
	{
		DifficultyController.OnDifficultyLevelUp -= DifficultyLevelUpAction;
	}
	#endregion


	#region Initialization&Updates Part
	// start of the game, spawn new elements
	private IEnumerator SpawnNewCube ()
	{
		yield return new WaitForSeconds (CubeSpawnDelay);

		foreach (CandyPoint[] cpLayer in candyPointsByLayers)
		{
			foreach (CandyPoint cp in cpLayer)
			{
				cp.SpawnACandy ();
				yield return new WaitForSeconds (StartSpawnDelay);
			}
		}

		if (!FindAFakeCombo ())
		{
			Debug.Log ("No possible combo");
		}

		MyGameController.GState = GameState.GamePlay;
		DragScript.readInput = true;
	}

	private void DifficultyLevelUpAction (bool addNewColor)
	{
		if (addNewColor)
		{
			++colorsLength;
		}
	}
	#endregion


	#region Main Combinations Part
	public bool FindAndExplodeACombo ()
	{
		bool returnValue = false;

		CleanUp ();
		// check layers (0-2) for 2d combos (1 - maxNumber elements)
		for (int candyShape = 0; candyShape < shapesLength; ++candyShape)
		{
			for (int candyType = 0; candyType < colorsLength; ++candyType)
			{			
				for (int layerIndex = 0; layerIndex < Layers.Count; ++layerIndex)
				{
					candiesTypesByLayersList.Add (SplitTheLayersCandiesByCombos (candyType, candyShape, Layers[layerIndex], layerIndex));
				}
			}
		}
		// now here's 15 lists of lists in candiesTypesByLayersList (for each candy type and layer) with combos of the same candies
		// check results for 3d combos
		for (int startIndexForCandy = 0; startIndexForCandy < candiesTypesByLayersList.Count; startIndexForCandy += Layers.Count)
		{
			RemoveNoncombo (startIndexForCandy);
		}

//		if (candiesToDestroyOne.Count > 0 || candiesToDestroyTwo.Count > 0 || candiesToDestroyThree.Count > 0)
		if (combinationsToDestroy.Count > 0)
		{
			returnValue = true;
		}
		else
		{
			CleanUp ();
			return returnValue;
		}

		StartCoroutine (DestroyCandiesByIndexesList (combinationsToDestroy));

		return returnValue;
//		Debug.Log (candiesTypesByLayersList[0].Count);
//		Debug.Log ("0000000000");
//		int nvm = 0;
//		foreach (List<int> list in candiesTypesByLayersList[0])
//		{			
//			Debug.Log (nvm + " : ");
//			foreach (int index in list)
//			{
//				Debug.Log ("      " + index); 
//			}
//			++nvm;
//		}
//		Debug.Log ("1111111111");
//		nvm = 0;
//		foreach (List<int> list in candiesTypesByLayersList[1])
//		{			
//			Debug.Log (nvm + " : ");
//			foreach (int index in list)
//			{
//				Debug.Log ("      " + index); 
//			}
//			++nvm;
//		}
//		Debug.Log ("2222222222");
//		nvm = 0;
//		foreach (List<int> list in candiesTypesByLayersList[2])
//		{			
//			Debug.Log (nvm + " : ");
//			foreach (int index in list)
//			{
//				Debug.Log ("      " + index); 
//			}
//			++nvm;
//		}

//		int x = 0;
//		foreach (List <List <int>> ctxLx in candiesTypesByLayersList)
//		{
//			Debug.Log (x + " : " + ctxLx);
//			Debug.Log (" : " + ctxLx.Count);
//			++x;
//
//			int y = 0;
//			foreach (List <int> combo in ctxLx)
//			{
//				Debug.Log ("     : " + y + " : " + combo);
////				Debug.Log ("      " + combo);
//				foreach (int index in combo)
//				{
//					Debug.Log ("              " + index);
//				}
//				++y;
//			}
//		}
	}

	// split a layer by combos of certain candy type
	private List <List <int>> SplitTheLayersCandiesByCombos (int candyType, int candyShape, Transform[] layer, int layerNumberForDebug)
	{
		int item;
		int currentItemIndex = 0;	// in currentCheck
		int itemStorage;
		int addCounter; 
		int columnLength = (int) Mathf.Sqrt (layer.Length);
		List <int> currentCheck = new List<int>();
		List <int> listToCheckNeighbourLayer = new List<int>();
		List <int> indexesToCheck = NewCandiesTypesList (candyType, candyShape, layer);
		List <List<int>> outputList = new List<List<int>>();

		if (indexesToCheck.Count < 1)
		{
			return outputList;
		}
		else if (indexesToCheck.Count == 1)
		{
			outputList.Add (indexesToCheck);
			return outputList;
		}

		while (indexesToCheck.Count != 0 || currentCheck.Count != 0)
		{
			if (currentCheck.Count == 0)
			{
				item = indexesToCheck[indexesToCheck.Count - 1];
				currentCheck.Add (item);
				indexesToCheck.RemoveAt (indexesToCheck.Count - 1);
				currentItemIndex = 0;
			}
			else
			{
				++currentItemIndex;

				if (currentItemIndex > currentCheck.Count - 1)
				{
					List<int> currentCombo = new List<int> ();
					currentCombo.AddRange (currentCheck);
					outputList.Add (currentCombo);
					currentCheck.Clear ();
					continue;
				}
				item = currentCheck[currentItemIndex];
			}
			addCounter = 0;

			for (int i = indexesToCheck.Count - 1; i >= 0; --i)
			{
				itemStorage = indexesToCheck[i];

				if ((item % columnLength != 0 && item - 1 == itemStorage) || ((item + 1) % columnLength != 0 && item + 1 == itemStorage) ||
					(item - columnLength >= 0 && item - columnLength == itemStorage) || (item + columnLength < columnLength * columnLength && item + columnLength == itemStorage))
				{
					currentCheck.Add (itemStorage);
					++addCounter;
				}
			}

			if (addCounter == 0)
			{
				if (currentCheck.Count > 0 && (currentItemIndex == currentCheck.Count - 1 || indexesToCheck.Count == 0))
				{
					List<int> currentCombo = new List<int> ();
					currentCombo.AddRange (currentCheck);
					outputList.Add (currentCombo);
					currentCheck.Clear ();
				}
			}
			else
			{
				indexesToCheck.RemoveAll (it => currentCheck.Contains (it));
			}
		}

//		if (candyType == 2)
//		{
//			int comboList = 0;
//			foreach (List <int> list in outputList)
//			{
//				Debug.Log (comboList + " :");
//				foreach (int index in list)
//				{
//					Debug.Log (Time.time + " : " + layerNumberForDebug + " : " + index);
//				}
//				++comboList;
//			}
//		}

		return outputList;
	}

	// add only elements indexes of the same shape and color
	private List<int> NewCandiesTypesList (int candyType, int candyShape, Transform[] layer)
	{
		int candyShapeLocal;
		int candyTypeLocal;
		CandyPoint cp;
		List<int> filledList = new List<int> ();

		for (int i = 0; i < layer.Length; ++i)
		{
			cp = layer[i].GetComponent <CandyPoint> ();
			candyTypeLocal = cp.CandyType;
			candyShapeLocal = cp.CandyShape;

			if (candyShapeLocal == candyShape && candyTypeLocal == candyType)
			{
				filledList.Add (i);
			}
		}

		return filledList;
	}


	// check layers for 3D-combos from 
	private void RemoveNoncombo (int firstLayerIndex)
	{
		bool list0WasAdded;
		bool list1WasAdded;
		bool list2WasAdded;
		int secondLayerIndex = firstLayerIndex + 1;
		int thirdLayerIndex = firstLayerIndex + 2;
		int countSum;
		List <int> layer0ComboIndexes = new List<int>();
		List <int> layer2ComboIndexes = new List<int>();
		List <int> layer1ComboIndexes = new List<int>();
		List <int> doubdfulComboIndexesL0 = new List<int> (); // if (countSum = 2) => 1 element in layers 1 and 0
		List <int> doubdfulComboIndexesL2 = new List<int> (); // if (countSum = 2) => 1 element in layers 1 and 2

		foreach (List<int> listInLayer1 in candiesTypesByLayersList[secondLayerIndex])
		{
			if (listInLayer1.Count > 2)
			{
				layer1ComboIndexes.AddRange (listInLayer1);
				list1WasAdded = true;
			}
			else
			{
				list1WasAdded = false;
			}

			for (int s = 0; s < listInLayer1.Count; ++s)
			{
				foreach (List<int> listInLayer0 in candiesTypesByLayersList[firstLayerIndex])
				{
					if (listInLayer0.Count > 2)
					{
						layer0ComboIndexes.AddRange (listInLayer0);
						list0WasAdded = true;
					}
					else
					{
						list0WasAdded = false;
					}

					if (listInLayer0.Contains (listInLayer1[s]))
					{						
						countSum = listInLayer0.Count + listInLayer1.Count;

						if (countSum > 2)
						{
							if (!list0WasAdded)
							{
								layer0ComboIndexes.AddRange (listInLayer0);
								list0WasAdded = true;
							}
							if (!list1WasAdded)
							{
								layer1ComboIndexes.AddRange (listInLayer1);
								list1WasAdded = true;
							}
						}
						if (countSum == 2)
						{
							doubdfulComboIndexesL0.Add (listInLayer1[s]);
						}
					}
				}

				foreach (List<int> listInLayer2 in candiesTypesByLayersList[thirdLayerIndex])
				{
					if (listInLayer2.Count > 2)
					{
						layer2ComboIndexes.AddRange (listInLayer2);
						list2WasAdded = true;
					}
					else
					{
						list2WasAdded = false;
					}

					if (listInLayer2.Contains (listInLayer1[s]))
					{						
						countSum = listInLayer2.Count + listInLayer1.Count;

						if (countSum > 2 || list1WasAdded)
						{
							if (!list2WasAdded)
							{
								layer2ComboIndexes.AddRange (listInLayer2);
								list2WasAdded = true;
							}
							if (!list1WasAdded)
							{
								layer1ComboIndexes.AddRange (listInLayer1);
								list1WasAdded = true;
							}
						}
						if (countSum == 2)
						{
							doubdfulComboIndexesL2.Add (listInLayer1[s]);
						}
					}
				}
			}

			// add combo to destroy-candies list
			if (layer0ComboIndexes.Count != 0 || layer1ComboIndexes.Count != 0 || layer2ComboIndexes.Count != 0)
			{
				layer0ComboIndexes = layer0ComboIndexes.Distinct ().ToList ();
				layer1ComboIndexes = layer1ComboIndexes.Distinct ().ToList ();
				layer2ComboIndexes = layer2ComboIndexes.Distinct ().ToList ();
				combinationsToDestroy.Add (new List<int> (layer0ComboIndexes));
				combinationsToDestroy.Add (new List<int> (layer1ComboIndexes));
				combinationsToDestroy.Add (new List<int> (layer2ComboIndexes));
				layer0ComboIndexes.Clear ();
				layer1ComboIndexes.Clear ();
				layer2ComboIndexes.Clear ();
			}
		}			

		if (candiesTypesByLayersList[secondLayerIndex].Count == 0)
		{
			List <int> empty = new List<int> ();

			foreach (List<int> listInLayer0 in candiesTypesByLayersList[firstLayerIndex])
			{
				if (listInLayer0.Count > 2)
				{
//					layer2ComboIndexes.AddRange (listInLayer0);
					combinationsToDestroy.Add (new List<int> (listInLayer0));
					combinationsToDestroy.Add (empty);
					combinationsToDestroy.Add (empty);
				}
			}

			foreach (List<int> listInLayer2 in candiesTypesByLayersList[thirdLayerIndex])
			{
				if (listInLayer2.Count > 2)
				{
//					layer2ComboIndexes.AddRange (listInLayer2);
					combinationsToDestroy.Add (empty);
					combinationsToDestroy.Add (empty);
					combinationsToDestroy.Add (new List<int> (listInLayer2));
				}
			}

			// add combo to destroy-candies list
//			layer0ComboIndexes = layer0ComboIndexes.Distinct ().ToList ();
//			layer2ComboIndexes = layer2ComboIndexes.Distinct ().ToList ();
//			combinationsToDestroy.Add (layer0ComboIndexes);
//			combinationsToDestroy.Add (layer1ComboIndexes);	// it's empty
//			combinationsToDestroy.Add (layer2ComboIndexes);
//			layer0ComboIndexes.Clear ();
//			layer2ComboIndexes.Clear ();
		}


		// check doubdful combos
		foreach (int index in doubdfulComboIndexesL0)
		{
			if (doubdfulComboIndexesL2.Contains (index))
			{
				//				layer0ComboIndexes.Add (index);
				//				layer1ComboIndexes.Add (index);
				//				layer2ComboIndexes.Add (index);
				//				doubdfulComboIndexesL2.Remove (index);
				combinationsToDestroy.Add (new List<int> () {index});
				combinationsToDestroy.Add (new List<int> () {index});
				combinationsToDestroy.Add (new List<int> () {index});
			}
		}


		// reflat // merging combinations connected through neighbour layers 
		bool gotConnectedCombo;
		int comboIndex = combinationsToDestroy.Count / 3;	// n strings by 3 lists(layers)
		int previousComboIndex;
		int indexStorage;		
		int mainIndexStorage;

		while (comboIndex > 0)
		{
			gotConnectedCombo = false;
			--comboIndex;

			for (int layerIndex = 0; layerIndex < Layers.Count; ++layerIndex)
			{
				previousComboIndex = comboIndex;
				mainIndexStorage = comboIndex * Layers.Count + layerIndex;

				while (previousComboIndex > 0)
				{
					--previousComboIndex;
					indexStorage = previousComboIndex * Layers.Count + layerIndex;

					foreach (int index in combinationsToDestroy[mainIndexStorage])
					{
						if (combinationsToDestroy[indexStorage].Contains (index))
						{
							// TODO merge
							MergeLists (combinationsToDestroy[previousComboIndex * Layers.Count], combinationsToDestroy[comboIndex * Layers.Count]);
							MergeLists (combinationsToDestroy[previousComboIndex * Layers.Count + 1], combinationsToDestroy[comboIndex * Layers.Count + 1]);
							MergeLists (combinationsToDestroy[previousComboIndex * Layers.Count + 2], combinationsToDestroy[comboIndex * Layers.Count + 2]);

							gotConnectedCombo = true;
							break;
						}
					}

					if (gotConnectedCombo)
					{
						break;
					}
				}

				if (gotConnectedCombo)
				{
					break;
				}
			}
		}

		//debug
//		if (firstLayerIndex == 12)
//		{
//			int j = 0;
//			int cn = 0;
//
//			while (j < combinationsToDestroy.Count)
//			{
//				Debug.Log ( "Combo " + cn);
//				for (int i = 0; i < 3; ++i)
//				{					
//					Debug.Log ( "---Layer " + i);
//					foreach (int index in combinationsToDestroy[cn * 3 + i])
//					{
//						Debug.Log ("#####  " + index);
//					}
//				}
//				++cn;
//				j += 3;
//			}
//		}
		//old part
//		layer0ComboIndexes = layer0ComboIndexes.Distinct ().ToList ();
//		layer1ComboIndexes = layer1ComboIndexes.Distinct ().ToList ();
//		layer2ComboIndexes = layer2ComboIndexes.Distinct ().ToList ();
//		candiesToDestroyOne.AddRange (layer0ComboIndexes);
//		candiesToDestroyTwo.AddRange (layer1ComboIndexes);
//		candiesToDestroyThree.AddRange (layer2ComboIndexes);
	}	// the end of RemoveNoncombo

	// old
//	IEnumerator MoveLikeTetris (int layerHeight, float delay, Transform[] layer)//, Vector3 offset)
//	{
//		int i;
//
//		yield return new WaitForSeconds (delay);
//
//		for (int j = 0; j < layer.Length - layerHeight; ++j)
//		{
//			if (layer[j].childCount == 0)
//			{
//				i = j + layerHeight;
//
//				while (i < layer.Length)
//				{
//					if (layer[i].childCount != 0)
//					{
//						//change opacity when move down to active layer
//						//						if (layerNumber == (int)(j / layerHeight))// && layerNumber + layerHeight > j)
//						//						{
//						////							Debug.Log ("LN = " + layerNumber + " | j = " + i);
//						//							layer[i].GetComponent <CandyPoint> ().MoveDownCandy (true, layer[j]);
//						//						}
//						//						else
//						//						{
//						//							layer[i].GetComponent <CandyPoint> ().MoveDownCandy (false, layer[j]);
//						//						}
//
//						if (i - layerHeight > j)
//						{
//							layer[i].GetComponent <CandyPoint> ().MoveDownCandy (false, true, layer[j]);
//						}
//						else
//						{
//							layer[i].GetComponent <CandyPoint> ().MoveDownCandy (false, false, layer[j]);
//						}
////						layer[i].GetComponent <CandyPoint> ().MoveDownCandy (false, MoveDownSpeed, layer[j]);
//						break;
//					}
//
//					if (i >= layer.Length - layerHeight)
//					{
//						if (i - layerHeight > j)
//						{
//							layer[j].GetComponent <CandyPoint> ().SpawnCandy (false, true, CandySpawnDelay);
//						}
//						else
//						{
//							layer[j].GetComponent <CandyPoint> ().SpawnCandy (false, false, CandySpawnDelay);
//						}
////						layer[j].GetComponent <CandyPoint> ().SpawnCandy (false, MoveDownSpeed, CandySpawnDelay);//, offset);
//					}
//					i += layerHeight;
//				}
//			}
//		}
//
//		for (i = layer.Length - layerHeight; i < layer.Length; ++i)
//		{
//			if (layer[i].childCount == 0)
//			{
//				layer[i].GetComponent <CandyPoint> ().SpawnCandy (false, false, CandySpawnDelay);//, offset);
//			}
//		}
////		if (layerNumber <= 0)
////		{
////			for (i = 0; i < layer.Length; ++i)
////			{
////				if (layer[i].childCount == 0)
////				{
////					layer[i].GetComponent <CandyPoint> ().SpawnCandy (false, CandySpawnDelay, offset);
////				}
////			}
////		}
////		else
////		{
////			for (i = 0; i < layer.Length; ++i)
////			{
////				if (layer[i].childCount == 0)
////				{
////					layer[i].GetComponent <CandyPoint> ().SpawnCandy (true, CandySpawnDelay, offset);
////				}
////			}
////		}
//
//		//		if (axis >= 0 && layerNumber > 0)
//		//		{
//		//			ChangeLayersOpacity (axis, layerNumber);
//		//		}
//	}


	IEnumerator DestroyCandiesByIndexesList (List <List <int>> ListOfCombinationsToDestroy)
	{
		int comboIndex = 0;
		int comboNumber = 0;
		float delay = 0f;
		Transform currentCandyPointTransform;
		CandyPoint currentCandyPoint;

		// light up combinations one by one and store the candies objects to destroy at the next step
		while (comboIndex < ListOfCombinationsToDestroy.Count)
		{
			yield return new WaitForSeconds (delay);

			soundController.PlayeNextComboSound (comboNumber);
				
			for (int layerIndex = 0; layerIndex < 3; ++layerIndex)
			{					
				foreach (int index in ListOfCombinationsToDestroy[comboNumber * 3 + layerIndex])
				{
					currentCandyPointTransform = Layers[layerIndex][index];
					currentCandyPoint = currentCandyPointTransform.GetComponent <CandyPoint> ();

					if (currentCandyPointTransform.childCount > 0)
					{
						emptyCandyPoints.Add (currentCandyPoint);
						candiesGOToDestroy.Add (currentCandyPointTransform.GetChild (0).gameObject);
//						SSAToPlay.Add (LayersSSA[layerIndex][index]);
					}
					currentCandyPoint.DespawnCandy ();	// despawn effects etc.
				}
			}

//			ComboScoreCounter (ListOfCombinationsToDestroy[tmpIndex], ListOfCombinationsToDestroy[tmpIndex + 1], ListOfCombinationsToDestroy[tmpIndex + 2]);	// one combo
//			scoreScript.AddScore (comboNumber);
			++comboNumber;
			comboIndex += 3;

			if (delay == 0)
			{
				delay = DelayLightUpNextCombo;
			}
		}

		scoreScript.ComboScoreCounter (ListOfCombinationsToDestroy); // set new score for combinations and set Score variable for FloatingTextPointController.cs instances
//		SSAToPlay = PlaySSA (ListOfCombinationsToDestroy);

		yield return new WaitForSeconds (DelayLightUpNextCombo);//MyGameController.TimeUntilExplosionAfterLastCombo);

		// destroy all combinations at 1 time
//		Vector3 candyPosition;

//		foreach (SpriteSheetController ssac in SSAToPlay)
//		{
//			ssac.PlayRandomAnimation ();
//		}

//		foreach (FloatingTextPointController[] fstLayer in LayersFST)
//		{
//			foreach (FloatingTextPointController fstc in fstLayer)
//			{
//				fstc.FloatText (); // if fst.Score > 0 floating score text will be played
//			}
//		}


//		if (ScoreController.WaveNumber == 0)
//		{
//			foreach (CandyPoint[] layer in candyPointsByLayers)
//			{
//				foreach (CandyPoint cp in layer)
//				{
//					cp.CheckAShape ();
//				}
//			}
//		}

		for (int i = candiesGOToDestroy.Count - 1; i >= 0; --i)
		{	
			if (candiesGOToDestroy[i] != null)
			{
//				candyPosition = candiesGOToDestroy[i].transform.position;
//				Instantiate (DestroyParticlesPrefabs[Random.Range (0, DestroyParticlesPrefabs.Length)], candyPosition, Quaternion.identity);
//				SSAToPlay[i].PlayRandomAnimation ();
				Destroy (candiesGOToDestroy[i]);//, 0.05f);
			}
		}
		soundController.PlayRandomExplosionSound ();

		StartCoroutine (SpawnNewCandies ());
		// put down candies to empty spots from top spots or spawn new ones
//		foreach (Transform[] localLayer in Layers)
//		{
//			MoveLikeTetris ((int)(Mathf.Sqrt (localLayer.Length)), localLayer);

			// TODO barrier synch
//		}

//		opacityScript.SetLayersOpacity ();
	}


	private IEnumerator SpawnNewCandies ()
	{
		int randomIndex;

		yield return new WaitForSeconds (NewCandiesSpawnDelay - CandySpawnDelay);

		while (emptyCandyPoints.Count > 0)
		{
			yield return new WaitForSeconds (CandySpawnDelay);

			randomIndex = Random.Range (0, emptyCandyPoints.Count);
			emptyCandyPoints[randomIndex].SpawnACandy ();
			emptyCandyPoints.RemoveAt (randomIndex);
		}

//		opacityScript.SetLayersOpacity ();
		MyGameController.NextComboWaveCheck = true;
		opacityScript.UpdateOpacity ();
	}
	// old
//	IEnumerator DestroyCandiesByIndexesList (Transform[] layer, List<int> indexes)
//	{
//		bool despawnFlag;
//
//		for (int i = 0; i < indexes.Count; ++i)
//		{			
//			despawnFlag = layer[indexes[i]].GetComponent <CandyPoint> ().DespawnCandy ();
//
//			if (!despawnFlag)
//			{
//				--i;
//			}
//		}
//
//		yield return new WaitForSeconds (NewCandiesSpawnDelay);//MyGameController.TimeOfInlightCandy);
//
//		foreach (Transform[] localLayer in Layers)
//		{
//			MoveLikeTetris ((int)(Mathf.Sqrt (localLayer.Length)), localLayer);
//		}
//	}


//	private void MoveLikeTetris (int layerHeight, Transform[] layer)
//	{
//		bool spawnDelay = false;
//		bool moveDownAddedDelayFlag = false;
////		bool[] rowDelayCheck = new bool[layerHeight];
//		float[] spawnDelays = new float[layerHeight];
//		int i;
//		int rowIndex;
//
//		for (int j = 0; j < layer.Length - layerHeight; ++j)
//		{			
//			rowIndex = j % layerHeight;
//
//			if (layer[j].childCount == 0)
//			{
//				moveDownAddedDelayFlag = false;
//				i = j + layerHeight;
//
//				while (i < layer.Length)
//				{
//					if (layer[i].childCount != 0)
//					{
//						if (i - layerHeight > j)
//						{
//							layer[i].GetComponent <CandyPoint> ().MoveDownCandy (false, true, layer[j]);
//
//							if (!moveDownAddedDelayFlag)
//							{
//								spawnDelays[rowIndex] += CandySpawnDelay;
//								moveDownAddedDelayFlag = true;
////								rowDelayCheck[rowIndex] = true;
//							}
//						}
//						else
//						{
//							layer[i].GetComponent <CandyPoint> ().MoveDownCandy (false, false, layer[j]);
//
//							if (!moveDownAddedDelayFlag)
//							{
//								spawnDelays[rowIndex] += CandySpawnDelay;
//								moveDownAddedDelayFlag = true;
//							}
//						}
//						break;
//					}
//
//					if (i >= layer.Length - layerHeight)
//					{
//						if (i - layerHeight > j)
//						{
//							layer[j].GetComponent <CandyPoint> ().SpawnCandy (true, spawnDelays[rowIndex]);
//							spawnDelays[rowIndex] += CandySpawnDelay;
//						}
//						else
//						{
//							layer[j].GetComponent <CandyPoint> ().SpawnCandy (false, spawnDelays[rowIndex]);
//							spawnDelays[rowIndex] += CandySpawnDelay;
//						}
////						rowDelayCheck[rowIndex] = true;
//					}
//					i += layerHeight;
//				}
//			}
//		}
//
//		for (i = layer.Length - layerHeight; i < layer.Length; ++i)
//		{
//			if (layer[i].childCount == 0)
//			{				
//				rowIndex = i % layerHeight;
////				spawnDelays[rowIndex] += CandySpawnDelay;
//				layer[i].GetComponent <CandyPoint> ().SpawnCandy (false, spawnDelays[rowIndex]);
//			}
//		}
//	}	// the end of MoveLikeTetris ()


	private void CleanUp ()
	{
		candiesTypesByLayersList.Clear ();
//		candiesToDestroyOne.Clear ();
//		candiesToDestroyTwo.Clear ();
//		candiesToDestroyThree.Clear ();
		combinationsToDestroy.Clear ();
		candiesGOToDestroy.Clear ();
		emptyCandyPoints.Clear ();
//		foreach (List <int> layer in emptyCandyPointsIndexes)
//		{
//			layer.Clear ();
//		}
//		SSAToPlay.Clear ();
	}


	// utilities

	// result will be in list1
	private void MergeLists (List <int> list1, List <int> list2)
	{
		var dictionary = list2.ToDictionary (v => v);

		foreach (var item in list1)
		{
			dictionary[item] = item;
		}

		list1 = dictionary.Values.ToList ();
	}
	#endregion


	#region CheckForCombo
	public bool FindAFakeCombo ()
	{
		bool flag = false;
		int otherIndex;
		int layerHeight = (int) Mathf.Sqrt (Layer1.Length);

		for (int pointIndex = 0; pointIndex < Layer1.Length; ++pointIndex)
		{
			// check side layers
			flag = TryToFindCombo (pointIndex, pointIndex, 1, 0);
			if (flag) return flag;	// combo was found
			flag = TryToFindCombo (pointIndex, pointIndex, 1, 2);
			if (flag) return flag;

			// check one-layer combinations
			otherIndex = pointIndex + 1;

			if (otherIndex % layerHeight != 0)
			{
				flag = TryToFindCombo (pointIndex, otherIndex, 0, 0);
				if (flag) return flag;
				flag = TryToFindCombo (pointIndex, otherIndex, 1, 1);
				if (flag) return flag;
				flag = TryToFindCombo (pointIndex, otherIndex, 2, 2);
				if (flag) return flag;
			}

			otherIndex = pointIndex + layerHeight;

			if (otherIndex < Layer1.Length)
			{
				flag = TryToFindCombo (pointIndex, otherIndex, 0, 0);
				if (flag) return flag;
				flag = TryToFindCombo (pointIndex, otherIndex, 1, 1);
				if (flag) return flag;
				flag = TryToFindCombo (pointIndex, otherIndex, 2, 2);
				if (flag) return flag;
			}
		}

		return flag;
	}

	// swaps 2 points and trying to find a possible combo
	public bool TryToFindCombo (int pointIndex1, int pointIndex2, int layerIndex1, int layerIndex2)
	{
		bool flag = false;

		SwapPoints (pointIndex1, pointIndex2, Layers[layerIndex1], Layers[layerIndex2]);
		flag = FindACombo (Layers);
		SwapPoints (pointIndex1, pointIndex2, Layers[layerIndex1], Layers[layerIndex2]);

		return flag;
	}


	public bool CheckSwapability (int pointIndex1, int pointIndex2, int layerIndex1, int layerIndex2)
	{
		bool flag = false;

		if (layerIndex1 == layerIndex2)
		{
			int layerHeight = (int) Mathf.Sqrt (Layer1.Length);	// works for square matrixes only

			if ((pointIndex1 + 1 == pointIndex2 && (pointIndex2 % layerHeight != 0) || 
				(pointIndex1 - 1 == pointIndex2 && pointIndex1 % layerHeight != 0) || 
				pointIndex1 + layerHeight == pointIndex2 || pointIndex1 - layerHeight == pointIndex2))
			{
				flag = true;
			}
		}
		else
		{
			if (Mathf.Abs (layerIndex1 - layerIndex2) < 2) // == 1 -> neighbour layers
			{
				if (pointIndex1 == pointIndex2)
				{
					flag = true;
				}
			}
		}

		return flag;
	}


	// swap candy points in the layer 
	private void SwapPoints (int index1, int index2, Transform[] layer1, Transform[] layer2)
	{
		Transform tmp = layer1[index1];
		layer1[index1] = layer2[index2];
		layer2[index2] = tmp;
	}


//	private List <int> ListOfCandiesTypes (int candyType, Transform[] layer)
//	{
//		List <int> outputIndexesListByCandyType = new List<int> ();
//
//		for (int candyIndex = 0; candyIndex < layer.Length; ++candyIndex)
//		{
//			if (layer[candyIndex].GetComponent <CandyPoint> ().CandyType == candyType)
//			{
//				outputIndexesListByCandyType.Add (candyIndex);
//			}
//		}
//
//		return outputIndexesListByCandyType;
//	}


	// TOFIX with fake parameters // // TOFIX return if combo was found
	private bool FindACombo (List <Transform[]> layers)
	{
		bool returnValue = false;
		List <List <int>> checkList;

		CleanUp ();

		for (int candyShape = 0; candyShape < shapesLength; ++candyShape)	// 0 -spheres, 1 - cubes
		{
			for (int candyType = 0; candyType < colorsLength; ++candyType)
			{			
				for (int layerIndex = 0; layerIndex < layers.Count; ++layerIndex)//, ++ctlIndex)
				{
					checkList = SplitTheLayersCandiesByCombos (candyType, candyShape, layers[layerIndex], layerIndex);

					foreach (List <int> list in checkList)
					{
						if (list.Count > 2) 
						{
							CleanUp ();
							return true;
						}
					}
					candiesTypesByLayersList.Add (checkList);
				}
			}
		}

//		Debug.Log ("###############################");
//		int typeCounter = 0;
//		int comboCounter = 0;
//		foreach (List<List<int>> type in candiesTypesByLayersList)
//		{			
//			Debug.Log (typeCounter);
//			foreach (List<int> combo in type)
//			{
//				Debug.Log ("   combo  :  " + comboCounter);
//				foreach (int index in combo)
//				{
//					Debug.Log ("        index  : " + index);
//				}
//				++comboCounter;
//			}
//			comboCounter = 0;
//			++typeCounter;
//		}
		// now here's N lists of lists in candiesTypesByLayersList (for each candy type and layer) with combos of the same candies
		// check results for 3D combos
		for (int startIndexForCandy = 0; startIndexForCandy < candiesTypesByLayersList.Count; startIndexForCandy += Layers.Count)
		{
			RemoveNoncombo (startIndexForCandy);
//			RemoveNoncomboFake (startIndexForCandy);
		}

		if (combinationsToDestroy.Count > 0)
		{
			returnValue = true;
		}
//		else
//		{
			CleanUp ();
//			return returnValue;
//		}

		return returnValue;
	}

	// TOFIX with fake parameters // TOFIX return if combo was found
//	private void RemoveNoncomboFake (int firstLayerIndex)
//	{
//		//		candiesTypesByLayersList
//		bool list0WasAdded;
//		bool list1WasAdded;
//		bool list2WasAdded;
//		int secondLayerIndex = firstLayerIndex + 1;
//		int thirdLayerIndex = firstLayerIndex + 2;
//		int countSum;
//		List <int> layer0ComboIndexes = new List<int>();
//		List <int> layer2ComboIndexes = new List<int>();
//		List <int> layer1ComboIndexes = new List<int>();
//		List <int> doubdfulComboIndexesL0 = new List<int> (); // if (countSum = 2) => 1 element in layers 1 and 0
//		List <int> doubdfulComboIndexesL2 = new List<int> (); // if (countSum = 2) => 1 element in layers 1 and 2
//
//		foreach (List<int> listInLayer1 in candiesTypesByLayersList[secondLayerIndex])
//		{
//			if (listInLayer1.Count > 2)
//			{
//				layer1ComboIndexes.AddRange (listInLayer1);
//				list1WasAdded = true;
//			}
//			else
//			{
//				list1WasAdded = false;
//			}
//
//			for (int s = 0; s < listInLayer1.Count; ++s)
//			{
//				foreach (List<int> listInLayer0 in candiesTypesByLayersList[firstLayerIndex])
//				{
//					if (listInLayer0.Count > 2)
//					{
//						layer0ComboIndexes.AddRange (listInLayer0);
//						list0WasAdded = true;
//					}
//					else
//					{
//						list0WasAdded = false;
//					}
//
//					if (listInLayer0.Contains (listInLayer1[s]))
//					{						
//						countSum = listInLayer0.Count + listInLayer1.Count;
//
//						if (countSum > 2)
//						{
//							if (!list0WasAdded)
//							{
//								layer0ComboIndexes.AddRange (listInLayer0);
//								list0WasAdded = true;
//							}
//							if (!list1WasAdded)
//							{
//								layer1ComboIndexes.AddRange (listInLayer1);
//								list1WasAdded = true;
//							}
//						}
//						else if (countSum == 2)
//						{
//							doubdfulComboIndexesL0.Add (listInLayer1[s]);
//						}
//					}
//				}
//
//				foreach (List<int> listInLayer2 in candiesTypesByLayersList[thirdLayerIndex])
//				{
//					if (listInLayer2.Count > 2)
//					{
//						layer2ComboIndexes.AddRange (listInLayer2);
//						list2WasAdded = true;
//					}
//					else
//					{
//						list2WasAdded = false;
//					}
//
//					if (listInLayer2.Contains (listInLayer1[s]))
//					{						
//						countSum = listInLayer2.Count + listInLayer1.Count;
//
//						if (countSum > 2 || list1WasAdded)
//						{
//							if (!list2WasAdded)
//							{
//								layer2ComboIndexes.AddRange (listInLayer2);
//								list2WasAdded = true;
//							}
//							if (!list1WasAdded)
//							{
//								layer1ComboIndexes.AddRange (listInLayer1);
//								list1WasAdded = true;
//							}
//						}
//						else if (countSum == 2)
//						{
//							doubdfulComboIndexesL2.Add (listInLayer1[s]);
//						}
//					}
//				}
//			}
//
//			// add combo to destroy-candies list
//			if (layer0ComboIndexes.Count != 0 || layer1ComboIndexes.Count != 0 || layer2ComboIndexes.Count != 0)
//			{
//				layer0ComboIndexes = layer0ComboIndexes.Distinct ().ToList ();
//				layer1ComboIndexes = layer1ComboIndexes.Distinct ().ToList ();
//				layer2ComboIndexes = layer2ComboIndexes.Distinct ().ToList ();
//				combinationsToDestroy.Add (new List<int> (layer0ComboIndexes));
//				combinationsToDestroy.Add (new List<int> (layer1ComboIndexes));
//				combinationsToDestroy.Add (new List<int> (layer2ComboIndexes));
//				layer0ComboIndexes.Clear ();
//				layer1ComboIndexes.Clear ();
//				layer2ComboIndexes.Clear ();
//			}
//		}			
//
//		if (candiesTypesByLayersList[secondLayerIndex].Count == 0)
//		{
//			List <int> empty = new List<int> ();
//
//			foreach (List<int> listInLayer0 in candiesTypesByLayersList[firstLayerIndex])
//			{
//				if (listInLayer0.Count > 2)
//				{
//					combinationsToDestroy.Add (new List<int> (listInLayer0));
//					combinationsToDestroy.Add (empty);
//					combinationsToDestroy.Add (empty);
//				}
//			}
//			foreach (List<int> listInLayer2 in candiesTypesByLayersList[thirdLayerIndex])
//			{
//				if (listInLayer2.Count > 2)
//				{
//					//					layer2ComboIndexes.AddRange (listInLayer2);
//					combinationsToDestroy.Add (empty);
//					combinationsToDestroy.Add (empty);
//					combinationsToDestroy.Add (new List<int> (listInLayer2));
//				}
//			}
//			layer2ComboIndexes.Clear ();
//		}
//
//		// check doubdful combos
//		foreach (int index in doubdfulComboIndexesL0)
//		{
//			if (doubdfulComboIndexesL2.Contains (index))
//			{
//				combinationsToDestroy.Add (new List<int> () {index});
//				combinationsToDestroy.Add (new List<int> () {index});
//				combinationsToDestroy.Add (new List<int> () {index});
//			}
//		}
//
//		// reflat // merging combinations connected through neighbour layers 
//		bool gotConnectedCombo;
//		int comboIndex = combinationsToDestroy.Count / 3;	// n strings by 3 lists(layers)
//		int previousComboIndex;
//		int indexStorage;		
//		int mainIndexStorage;
//
//		while (comboIndex > 0)
//		{
//			gotConnectedCombo = false;
//			--comboIndex;
//
//			for (int layerIndex = 0; layerIndex < Layers.Count; ++layerIndex)
//			{
//				previousComboIndex = comboIndex;
//				mainIndexStorage = comboIndex * Layers.Count + layerIndex;
//
//				while (previousComboIndex > 0)
//				{
//					--previousComboIndex;
//					indexStorage = previousComboIndex * Layers.Count + layerIndex;
//
//					foreach (int index in combinationsToDestroy[mainIndexStorage])
//					{
//						if (combinationsToDestroy[indexStorage].Contains (index))
//						{
//							MergeLists (combinationsToDestroy[previousComboIndex * Layers.Count], combinationsToDestroy[comboIndex * Layers.Count]);
//							MergeLists (combinationsToDestroy[previousComboIndex * Layers.Count + 1], combinationsToDestroy[comboIndex * Layers.Count + 1]);
//							MergeLists (combinationsToDestroy[previousComboIndex * Layers.Count + 2], combinationsToDestroy[comboIndex * Layers.Count + 2]);
//
//							gotConnectedCombo = true;
//							break;
//						}
//					}
//
//					if (gotConnectedCombo)
//					{
//						break;
//					}
//				}
//
//				if (gotConnectedCombo)
//				{
//					break;
//				}
//			}
//		}
//	}	// the end of RemoveNoncombo


	public int GetCPIndexInTheLayer (int layerIndex, Transform candyPoint)
	{
		return System.Array.IndexOf (Layers[layerIndex], candyPoint);
	}
	#endregion
}
