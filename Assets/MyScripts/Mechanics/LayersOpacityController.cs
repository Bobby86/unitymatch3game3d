﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class LayersOpacityController : MonoBehaviour 
{
	#region Public References
	public static LayersOpacityController Instance;

	[HideInInspector]public int CurrentAxis = -1;	// no axis selected - full alpha
	[HideInInspector]public int XLayerIndexWithFullOpacity;
	[HideInInspector]public int YLayerIndexWithFullOpacity;
	[HideInInspector]public int ZLayerIndexWithFullOpacity;
	#endregion


	#region Private References
	private bool lockWhileChange = false;
	private int layerLength;
	private int strColLength; // length of the string or column in the layer
	private int layerIndexWithFullOpacity;
	private Transform[] layerOneRef;
	private Transform[] layerTwoRef;
	private Transform[] layerThreeRef;
	private List <Transform[]> layers = new List <Transform[]> ();
//	private CandyPoint[] LayerOneCMPointsScripts;
//	private CandyPoint[] LayerTwoCMPointsScripts;
//	private CandyPoint[] LayerThreeCMPointsScripts;
//	private List <CandyPoint[]> cpsArraysList = new List<CandyPoint[]> (); 
	#endregion


	#region Unity Callbacks
	void Awake ()
	{
		Instance = this;
	}


	void Start () 
	{			
		layerLength = ComboManager.LayerLength;
		layerOneRef = ComboManager.Instance.Layer1;
		layerTwoRef = ComboManager.Instance.Layer2;
		layerThreeRef = ComboManager.Instance.Layer3;
		strColLength = (int) Mathf.Sqrt (layerLength); // for square matrix only
		layers.Add (layerOneRef);
		layers.Add (layerTwoRef);
		layers.Add (layerThreeRef);

		XLayerIndexWithFullOpacity = 0;
		YLayerIndexWithFullOpacity = 2;
		ZLayerIndexWithFullOpacity = 2;
//		LayerOneCMPointsScripts = InitCMScriptsArray (ComboManager.Instance.Layer1);
//		LayerTwoCMPointsScripts = InitCMScriptsArray (ComboManager.Instance.Layer2);
//		LayerThreeCMPointsScripts = InitCMScriptsArray (ComboManager.Instance.Layer3);

//		cpsArraysList.Add (LayerOneCMPointsScripts);
//		cpsArraysList.Add (LayerTwoCMPointsScripts);
//		cpsArraysList.Add (LayerThreeCMPointsScripts);
		SetLayersOpacityFull ();
//		ChangeXLayerOpacity (2);	
//		ChangeYLayerOpacity (2);
//		ChangeZLayerOpacity (2);
	}
	#endregion


//	#region Private Methods
//	private CandyPoint[] InitCMScriptsArray (Transform[] layer)
//	{		
//		CandyPoint[] scriptsArray = new ChangeMaterial[ComboManager.LayerLength];
//
//		for (int pointIndex = 0; pointIndex < layer.Length; ++pointIndex)
//		{
//			scriptsArray[pointIndex] = layer[pointIndex].GetComponent <CandyPoint> ();
//		}
//
//		return scriptsArray;
//	}
//	#endregion


	#region Public Methods
	public void SetLayersOpacity (int axis = -100, int layer = -1)
	{
		if (!lockWhileChange)
		{
			lockWhileChange = true;

			if (axis > -99)
			{
				CurrentAxis = axis;
			}

			layerIndexWithFullOpacity = layer;
	//		ChangeXLayerOpacity ();
	//		ChangeYLayerOpacity ();
	//		ChangeZLayerOpacity ();

			// Warning! Z and X are switched
			switch (CurrentAxis)
			{
			case -1:
				SetLayersOpacityFull ();
				break;

			case 0:
				ChangeZLayerOpacity (layer);
				break;

			case 1:
				ChangeYLayerOpacity (layer);
				break;

			case 2:
				ChangeXLayerOpacity (layer);
				break;
			}
		}
	}

	public void UpdateOpacity ()
	{
		if (CurrentAxis > -1)
		{
			if (!lockWhileChange)
			{
				lockWhileChange = true;

				// Warning! Z and X are switched
				switch (CurrentAxis)
				{
				case 0:
					ChangeZLayerOpacity (layerIndexWithFullOpacity);
					break;

				case 1:
					ChangeYLayerOpacity (layerIndexWithFullOpacity);
					break;

				case 2:
					ChangeXLayerOpacity (layerIndexWithFullOpacity);
					break;
				}
			}
		}
	}

	private void SetLayersOpacityFull ()
	{
		foreach (Transform[] layer in layers)
		{
			foreach (Transform candyPoint in layer)
			{
				if (candyPoint.childCount != 0)
				{					
					candyPoint.GetChild (0).GetComponent <ChangeMaterial> ().ChangeOpacity (true);
				}
			}
		}

		lockWhileChange = false;
	}


	public void ChangeXLayerOpacity (int layerIndex = -1)
	{
		bool opacity = false;

		if (layerIndex < 0)
		{
			layerIndex = XLayerIndexWithFullOpacity;
		}
		else
		{
			XLayerIndexWithFullOpacity = layerIndex;
		}

		for (int currentLayerIndex = 0; currentLayerIndex < layers.Count; ++currentLayerIndex)
		{
			if (currentLayerIndex != layerIndex)
			{
				opacity = false;
			}
			else
			{
				opacity = true;
			}

			foreach (Transform candyPoint in layers[currentLayerIndex])
			{
				if (candyPoint.childCount != 0)
				{					
					candyPoint.GetChild (0).GetComponent <ChangeMaterial> ().ChangeOpacity (opacity);//, 0); // change x layer opacity
				}
			}
		}

		lockWhileChange = false;
	}


	public void ChangeZLayerOpacity (int layerIndex = -1)
	{
		// change columns opacity in each layer
		bool opacity = false;
//		int currentPointIndex = layerIndex;

		if (layerIndex < 0)
		{			
			layerIndex = ZLayerIndexWithFullOpacity;
//			Debug.Log (layerIndex);
		}
		else
		{
			ZLayerIndexWithFullOpacity = layerIndex;
		}
//		foreach (Transform[] layer in layers)
//		{
//			while (currentPointIndex < layerLength)
//			{
//				layer[currentPointIndex].GetChild (0).GetComponent <ChangeMaterial> ().ChangeOpacity (toFull, 1); // change y layer opacity
//				currentPointIndex += strColLength;
//			}
		for (int pointIndex = 0; pointIndex < layerLength; ++pointIndex)
		{
			if (pointIndex % strColLength != layerIndex)
			{
				opacity = false;
			}
			else
			{
				opacity = true;
			}

			if (layerOneRef[pointIndex].childCount != 0)
			{				
				layerOneRef[pointIndex].GetChild (0).GetComponent <ChangeMaterial> ().ChangeOpacity (opacity);//, 2); // change z layer opacity
			}
			if (layerTwoRef[pointIndex].childCount != 0)
			{				
				layerTwoRef[pointIndex].GetChild (0).GetComponent <ChangeMaterial> ().ChangeOpacity (opacity);//, 2); // change z layer opacity
			}
			if (layerThreeRef[pointIndex].childCount != 0)
			{
				layerThreeRef[pointIndex].GetChild (0).GetComponent <ChangeMaterial> ().ChangeOpacity (opacity);//, 2); // change z layer opacity
			}
//				foreach (Transform[] layer in layers)
//				{
//					candyPoint.GetChild (0).GetComponent <ChangeMaterial> ().ChangeOpacity (opacity, 1); // change y layer opacity
//				}
		}
//		}
		lockWhileChange = false;
	}


	public void ChangeYLayerOpacity (int layerIndex = -1)
	{
		// change strings opacity in each layer
		bool opacity = false;

		if (layerIndex < 0)
		{
			layerIndex = YLayerIndexWithFullOpacity;
		}
		else
		{
			YLayerIndexWithFullOpacity = layerIndex;
		}

		layerIndex = layerIndex * strColLength;
		int finalPointIndex = layerIndex + strColLength - 1;

//		foreach (Transform[] layer in layers)
//		{
		for (int pointIndex = 0; pointIndex < layerLength; ++pointIndex)
		{
			if (pointIndex < layerIndex || pointIndex > finalPointIndex)
			{
				opacity = false;
			}
			else
			{
				opacity = true;
			}

			if (layerOneRef[pointIndex].childCount != 0)
			{
				layerOneRef[pointIndex].GetChild (0).GetComponent <ChangeMaterial> ().ChangeOpacity (opacity);//, 1); // change y layer opacity
			}
			if (layerTwoRef[pointIndex].childCount != 0)
			{
				layerTwoRef[pointIndex].GetChild (0).GetComponent <ChangeMaterial> ().ChangeOpacity (opacity);//, 1); // change y layer opacity
			}
			if (layerThreeRef[pointIndex].childCount != 0)
			{
				layerThreeRef[pointIndex].GetChild (0).GetComponent <ChangeMaterial> ().ChangeOpacity (opacity);//, 1); // change y layer opacity
			}

		}
//			while (currentPointIndex % strColLength > 0)
//			{
//				layer[currentPointIndex - 1].GetChild (0).GetComponent <ChangeMaterial> ().ChangeOpacity (toFull, 2); // change z layer opacity
//				++currentPointIndex;
//			}
//		}
		lockWhileChange = false;
	}
	#endregion
}
