﻿using UnityEngine;
using System.Collections;


public class HoverAxis : MonoBehaviour 
{
	#region Public References
	public static int CurrentlyHoveredAxis = -1;
//	public static int CurrentXLayerIndex = 0;
//	public static int CurrentYLayerIndex = 0;
//	public static int CurrentZLayerIndex = 0;
	#endregion


	#region Private References
	private int axisIndex; // x, y, z ~ 0, 1, 2
	private CandyPoints2DLayoutModel scriptWithCandiesOpacity;
	#endregion


	#region Unity Callbacks
	void Start () 
	{
		string thisName = gameObject.name;

		if (thisName.Contains ("X"))
		{
			axisIndex = 0;
		}
		else if (thisName.Contains ("Y"))
		{
			axisIndex = 1;
		}
		else if (thisName.Contains ("Z"))
		{
			axisIndex = 2;
		}
		else if (thisName.Contains ("T"))
		{
			axisIndex = -2;
		}

		scriptWithCandiesOpacity = MyGameController.gameController.GetComponent <CandyPoints2DLayoutModel> ();
	}


	void OnMouseEnter ()
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

		if (CurrentlyHoveredAxis != axisIndex)
		{
			CurrentlyHoveredAxis = axisIndex;

//			switch (axisIndex)
//			{
//			case -2:
//				scriptWithCandiesOpacity.SetOpacityToFull ();
//				break;
//			}
		}
	}
	#endregion


	#region Private Methods

	#endregion
}
