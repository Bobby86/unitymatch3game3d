﻿using UnityEngine;
using System.Collections;

// detect a layer of a mouseovered candy and set the layer opacity for this layer's axis
public class CandyLayerDetection : MonoBehaviour 
{
	#region Private References
	private int xLayerIndexOfThisCandy;
	private int yLayerIndexOfThisCandy;
	private int zLayerIndexOfThisCandy;
	#endregion


	#region Unity Callbacks
	void Start () 
	{
		
	}
	#endregion


	#region Public Methods
	public void SetCoordinates (int x, int y, int z)
	{
		xLayerIndexOfThisCandy = x;
		yLayerIndexOfThisCandy = y;
		zLayerIndexOfThisCandy = z;
	}
	#endregion
}
