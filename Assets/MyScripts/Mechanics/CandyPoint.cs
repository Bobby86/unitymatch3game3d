﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CandyPoint : MonoBehaviour 
{
	#region Public References
	public static float CandySpawnDelay;

	public bool isSphere = false;
	public GameObject SelectedPS;	// particle system to turn on when this point element are selected

	[HideInInspector]public int PointShape;
	[HideInInspector]public int CandyShape; // shape (sphere, cube)
	[HideInInspector]public int CandyType; // color

//	public float YSpawnCoordinate = 2.5f;	//local
	[HideInInspector]public int PointLayer;
	[HideInInspector]public int XLayerIndex;
	[HideInInspector]public int YLayerIndex;
	[HideInInspector]public int ZLayerIndex;
	[HideInInspector]public int CubeOrSphere; // 0 - sphere; 1 - cube
//	public GameObject DespawnParticles;
	#endregion


	#region Private References
//	private bool moveDown;
	private int candiesLength;
	private float spawnDelay = .7f;
	private Transform newFParent;
	private Vector3 candyPosition;
	private Quaternion candyRotation;
	private MyGameController myGameController;
	private GameObject currentCandy;
	private ParticleSystem despawnParticles;
	private LayersOpacityController opacityScript;
	private List<GameObject> prefabsReference;
	#endregion


	#region Unity Callbacks
	void OnEnable ()
	{
		DifficultyController.OnDifficultyLevelUp += DifficultyLevelUpAction;
	}

	void OnDisable ()
	{
		DifficultyController.OnDifficultyLevelUp -= DifficultyLevelUpAction;
	}

	void Start ()
	{
//		Transform localCandyTransform;

		myGameController = FindObjectOfType (typeof (MyGameController)) as MyGameController;
//		localCandyTransform = transform.GetChild (0);
//		currentCandy = localCandyTransform.gameObject;
		candyPosition = transform.position;//new Vector3 (transform.position.x, YSpawnCoordinate, transform.position.z);//transform.position;//localCandyTransform.position;
		candyRotation = transform.rotation;
		despawnParticles = GetComponent <ParticleSystem> ();
		candiesLength = myGameController.SpherePrefabs.Count;

		if (despawnParticles.isPlaying)
		{
			despawnParticles.Stop ();
		}
		despawnParticles.startDelay = CandySpawnDelay - 0.1f;
		SetPointShape ();
//		SetPointType ();
		SetPointLayersIndexes ();

//		switch (CandyShape)
//		{
//		case 0:
//			prefabsReference = myGameController.SpherePrefabs;
//			break;
//
//		case 1:
//			prefabsReference = myGameController.CubePrefabs;
//			break;
//		}

		if (isSphere)
		{
			prefabsReference = myGameController.SpherePrefabs;
		}
		else
		{
			prefabsReference = myGameController.CubePrefabs;
		}

		opacityScript = LayersOpacityController.Instance;
//		SetCandyOpacity ();
	}
	#endregion


	#region Public Methods
	// if the shape of the point type does not match to the element shape
//	public void CheckAShape ()
//	{
//		if (transform.childCount != 0)
//		{
//			Transform currentChild = transform.GetChild (0);
//			int shape = 1;	// debug
//
//			if (currentChild.name.Contains("Sp")) // "Sphere"
//			{
//				shape = 0;
//			}
//				
//			if (PointShape != shape)
//			{
//				currentChild.parent = transform.root;
//				currentChild.GetComponent <ChangeMaterial> ().DeleteThis ();
//				SpawnACandy (false);	// spawn the same colored right sphere
//			}
////			Transform currentChild = transform.GetChild (0);
////
////			switch (CandyShape)
////			{
////			case 0:
////				if (!currentChild.name.Contains ("Sphere"))
////				{
////					currentChild.parent = transform.root;
////					currentChild.GetComponent <ChangeMaterial> ().DeleteThis ();
////					SpawnACandy (false);	// spawn the same colored sphere
////					Debug.Log (currentChild.name);
////				}
////				break;
////
////			case 1:
////				if (currentChild.name.Contains ("Sphere"))
////				{
////					currentChild.parent = transform.root;
////					currentChild.GetComponent <ChangeMaterial> ().DeleteThis ();
////					SpawnACandy (false);	// spawn the same colored cube
////					Debug.Log (currentChild.name);
////				}
////				break;
////			}
//		}
//	}


	public void SpawnACandy ()//bool RandomColored = true)
	//	private IEnumerator SpawnACandy (bool doubleMoveDownSpeed, float spawnDelay)
	{	
//		if (RandomColored)	// change a color
//		{
		CandyType = Random.Range (0, candiesLength);
//		}
//		else	// gotta change a shape only
//		{
//			CandyShape = PointShape;
//			CandyShape = (++CandyShape) % 2;	// !!! 2 shapes available for now // it's been changed in SwapCandies()
////			CandyType = candyType;
////			Debug.Log (CandyType);
//		}

		if (transform.childCount == 0)
		{
			currentCandy = Instantiate (prefabsReference[CandyType], candyPosition, candyRotation) as GameObject;
			currentCandy.transform.parent = transform;

			if (CandyShape != PointShape)
			{
				CandyShape = PointShape;
			}
//			if (RandomColored)	// only when truely swap elements
//			{	
//				currentCandy.GetComponent <DragScript> ().Swapped = true;
//				currentCandy.GetComponent <ChangeMaterial> ().ChangeOpacity (true);
//			}
//			else
//			{
			SetCandyOpacity (currentCandy);
			SetPointType ();
//			}
		}
	}


	public bool DespawnCandy ()
	{
		if (transform.childCount != 0)
		{
			//			Destroy (transform.GetChild(0).gameObject);
			Transform child = transform.GetChild(0);
			child.parent = transform.root;
			child.GetComponent <ChangeMaterial> ().LightUp ();
			child.GetComponent <NotSimpleScaler> ().FinishScale (myGameController.AddScale);
//			PlayDespawnParticles ();
			//			Debug.Log ("000");
		}
		else
		{
			//			return false;
			//			Debug.Log ("111");
		}

		return true;
	}


	//	public void SpawnCandy (bool doubleMoveDownSpeed, float spawnDelay)
	//	{
	//
	//		StartCoroutine (SpawnACandy (doubleMoveDownSpeed, spawnDelay));
	//	}


	//	public void SpawnCandy (bool setOpacity, bool doubleMoveDownSpeed, float spawnTime)
	//	{
	//		StartCoroutine (SpawnACandy (setOpacity, doubleMoveDownSpeed, spawnTime));
	//	}


	public bool CheckChildPosition ()
	{
		if (transform.childCount == 0)
		{
			return false;
		}

//		if (Vector3.Distance (transform.position, transform.GetChild (0).position) > 0.1f)
//		{
//			return false;
//		}

		return true;
	}


	public void TurnSelectedParticles (bool turnOn)
	{
		SelectedPS.SetActive (turnOn);
	}
	#endregion


	#region Private Methods
	private void SetCandyOpacity (GameObject candy = null)
	{
		if (transform.childCount == 0)
		{
			Debug.Log ("Error CandyPoint SetCandyOpacity 000");
			return;
		}

		if (candy == null)
		{
			candy = transform.GetChild (0).gameObject;

			if (candy == null)
			{
				Debug.Log ("Error CandyPoint SetCandyOpacity 111");
				return;
			}
		}

		if (opacityScript.CurrentAxis == -1)
		{
			candy.GetComponent <ChangeMaterial> ().ChangeOpacity (true);	// TODO
//			Debug.Log ("00");
			return;
		}

		if (XLayerIndex != opacityScript.ZLayerIndexWithFullOpacity)
		{
			candy.GetComponent <ChangeMaterial> ().ChangeOpacity (false);//, 0);
//			Debug.Log ("11");
		}
		else
		{
			candy.GetComponent <ChangeMaterial> ().ChangeOpacity (true);//, 0);
//			Debug.Log ("22");
		}

		if (YLayerIndex != opacityScript.YLayerIndexWithFullOpacity)
		{
			candy.GetComponent <ChangeMaterial> ().ChangeOpacity (false);//, 1);
//			Debug.Log ("33");
		}
		else
		{
			candy.GetComponent <ChangeMaterial> ().ChangeOpacity (true);//, 1);
//			Debug.Log ("44");
		}

		if (ZLayerIndex != opacityScript.XLayerIndexWithFullOpacity)
		{
			candy.GetComponent <ChangeMaterial> ().ChangeOpacity (false);//, 2);
//			Debug.Log ("55");
		}
		else
		{
			candy.GetComponent <ChangeMaterial> ().ChangeOpacity (true);//, 2);
//			Debug.Log ("66");
		}
	}


//	IEnumerator SpawnACandy (bool setOpacity, bool doubleMoveDownSpeed, float spawnTime)
//	{	
//		CandyType = Random.Range (0, candiesLength);
//		yield return new WaitForSeconds (spawnTime);
//		//		currentCandy = Instantiate (myGameController.CandiesPrefabs[CandyType], candyPosition, candyRotation) as GameObject;
//		if (transform.childCount == 0)
//		{
//			currentCandy = Instantiate (prefabsReference[CandyType], candyPosition, candyRotation) as GameObject;
//			currentCandy.transform.parent = transform;
//			SetCandyOpacity (currentCandy);
////			currentCandy.GetComponent <DragScript> ().CandyType = GetCandyType ();
//			SetPointType ();
//
////			if (setOpacity)
////			{
////				currentCandy.GetComponent <ChangeMaterial> ().ChangeOpacity (false); // change material to faded
////			}
//
////			if (moveDown)
////			{			
////				MoveDownCandy (false, doubleMoveDownSpeed, newFParent);
////				moveDown = false;
////			}
//		}
//	}


	private void PlayDespawnParticles ()
	{
		if (despawnParticles.isStopped)
		{
			despawnParticles.Play ();
		}
	}

//	public void MoveDownCandy (bool changeOpacity, bool doubleMoveDownSpeed, Transform newParent)
//	{
//		if (transform.childCount != 0)
//		{
//			Transform candy = transform.GetChild (0);
//			newParent.GetComponent <CandyPoint> ().CandyType = CandyType;
//			candy.parent = newParent;
//			candy.GetComponent <DragScript> ().MoveToParent (doubleMoveDownSpeed);
////			newParent.GetComponent <CandyPoint> ().SetCandyOpacity (candy.gameObject);
////			if (changeOpacity)
////			{
////				candy.GetComponent <ChangeMaterial> ().ChangeOpacity (true);
////			}
//		}
//		else
//		{
//			newFParent = newParent;
//			moveDown = true;
//		}
//	}


	private void SetPointShape ()
	{
//		if (gameObject.name.Contains ("s"))
//		{
//			CandyShape = 0;
//		}
//		else
//		{
//			CandyShape = 1;
//		}
		if (isSphere)
		{
			CandyShape = 0;
		}
		else
		{
			CandyShape = 1;
		}

		PointShape = CandyShape;
	}


	// set shape and current color
	private void SetPointType ()
	{
		string candyName = currentCandy.name;

		if (candyName.Contains ("Blue"))
		{
			CandyType = 0;
		}
		else if (candyName.Contains ("Green"))
		{
			CandyType = 1;
		}
		else if (candyName.Contains ("Red"))
		{
			CandyType = 2;
		}
		else if (candyName.Contains ("Yellow"))
		{
			CandyType = 3;
		}
		/* difficulty up level */
		else if (candyName.Contains ("Orange"))
		{
			CandyType = 4;
		}
		else if (candyName.Contains ("Purple"))
		{
			CandyType = 5;
		}

		// gameover tests
//		else if (candyName.Contains ("Cube0"))
//		{
//			CandyType = 6;
//		}
//		else if (candyName.Contains ("Cube1"))
//		{
//			CandyType = 7;
//		}
//		else if (candyName.Contains ("Cube2"))
//		{
//			CandyType = 8;
//		}
//		else if (candyName.Contains ("Cube3"))
//		{
//			CandyType = 9;
//		}
//		else if (candyName.Contains ("Cube4"))
//		{
//			CandyType = 10;
//		}
//		else if (candyName.Contains ("Cube5"))
//		{
//			CandyType = 11;
//		}
//		else
//		{
//			CandyType = -1;
//		}
	}


	private void SetPointLayersIndexes ()
	{
		string pointName = gameObject.name;

		if (pointName.Contains ("B"))
		{
			PointLayer = 0;
			XLayerIndex = 0;
		}
		else if (pointName.Contains ("C"))
		{
			PointLayer = 1;
			XLayerIndex = 1;
		}
		else  if (pointName.Contains ("D"))
		{
			PointLayer = 2;
			XLayerIndex = 2;
		}

		if (pointName.Contains ("1"))
		{
			ZLayerIndex = 0;
		}
		else if (pointName.Contains ("2"))
		{
			ZLayerIndex = 1;
		}
		else if (pointName.Contains ("3"))
		{
			ZLayerIndex = 2;
		}

		if (pointName.Contains ("III"))
		{
			YLayerIndex = 2;
//			CubeOrSphere = 0;
		}
		else if (pointName.Contains ("II"))
		{
			YLayerIndex = 1;
//			CubeOrSphere = 1;
		}
		else  if (pointName.Contains ("I"))
		{
			YLayerIndex = 0;
//			CubeOrSphere = 0;
		}
	}


	private void DifficultyLevelUpAction (bool newShapesAdded = false)
	{
		if (newShapesAdded)
		{
//			switch (PointShape)
//			{
//			case 0:
//				prefabsReference = myGameController.SpherePrefabs;
//				break;
//
//			case 1:
//				prefabsReference = myGameController.CubePrefabs;
//				break;
//			}

			if (isSphere)
			{
				prefabsReference = myGameController.SpherePrefabs;
			}
			else
			{
				prefabsReference = myGameController.CubePrefabs;
			}
		}

		candiesLength = prefabsReference.Count;
	}
	// set cube or sphere type
//	private void SetPointType ()
//	{
//		string pointName = gameObject.name;
//
//		if (pointName.Contains ("III"))
//		{
//			CubeOrSphere = 0;
//		}
//		else if (pointName.Contains ("II"))
//		{
//			CubeOrSphere = 1;
//		}
//		else
//		{
//			CubeOrSphere = 0;
//		}
//	}
	#endregion

}
