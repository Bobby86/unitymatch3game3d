﻿using UnityEngine;
using System.Collections;

public class SimpleScaler : MonoBehaviour 
{
	public float ScaleTime = 0.3f;
	public static Vector3 AddScale;

	private bool scale = false;
	private float currentLerpTime = 0f;
	private float step;


	void LateUpdate () 
	{
		if (MyGameController.gameController != null && MyGameController.GState == GameState.Pause)
		{
			return;
		}

		if (scale)
		{
			currentLerpTime += Time.deltaTime;

			if (currentLerpTime > ScaleTime)
			{
				scale = false;
				return;
			}

			step = currentLerpTime / ScaleTime;
			transform.localScale = Vector3.Lerp (transform.localScale, AddScale, step);
		}
	}


	public void ScaleUp ()
	{
		AddScale += transform.localScale;
		scale = true;
	}
}
