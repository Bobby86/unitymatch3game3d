﻿using UnityEngine;
using System.Collections;


public class UISpriteSheetController : MonoBehaviour 
{
	#region Public References
	public static int SSAcount = 4;
	public static float SSASpeed;
	#endregion


	#region Private References
	private int animatorIndex = 0;
	private Animator[] animators;
	#endregion


	#region Unity CallBacks
	void Start () 
	{
		animators = new Animator[SSAcount];
		animators = GetComponentsInChildren <Animator> ();

		for (int i = 0; i < animators.Length; ++i)
		{
			animators[i].speed = SSASpeed;
		}
	}
	#endregion


	#region Public Methods
	public void PlayRandomAnimation ()
	{
		animatorIndex = Random.Range (0, SSAcount);
		animators[animatorIndex].gameObject.SetActive (true);
		animators[animatorIndex].SetTrigger ("PlayTrigger");
	}
	#endregion
}
