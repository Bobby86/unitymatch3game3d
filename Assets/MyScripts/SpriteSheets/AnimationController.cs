﻿using UnityEngine;
using System.Collections;


public class AnimationController : MonoBehaviour 
{
	private SpriteRenderer sr;


	void Awake ()
	{
		sr = GetComponent <SpriteRenderer> ();
	}


	public void TurnOnSpriteRenderer ()
	{
		sr.enabled = true;
	}


	public void TurnOffSpriteRenderer ()
	{		
		sr.enabled = false;
		gameObject.SetActive (false);
	}
}
