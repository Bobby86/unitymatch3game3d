﻿using UnityEngine;
using System.Collections;


public class SpriteSheetController : MonoBehaviour 
{
	public static int SSAcount = 4;
	public static float SSASpeed;

	public Animator[] ssaX;
	public Animator[] ssaY;
	public Animator[] ssaZ;
//	[HideInInspector] public bool PlayFloatingScore = false; // changes from ScoreController

	private int currentIndex;
//	private FloatingTextPointController ftControllerScript;


	void Start ()
	{
		int xIndex = 0;
		int yIndex = 0;
		int zIndex = 0;

		ssaX = new Animator[SSAcount];
		ssaY = new Animator[SSAcount];
		ssaZ = new Animator[SSAcount];
//		srX = new SpriteRenderer[SSAcount];
//		srY = new SpriteRenderer[SSAcount];
//		srZ = new SpriteRenderer[SSAcount];

		foreach (Transform child in transform)
		{			
			if (child.name.Contains ("X"))
			{
				ssaX[xIndex] = child.GetComponent <Animator> ();
				ssaX[xIndex].speed = SSASpeed;
				ssaX[xIndex].gameObject.SetActive (false);
//				srX[xIndex] = child.GetComponent <SpriteRenderer> ();
				++xIndex;
			}
			else if (child.name.Contains ("Y"))
			{
				ssaY[yIndex] = child.GetComponent <Animator> ();
				ssaY[yIndex].speed = SSASpeed;
				ssaY[yIndex].gameObject.SetActive (false);
//				srY[yIndex] = child.GetComponent <SpriteRenderer> ();
				++yIndex;
			}
			else if (child.name.Contains ("Z"))
			{
				ssaZ[zIndex] = child.GetComponent <Animator> ();
				ssaZ[zIndex].speed = SSASpeed;
				ssaZ[zIndex].gameObject.SetActive (false);
//				srZ[zIndex] = child.GetComponent <SpriteRenderer> ();
				++zIndex;
			}
		}

//		ftControllerScript = GetComponent <FloatingTextPointController> ();
	}


//	public void PlayRandomAnimation (bool x, bool y, bool z)
	public void PlayRandomAnimation ()
	{		
//		Debug.Log (Time.time + " : " + gameObject.name);
//		if (x)
//		{
			currentIndex = Random.Range (0, SSAcount);
			ssaX[currentIndex].gameObject.SetActive (true);
			ssaX[currentIndex].SetTrigger ("PlayTrigger");
//			Debug.Log ("   # X");
//		}
//		if (y)
//		{
			currentIndex = Random.Range (0, SSAcount);
			ssaY[currentIndex].gameObject.SetActive (true);
			ssaY[currentIndex].SetTrigger ("PlayTrigger");
//			Debug.Log ("   # Y");
//		}
//		if (z)
//		{			
			currentIndex = Random.Range (0, SSAcount);
			ssaZ[currentIndex].gameObject.SetActive (true);
			ssaZ[currentIndex].SetTrigger ("PlayTrigger");
//			Debug.Log ("   # Z");
//		}

//		ftControllerScript.FloatText ();
	}
}
