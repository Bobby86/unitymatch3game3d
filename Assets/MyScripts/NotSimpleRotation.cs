﻿using UnityEngine;
using System.Collections;

public class NotSimpleRotation : MonoBehaviour 
{
	public float MinRotationSpeed = 0.05f; 	// constant part of rotation speed
	public float MaxRotationSpeed = 0.25f;	
	public float MinRangeBetweenMinAndMax = 0.1f;
	public float SpeedModifier = 0.5f;

	private bool rotate = true;
	private float currentLerpTime;
	private float step;
	private Vector3 rotationVector;


	void Start () 
	{			
		MaxRotationSpeed = Random.Range (MinRotationSpeed + MinRangeBetweenMinAndMax, MaxRotationSpeed);
		currentLerpTime = MinRotationSpeed;
		SetRotationAxis ();
	}
	

	void FixedUpdate () 
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

		currentLerpTime += Time.fixedDeltaTime;

		if (currentLerpTime > MaxRotationSpeed)
		{
			currentLerpTime = MinRotationSpeed;
		}

		step = currentLerpTime / MaxRotationSpeed;
//		step = step * step * (3f - 2f * step);
		step = step * step * step * (step * (6f * step - 15f) + 10f) * SpeedModifier;

		transform.Rotate (rotationVector * step);
	}


	private void SetRotationAxis ()
	{
		int axis = Random.Range (0, 6);

		switch (axis)
		{
		case 0:
			rotationVector = Vector3.up;
			break;

		case 1:
			rotationVector = Vector3.forward;
			break;

		case 2:
			rotationVector = Vector3.right;
			break;

		case 3:
			rotationVector = Vector3.down;
			break;

		case 4:
			rotationVector = Vector3.back;
			break;

		case 5:
			rotationVector = Vector3.left;
			break;
		}
	}
}
