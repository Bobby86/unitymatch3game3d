﻿using UnityEngine;
using System.Collections;

// ~animation
public class NotSimpleScaler : MonoBehaviour 
{
	#region Public References
	public static float ScaleFactor;
	public static float ScaleSpeed;
	#endregion

	#region Private References
	private bool scale = true;
	private bool finish = false;
	private bool scaleUp = true;
	private int randomScaleType;	// 0 - scale by y; 1 - scale by x, z
	private float currentLerpValue = 0f;
	private Vector3 startMinScale;
	private Vector3 startMaxScale;
	private Vector3 minScale;
	private Vector3 maxScale;
	private Vector3 targetScale;
	#endregion


	#region Unity Callbacks
	void Start () 
	{		
		SetShapeType ();
		minScale = new Vector3 ();
		startMinScale = new Vector3 ();
		startMaxScale = new Vector3 ();
		minScale = transform.localScale;
		maxScale = MaxScale ();
		startMinScale = minScale;
		startMaxScale = maxScale;
		targetScale = maxScale;
	}


	void LateUpdate ()
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

		if (scale)
		{
			if (scaleUp)
			{
				switch (randomScaleType)
				{
				case 0:
					if (maxScale.y - transform.localScale.y < 0.055f)
					{
						if (finish)
						{
							this.enabled = false;
						}
						
						scaleUp = false;
						currentLerpValue = 0f;
						targetScale = minScale;
					}
					break;
				
				case 1:
					if (maxScale.x - transform.localScale.x < 0.055f)
					{
						if (finish)
						{
							this.enabled = false;
						}


						scaleUp = false;
						currentLerpValue = 0f;
						targetScale = minScale;
					}
					break;
				}
			}
			else
			{
				switch (randomScaleType)
				{
				case 0:
					if (transform.localScale.y - minScale.y < 0.055f)
					{
						scaleUp = true;
						currentLerpValue = 0f;
						targetScale = maxScale;
					}
					break;

				case 1:
					if (transform.localScale.x - minScale.x < 0.055f)
					{
						scaleUp = true;
						currentLerpValue = 0f;
						targetScale = maxScale;
					}
					break;
				}
			}
		}
	}


	void FixedUpdate () 
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

		if (scale)
		{
			currentLerpValue += Time.fixedDeltaTime;
			transform.localScale = Vector3.Lerp (transform.localScale, targetScale, currentLerpValue * ScaleSpeed);
		}
	}
	#endregion


	#region Private Methods
	private void SetShapeType ()
	{		
		randomScaleType = Random.Range (0, 2); // get 0 or 1
//		if (gameObject.name.Contains ("Sphere"))
//		{
//			randomScaleType = 0;
//		}
//		else
//		{
//			randomScaleType = 1;
//		}
	}


	private Vector3 MaxScale ()
	{
		var localMaxScale = new Vector3 ();
		localMaxScale = minScale;

		switch (randomScaleType)
		{
		case 0:
			localMaxScale += new Vector3 (0f, ScaleFactor, 0f);
			break;

		case 1:
			localMaxScale += new Vector3 (ScaleFactor, 0f, ScaleFactor);
			break;
		}

		return localMaxScale;
	}
	#endregion


	#region Public Methods
	public void ChangeScales (bool increase, Vector3 addScale)
	{
		if (increase)
		{
			switch (randomScaleType)
			{
			case 0:
				minScale += new Vector3 (0f, addScale.y, 0f);
				maxScale += new Vector3 (0f, addScale.y, 0f);

				if (transform.localScale.y < minScale.y)
				{
					currentLerpValue = 0f;
					targetScale = maxScale;
					scaleUp = true;
				}
				break;

			case 1:
				minScale += new Vector3 (addScale.x, 0f, addScale.z);
				maxScale += new Vector3 (addScale.x, 0f, addScale.z);

				if (transform.localScale.x < minScale.x)
				{
					currentLerpValue = 0f;
					targetScale = maxScale;
					scaleUp = true;
				}
				break;
			}

			transform.localScale += addScale;
		}
		else
		{
			minScale = startMinScale;
			maxScale = startMaxScale;

			switch (randomScaleType)
			{
			case 0:
				if (transform.localScale.y > maxScale.y)
				{
					currentLerpValue = 0f;
					targetScale = minScale;
					scaleUp = false;
				}
				break;

			case 1:
				if (transform.localScale.x > minScale.x)
				{
					currentLerpValue = 0f;
					targetScale = minScale;
					scaleUp = false;
				}
				break;
			}

			transform.localScale -= addScale;
		}
	}


	public void FinishScale (Vector3 addScale)
	{
		switch (randomScaleType)
		{
		case 0:
			minScale += new Vector3 (0f, addScale.y, 0f);
			maxScale += new Vector3 (0f, addScale.y, 0f);

			if (transform.localScale.y < minScale.y)
			{
				currentLerpValue = 0f;
				targetScale = maxScale;
				scaleUp = true;
			}
			break;

		case 1:
			minScale += new Vector3 (addScale.x, 0f, addScale.z);
			maxScale += new Vector3 (addScale.x, 0f, addScale.z);

			if (transform.localScale.x < minScale.x)
			{
				currentLerpValue = 0f;
				targetScale = maxScale;
				scaleUp = true;
			}
			break;
		}

		scaleUp = true;
		finish = true;
	}
	#endregion
}
