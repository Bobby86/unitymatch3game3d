﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections;

public class AdsScript: MonoBehaviour 
{
//	public GameObject removeAdsButton;

	[SerializeField] bool disableAds = false;
	[Tooltip ("X: every X gameover an Ad would be shown")]
	[SerializeField] int showAdEachXGameOver = 1;
	[Tooltip ("check Ad readiness with this rate(time), if ad not ready or initialized yet")]
	[SerializeField] float checkRateIfAdReady = 0.5f;
	[SerializeField] string androidGameID;
	[SerializeField] string iosGameID;

	void OnEnable()
	{
		MyGameController.GameOverEvent += OnGameOverEvent;
	}

	void OnDisable()
	{
		MyGameController.GameOverEvent -= OnGameOverEvent;
	}

	public void Initialize ()
	{
		#if UNITY_ADS
		if (!Advertisement.isSupported)
		{
			disableAds = true;
			TurnOffAds ();
		}

		if (!disableAds)
		{
			#if UNITY_IOS
			Advertisement.Initialize (iosGameID, true);
			#elif UNITY_ANDROID
			Advertisement.Initialize (androidGameID, true);
			#endif
		}
		#else
		TurnOffAds ();
		#endif
	}

	public void TurnOffAds ()
	{
		disableAds = true;
		this.enabled = false;
	}

	public bool AdsEnabled ()
	{
		return !disableAds;
	}

	private void OnGameOverEvent ()
	{
		if (disableAds)
		{
			return;
		}

		if (showAdEachXGameOver > 0)
		{
			if (checkRateIfAdReady <= 0)
			{
				checkRateIfAdReady = 0.5f;
			}

			StartCoroutine (ShowMyAd (checkRateIfAdReady));
		}
	}

	private IEnumerator ShowMyAd (float delay)
	{
		#if UNITY_ADS
		while (!Advertisement.isInitialized || !Advertisement.IsReady ())
		{
			yield return new WaitForSeconds (delay);
		}

		Advertisement.Show ();
		#else
		yield return null;
		#endif
	}
}