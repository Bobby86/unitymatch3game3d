﻿using UnityEngine;
using System.Collections;

public delegate void MouseMoved(float xMovement, float yMovement);

public class MyInputManager : MonoBehaviour
{
	public static bool ReadInput = true;

	#region Private References
	private float _xMovement;
	private float _yMovement;
	#endregion

	#region Events
	public static event MouseMoved MouseMoved;
	#endregion

	#region Event Invoker Methods
	private static void OnMouseMoved(float xmovement, float ymovement)
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

//		if (MouseoverLayerDetection.HoverAxis != -2)
		if (HoverAxis.CurrentlyHoveredAxis != -2)
		{
			return;
		}

		var handler = MouseMoved;
		if (handler != null) handler(xmovement, ymovement);
	}
	#endregion

	#region Private Methods
	private void InvokeActionOnInput()
	{
		if (Input.GetMouseButton(0) && !Input.GetMouseButtonDown (0))
		{
			_xMovement = Input.GetAxis("Mouse X");
			_yMovement = Input.GetAxis("Mouse Y");
			OnMouseMoved(_xMovement, _yMovement);
		}
	}
	#endregion

	#region Unity CallBacks
	void Update()
	{
		if (ReadInput)
		{
			InvokeActionOnInput();
		}
	}
	#endregion
}