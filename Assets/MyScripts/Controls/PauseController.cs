﻿using UnityEngine;
using System.Collections;

// also GameOver Actions
public class PauseController : MonoBehaviour 
{
	#region Public References
	public BlurControl BlurController;
	[SerializeField]private GameObject mainPanel;
	[SerializeField]private GameObject tutorialPanel;
	public GameObject GameUIPanel;
	public GameObject PauseUIPanel;
	public GameObject PauseButton;
	public GameObject PauseIcon;
	public GameObject BackIcon;
//	public UnityStandardAssets.ImageEffects.BlurOptimized BlurInst;
	#endregion


	#region Private References
	private bool pauseButtonTrigger = false;
	private bool gameOver = false;
	private float lastTimeScale;
	private GameState lastGState;
	#endregion


	#region Unity Callbacks
	void OnEnable ()
	{
		MyGameController.GameOverEvent += OnGameOver;
	}

	void OnDisable ()
	{
		MyGameController.GameOverEvent -= OnGameOver;
	}


	void Start () 
	{
		if (BlurController == null)
		{
			BlurController = Camera.main.GetComponent <BlurControl> ();

			if (BlurController == null)
			{
				Debug.Log ("Blur Controller havn't been found");
			}
		}
	}


	void Update ()
	{
		if ((Input.GetKeyDown (KeyCode.Escape) || pauseButtonTrigger) && !gameOver)
		{
			pauseButtonTrigger = false;

			switch (MyGameController.GState)
			{
			case GameState.GamePlay:
				DragScript.readInput = false;
//				MyInputManager.ReadInput = false;
//				lastTimeScale = Time.timeScale;
//				Time.timeScale = 0f;
//				MyGameController.GState = GameState.Pause;
				SetPause ();
				lastGState = GameState.GamePlay;
//				BlurInst.enabled = true;
//				BlurController.enabled = true;
//				BlurController.TurnTheBlur (true);
				break;

			case GameState.GamePlayNoControl:
//				MyInputManager.ReadInput = false;
//				lastTimeScale = Time.timeScale;
//				Time.timeScale = 0f;
//				MyGameController.GState = GameState.Pause;
				SetPause ();
				lastGState = GameState.GamePlayNoControl;
//				BlurInst.enabled = true;
//				BlurController.enabled = true;
//				BlurController.TurnTheBlur (true);
				break;

			case GameState.Pause:

				if (lastGState == GameState.GamePlay)
				{
					DragScript.readInput = true;
				}

				Time.timeScale = lastTimeScale;
				MyInputManager.ReadInput = true;
				MyGameController.GState = lastGState;
//				BlurInst.enabled = false;
				BlurController.TurnTheBlur (false);
				PauseUIPanel.SetActive (false);
				GameUIPanel.SetActive (true);
				PauseIcon.SetActive (true);
				BackIcon.SetActive (false);
				break;
			}
		}
	}
	#endregion


	#region PauseButton
	public void PauseButtonClick ()
	{
		if (MyGameController.GState == GameState.GamePlay ||
			MyGameController.GState == GameState.GamePlayNoControl ||
			MyGameController.GState == GameState.Pause)
		{
			pauseButtonTrigger = true;
		}
	}

	public void TurnTutorial (bool enable)
	{
		tutorialPanel.SetActive (enable);
		mainPanel.SetActive (!enable);
	}
	#endregion


	#region Private Methods
	private void SetPause ()
	{		
		MyInputManager.ReadInput = false;
		PauseIcon.SetActive (false);
		BackIcon.SetActive (true);
		lastTimeScale = Time.timeScale;
		Time.timeScale = 0f;
		MyGameController.GState = GameState.Pause;
		BlurController.enabled = true;
		BlurController.TurnTheBlur (true);
		GameUIPanel.SetActive (false);
		PauseUIPanel.SetActive (true);
	}

	private void OnGameOver ()
	{
		PauseButton.SetActive (false);
		PauseUIPanel.SetActive (true);
		PauseUIPanel.GetComponent <PausePanelLoader> ().GameOver ();
		DragScript.readInput = false;
//		MyInputManager.ReadInput = false;
		BlurController.enabled = true;
		BlurController.TurnTheBlur (true);
		GameUIPanel.SetActive (false);
	}
	#endregion
}
