﻿using UnityEngine;
using System.Collections;

public class CameraRotator : MonoBehaviour 
{
//	public float AutoRotationTime;
	public float MobileLerpValue = 0.8f;

	#region Private References
	[SerializeField, Range(0.0f, 1.0f)]
	private float _lerpRate;
	private float _xRotation;
	private float _yYRotation;

	private float xVar;
	private float yVar;

	// 
//	private bool autoRotate = false;
//	private float currentLerpValue;
//	private float startYEuler;
//	private float startZEuler;
//	private float currentYEuler;
//	private float currentZEuler;
	// eulers for automatic rotations
//	private float autoXEuler;
//	private float autoYEuler;
//	private float autoZEuler;

//	private SimpleRotation thisObjectSimpleRotationScript;
//	private Quaternion currentAutoRotation;
//	private Quaternion xRotation;
//	private Quaternion xRotationMiddle;
//	private Quaternion yRotation;
//	private Quaternion yRotationMiddle;
//	private Quaternion zRotation;
//	private Quaternion zRotationMiddle;
//	private Quaternion zeroRotation;
	#endregion

	#region Private Methods
	private void Rotate(float xMovement, float yMovement)
	{
		_xRotation += xMovement;
		_yYRotation += yMovement;
	}
	#endregion

	#region Unity CallBacks
	void Start ()
	{
		MyInputManager.MouseMoved += Rotate;

//		xRotation = Quaternion.Euler (new Vector3 (0f, 345f, 0f));
//		yRotation = Quaternion.Euler (new Vector3 (0f, 8f, 15f));
//		zRotation = Quaternion.Euler (new Vector3 (0f, 15f, 4f));

		if (Application.isMobilePlatform)
		{
			_lerpRate = MobileLerpValue;
		}
//		xRotation = Quaternion.Euler (new Vector3 (0f, 359f, 356f));
//		xRotationMiddle = Quaternion.Euler (new Vector3 (0f, 345f, 357f));
//		yRotation = Quaternion.Euler (new Vector3 (0f, 7.5f, 7.5f));
//		yRotationMiddle = Quaternion.Euler (new Vector3 (0f, 357f, 357f));
//		zRotation = Quaternion.Euler (new Vector3 (0f, 14f, 350f));
//		zRotationMiddle = Quaternion.Euler (new Vector3 (0f, 345f, 358f));

//		zeroRotation = Quaternion.Euler (new Vector3 (0f, 0f, 0f));

//		thisObjectSimpleRotationScript = GetComponent <SimpleRotation> ();
	}


	void FixedUpdate ()
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

//		Debug.Log (transform.eulerAngles);

//		if (autoRotate)
//		{
//			if (transform.rotation == currentAutoRotation)
////			if (Vector3.Distance (transform.rotation, currentAutoRotation) < 0.05f)
//			{
////				Debug.Log (transform.eulerAngles);
//				autoRotate = false;
//				_xRotation = 0f;
//				_yYRotation = 0f;
////				thisObjectSimpleRotationScript.enabled = true;
////				Debug.Log ("111");
//			}
//			else
//			{
//				currentLerpValue += Time.fixedDeltaTime;
//				transform.rotation = Quaternion.Lerp (transform.rotation, currentAutoRotation, AutoRotationTime * currentLerpValue);
//			}
//		}
//		else
//		{
			_xRotation = Mathf.Lerp (-_xRotation, 0, _lerpRate);
			_yYRotation = Mathf.Lerp (_yYRotation, 0, _lerpRate);

//			xVar = transform.eulerAngles.y + _xRotation;
//			yVar = transform.eulerAngles.z - _yYRotation;
//
//			if (xVar > 15 && xVar < 345)
//			{
//				_xRotation = 0f;
//			}
//			if (yVar > 15 && yVar < 345)
//			{
//				_yYRotation = 0f;
//			}
			if (_xRotation != 0 || _yYRotation != 0)
			{
				transform.eulerAngles += new Vector3(0, _xRotation, -_yYRotation);
			}
//		}
	}


	void OnDestroy()
	{
		MyInputManager.MouseMoved -= Rotate;
	}
	#endregion

//
//	public void RotateCamera ()
//	{
//		currentAutoRotation = zeroRotation;
////		thisObjectSimpleRotationScript.enabled = false;
//		currentLerpValue = 0f;
//		autoRotate = true;
//	}
//
//
//	public void RotateCamera (int axis, int layerNumber)
//	{
//		switch (axis)
//		{
//		case 0:
//			currentAutoRotation = xRotation;
////			if (layerNumber == 1) // rotate to middle layer
////			{
////				currentAutoRotation = zeroRotation;
////			}
////			else // side layers has the same rotations
////			{
////				currentAutoRotation = xRotation;
////			}
//			break;
//
//		case 1:
//			currentAutoRotation = yRotation;
////			if (layerNumber == 1) // rotate to middle layer
////			{
////				currentAutoRotation = zeroRotation;
////			}
////			else // side layers has the same rotations
////			{
////				currentAutoRotation = yRotation;
////			}
//			break;
//
//		case 2:
//			currentAutoRotation = zRotation;
////			if (layerNumber == 1) // rotate to middle layer
////			{
////				currentAutoRotation = zeroRotation;
////			}
////			else // side layers has the same rotations
////			{
////				currentAutoRotation = zRotation;
////			}
//
//			break;
//
//		case -2:
//			currentAutoRotation = zeroRotation;
//			break;
//		}
//
////		thisObjectSimpleRotationScript.enabled = false;
//		currentLerpValue = 0f;
//		autoRotate = true;
//
////		startYEuler = transform.rotation.eulerAngles.y;
////		startZEuler = transform.rotation.eulerAngles.z;
//	}
}

