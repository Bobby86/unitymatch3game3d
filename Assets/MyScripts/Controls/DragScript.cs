﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


// drag and swap elements
public class DragScript : MonoBehaviour 
{
	#region Public References
	public static int DragLayer; // axis numbers: x = 0, y = 1, z = 2; -1 = none;
//	public static int OtherDragLayer; // for 3d tap logic
//	public static CandyPoints2DLayoutModel candiesStorage;
	public static bool readInput;
	public static bool SwapOnClick;
	public static float MoveDownSpeed;
	public static float MoveDownAcceleration;	// 
	public static float SwapMovementStep;
	public static float DistanceBetweenPoints;
	public static Transform CurrentlyScaled;

//	public float DragSpeedModifier = .04f;
//	public int CandyType;
	public static Vector3 AddScale;

	[HideInInspector]public bool MoveToParentPosition = true;
	[HideInInspector]public bool Swapped = false;	// if elements was swapped -> execution
	#endregion


	#region Private References
//	private bool inDrag = true;	// check if this is candy that user drag with mouse
	private bool canSwap = false;	// swap OnMouseUp
	private bool scaled = false;
	private bool fakeSwap = false;	// if elements cannot be swaped -> move it to each other spot and back
	private bool fakeSwapBack = false; // when it's back turn
	private bool changeCameraLayerVisibilityFlag = false;
	private int axisOnSwap;
	private float movementSpeed;
	private float movementAcceleration;
	private ComboManager comboScript;
	private ChangeMaterial thisMaterialScript;
	private Collider thisCollider;
//	private Vector3 previousCursorPosition;
	private Vector3 parentTransform;		// remember where to move
//	private Vector3 dragOffset;
	private Transform otherTransform;
	private Vector3 fakeParent; // otherTransform parent for fake swap
//	private MeshRenderer thisMeshRenderer;
//	private bool checkDistance = false;

	private float currentLerpFallSpeedValue;
	private float currentFallSpeed;
	private float currentDistanceCheck;
	#endregion


	#region Unity Callbacks
//	void Awake ()
//	{
//		thisMeshRenderer = GetComponent<MeshRenderer>();
//		thisMeshRenderer.enabled = false;

//		thisCollider = GetComponent <Collider> ();
//		thisCollider.isTrigger = true;
////		thisMeshRenderer = GetComponent<MeshRenderer>();
//	}


	void Start ()
	{
		axisOnSwap = DragLayer;
		movementSpeed = MoveDownSpeed;
		movementAcceleration = MoveDownAcceleration;
		thisCollider = GetComponent <Collider> ();
		thisMaterialScript = GetComponent <ChangeMaterial> ();
		comboScript = MyGameController.gameController.GetComponent <ComboManager> ();
	}


	void LateUpdate ()
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

		if (MoveToParentPosition)
		{
//			if (transform.position == transform.parent.position)
			if (Vector3.Distance (transform.position, transform.parent.position) < 0.05f)
			{
				transform.position = transform.parent.position;
				MoveToParentPosition = false;
//				thisCollider.isTrigger = false;
//				checkDistance = false;
//				inDrag = false;
//				gameObject.layer = 0;

				if (Swapped)
				{
					Swapped = false;
//					gameObject.layer = 0;
//					MyGameController.gameController.ExecuteLayer (axisOnSwap);
					MyGameController.gameController.ExecuteCombo ();
//					Debug.Log (gameObject.name);
				}
//				else
//				{
//					transform.parent.GetComponent <CandyPoint> ().SetCandyOpacity (gameObject);
//				}
//				if (!fakeSwap)
//				{
//					transform.parent.GetComponent <CandyPoint> ().SetCandyOpacity (gameObject);
//				}
//				else if (fakeSwap)
//				{
//					if (fakeSwapBack)
//					{
//						fakeSwapBack = false;
//						fakeSwap = false;
//					}
//					else
//					{
//						fakeSwapBack = true;
//						FakeSwap (otherTransform);
//					}
//					gameObject.layer = 0;
//				}
				return;
//				dragStep = MovementStep / 3;
			}

//			if (Swapped)// || fakeSwap)
//			{
//				transform.position = Vector3.MoveTowards (transform.position, transform.parent.position, movementSpeed * Time.deltaTime);
//			}
//			else
//			{
//				currentLerpFallSpeedValue += Time.deltaTime * movementAcceleration;
//
//				if (currentLerpFallSpeedValue > movementSpeed)
//				{
//					currentLerpFallSpeedValue = movementSpeed;
//				}
//
//				currentFallSpeed = currentLerpFallSpeedValue / movementSpeed;
//				currentFallSpeed = 1f - Mathf.Cos (currentFallSpeed * Mathf.PI);
//	//			currentFallSpeed *= currentFallSpeed * 2;
//				transform.position = Vector3.MoveTowards (transform.position, transform.parent.position, currentFallSpeed);
//			}

//			if (Swapped)// || fakeSwap)
//			{
////				Debug.Log (transform.localPosition.y);
//				if ((transform.position.y < 2.34f && transform.position.y > 1.65f) || (transform.position.y < 1.34f && transform.position.y > 0.65f)) // visibility in Y layers bug
////				if (Mathf.Abs (transform.localPosition.y) > 0.16)
//				{		
////					Debug.Log (10);
////					if (!changeCameraLayerVisibilityFlag)
////					{
////						Debug.Log (1);
//						gameObject.layer = 25; //"NotVisibleInY";
////						changeCameraLayerVisibilityFlag = true;
////					}
//				}
//				else
//				{					
////					Debug.Log (2);
////					if (changeCameraLayerVisibilityFlag)
////					{
////						Debug.Log (3);
//						gameObject.layer = 0; //"Default";
////						changeCameraLayerVisibilityFlag = false;
////					}
//				}
//			}
//			else
//			{
//				Debug.Log (4);
//				if (Mathf.Abs (transform.localPosition.y) > 0.16f)
//				{
////					if (!changeCameraLayerVisibilityFlag)
////					{
////						Debug.Log (5);
//						gameObject.layer = 25;//"NotVisibleInY";
////						changeCameraLayerVisibilityFlag = true;
////					}
//				}
//				else
//				{
////					if (changeCameraLayerVisibilityFlag)
////					{
////						Debug.Log (6);
//						gameObject.layer = 0;
////						changeCameraLayerVisibilityFlag = false;
////					}
//				}
//			}
		}
		else if (fakeSwap)
		{
			if (fakeSwapBack)
			{
//				transform.position = Vector3.MoveTowards (transform.position, transform.parent.position, SwapMovementStep * Time.deltaTime);
				currentDistanceCheck = Vector3.Distance (transform.position, transform.parent.position);

//				if (transform.position == transform.parent.position)
				if (currentDistanceCheck < 0.05f)
				{
					transform.position = transform.parent.position;
					fakeSwapBack = false;
					fakeSwap = false;
					DragScript.readInput = true;
//					MyInputManager.ReadInput = true;
//					gameObject.layer = 0;
				}
				else if (currentDistanceCheck < DistanceBetweenPoints / 2)
				{
					thisMaterialScript.ChangeOpacity (true, true);
				}
			}
			else
			{
//				transform.position = Vector3.MoveTowards (transform.position, fakeParent, SwapMovementStep * Time.deltaTime);
				currentDistanceCheck = Vector3.Distance (transform.position, fakeParent);

				if (currentDistanceCheck < 0.05f)
				{
					thisMaterialScript.ChangeOpacity (false, true);
					fakeSwapBack = true;
				}
				else if (currentDistanceCheck < DistanceBetweenPoints / 2)
				{
					thisMaterialScript.ChangeOpacity (true, true);
				}
			}

//			if (Mathf.Abs (transform.localPosition.y) > 0.16f)
//			{
//				gameObject.layer = 25;//"NotVisibleInY";
//			}
//			else
//			{
//				gameObject.layer = 0;
//			}
		}
	}


	void FixedUpdate ()
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

		if (MoveToParentPosition)
		{
			if (Swapped)
			{
				transform.position = Vector3.MoveTowards (transform.position, transform.parent.position, movementSpeed * Time.fixedDeltaTime);
			}
			else
			{
				currentLerpFallSpeedValue += Time.fixedDeltaTime * movementAcceleration;

				if (currentLerpFallSpeedValue > movementSpeed)
				{
					currentLerpFallSpeedValue = movementSpeed;
				}

				currentFallSpeed = currentLerpFallSpeedValue / movementSpeed;
				currentFallSpeed = 1f - Mathf.Cos (currentFallSpeed * Mathf.PI);
				//			currentFallSpeed *= currentFallSpeed * 2;
				transform.position = Vector3.MoveTowards (transform.position, transform.parent.position, currentFallSpeed);
			}
		}
		else if (fakeSwap)
		{
			if (fakeSwapBack)
			{
				transform.position = Vector3.MoveTowards (transform.position, transform.parent.position, SwapMovementStep * Time.fixedDeltaTime);
			}
			else
			{
				transform.position = Vector3.MoveTowards (transform.position, fakeParent, SwapMovementStep * Time.fixedDeltaTime);
			}
		}
	}


	public void OnMouseDown ()
	{
//		Debug.Log (gameObject.name);
//		if (DragScript.CurrentlyScaled != null)
//		{
//			Debug.Log (" : " +  DragScript.CurrentlyScaled.name);
//		}

		if (DragScript.readInput)
		{
			MoveToParentPosition = false;
//			OtherDragLayer 
//			inDrag = true;
//			thisCollider.isTrigger = true;
//			previousCursorPosition = Input.mousePosition;
			axisOnSwap = DragLayer;

			ElementSelection (true);	// scale/unscale + turn on/off select particles
		}
	}
	#endregion

	private void ElementSelection (bool fromMouseClick = false)
	{
		if (transform != DragScript.CurrentlyScaled)
		{				
			if (DragScript.CurrentlyScaled != null)
			{
				SelectElement (false, DragScript.CurrentlyScaled);

//				if (fromMouseClick)
//				{
					DragScript.SwapOnClick = true;
					DragScript.CurrentlyScaled.GetComponent <DragScript> ().MyOnTriggerEnter (thisCollider, fromMouseClick); // 2nd argument - 1st call (from mouse)
					DragScript.CurrentlyScaled = null;
//				}
//				else
//				{					
//					SelectElement (true, transform);
//					DragScript.CurrentlyScaled = transform;
//					DragScript.SwapOnClick = false;
//				}
			}
			else
			{
				SelectElement (true, transform);
				DragScript.CurrentlyScaled = transform;
				DragScript.SwapOnClick = false;
			}
		}
		else
		{
			SelectElement (false, transform);
			DragScript.CurrentlyScaled = null;
			DragScript.SwapOnClick = false;
		}
	}
//	void OnMouseDrag ()
//	{
//		if (DragScript.readInput)
//		{
//			if (DragLayer < 0)
//			{
//				OnMouseUp ();
//				return;
//			}
//
//
//			if (Input.mousePosition != previousCursorPosition)
//			{
//				dragOffset = Input.mousePosition - previousCursorPosition;
//
//				switch (DragLayer)
//				{
//				case 0:
//					dragOffset = new Vector3 (0f, dragOffset.y, -dragOffset.x);
//					dragOffset = Vector3.Scale (dragOffset.normalized, new Vector3 (0f, dragSpeedModifier, dragSpeedModifier));
//					break;
//
//				case 1:
//					dragOffset = new Vector3 (dragOffset.x, 0f, dragOffset.y);
//					dragOffset = Vector3.Scale (dragOffset.normalized, new Vector3 (dragSpeedModifier, 0f, dragSpeedModifier));
//					break;
//
//				case 2:
//					dragOffset = new Vector3 (dragOffset.x, dragOffset.y, 0f);
//					dragOffset = Vector3.Scale (dragOffset.normalized, new Vector3 (dragSpeedModifier, dragSpeedModifier, 0f));
//					break;
//				}
//
//				transform.position += dragOffset;
//			}
//			previousCursorPosition = Input.mousePosition;
//		}
//	}

	#region Public Methods
	public void MyOnMouseUp ()
	{
		MoveToParentPosition = true;
		changeCameraLayerVisibilityFlag = false;
//		inDrag = false;
		currentLerpFallSpeedValue = MoveDownSpeed / 10;
		movementSpeed = SwapMovementStep;

		if (canSwap)
		{
			canSwap = false;
			DragScript.readInput = false;
			SwapElements (otherTransform);
		}
		else
		{
			parentTransform = transform.parent.position;
//			moveToParent = true;
		}

		if (DragScript.SwapOnClick)
		{
//			if (DragScript.CurrentlyScaled != null)
//			{
////				DragScript.CurrentlyScaled.localScale -= AddScale;
//				DragScript.CurrentlyScaled = null;
//			}
			DragScript.SwapOnClick = false;
		}
	}


	public void MoveToParent (bool doubleMoveDownSpeed)
	{
		parentTransform = transform.parent.position;

//		if (doubleMoveDownSpeed)
//		{
//			movementSpeed = MoveDownSpeed;
//			movementAcceleration = MoveDownAcceleration;
//		}
//		else
//		{
//			movementSpeed = MoveDownSpeed;
//			movementAcceleration = MoveDownAcceleration;
//		}
		movementSpeed = MoveDownSpeed;
		movementAcceleration = MoveDownAcceleration;

		changeCameraLayerVisibilityFlag = false;
		currentLerpFallSpeedValue = movementSpeed / 10;
		MoveToParentPosition = true;
	}
	#endregion
//	void OnTriggerEnter (Collider other)
//	{
//		if (!thisMeshRenderer.enabled)
//		{
//			if (other.name.Contains ("Trigger"))
//			{
//				thisMeshRenderer.enabled = true;
//			}
//		}
//	}

	#region Private Methods
	private void SelectElement (bool select, Transform element)
	{
		if (element != null)
		{
			element.GetComponent <NotSimpleScaler> ().ChangeScales (select, MyGameController.gameController.AddScale);
			element.GetComponentInParent <CandyPoint> ().TurnSelectedParticles (select);
		}
	}

	// swap elements part // modified 
	private void MyOnTriggerEnter (Collider other, bool calledFromMouse)
	{		
		if (!MoveToParentPosition)// && inDrag)
		{
//			if (other.name.Contains ("Cube"))
//			{
			Transform[] layer = null;
			CandyPoint thisCP = transform.parent.GetComponent <CandyPoint> ();
			CandyPoint otherCP = other.transform.parent.GetComponent <CandyPoint> ();
			int thisCandyType = thisCP.CandyType;
			int thisIndex = 0;
			int otherIndex = 0;

//			if (axisOnSwap < 0) // 3d layout part
//			{
//				FindAndCheckLayers (other, calledFromMouse);
//				return;
//			}

			thisIndex = comboScript.GetCPIndexInTheLayer (thisCP.PointLayer, thisCP.transform);
			otherIndex = comboScript.GetCPIndexInTheLayer (otherCP.PointLayer, otherCP.transform);

//			Debug.Log (thisCP.PointLayer + " " + thisIndex);
//			Debug.Log (otherCP.PointLayer + " " + otherIndex);

			if (comboScript.CheckSwapability (thisIndex, otherIndex, thisCP.PointLayer, otherCP.PointLayer))
			{
				if (thisCP.CandyShape != otherCP.CandyShape || thisCandyType != otherCP.CandyType)
				{
					bool swap = comboScript.TryToFindCombo (thisIndex, otherIndex, thisCP.PointLayer, otherCP.PointLayer);
//					bool swap = CheckNeighboursForSwap (thisIndex, otherIndex, thisCandyType, layer);
//					Debug.Log (swap);

					if (swap)
					{
						DragScript.readInput = false;
//						MyInputManager.ReadInput = false;
						canSwap = true;
						otherTransform = other.transform;

						if (DragScript.SwapOnClick)
						{
							MyOnMouseUp ();
						}
					}
					else
					{							
						canSwap = false;
						otherTransform = other.transform;
						//								fakeSwap = true;
						FakeSwap (otherTransform);
					}
				}
				else
				{
					canSwap = false;
					otherTransform = other.transform;
					//						fakeSwap = true;
					FakeSwap (otherTransform);
				}
			}
			else
			{
				canSwap = false;
				ElementSelection ();
//				SelectElement (false, transform);
//				DragScript.CurrentlyScaled = other.transform;
//				SelectElement (true, other.transform);
			}
		}
	}
	// old
//	void MyOnTriggerEnter (Collider other, bool calledFromMouse)
//	{		
//		if (!MoveToParentPosition)// && inDrag)
//		{
//			if (other.name.Contains ("Cube"))
//			{
//				Transform[] layer = null;
//				CandyPoint thisCP = transform.parent.GetComponent <CandyPoint> ();
//				CandyPoint otherCP = other.transform.parent.GetComponent <CandyPoint> ();
//				int thisCandyType = thisCP.CandyType;
//				int thisIndex = 0;
//				int otherIndex = 0;
//				int localSwapAxis = 0; // for 3D swaps
//
//				if (axisOnSwap < 0) // 3d layout part
//				{
//					FindAndCheckLayers (other, calledFromMouse);
//				}
//				else // 2D layout part
//				{
////					Debug.Log ("2D " + axisOnSwap);
//					thisIndex = candiesStorage.FindItemIndex (axisOnSwap, transform.parent);
//					otherIndex = candiesStorage.FindItemIndex (axisOnSwap, other.transform.parent);
//				}
//				int layerLength = -1;
//
//				if (thisCandyType != otherCP.CandyType)
//				{
//					//					Transform[] layer = null;
//
//					switch (axisOnSwap)
//					{
//					case 0:
//						layerLength = candiesStorage.XLayerLength;
//						layer = candiesStorage.GetLayer (0);
//						break;
//
//					case 1:
//						layerLength = candiesStorage.YLayerLength;
//						layer = candiesStorage.GetLayer (1);
//						break;
//
//					case 2:
//						layerLength = candiesStorage.ZLayerLength;
//						layer = candiesStorage.GetLayer (2);
//						break;
//					}
//
//					if ((thisIndex + 1 == otherIndex && (otherIndex % layerLength != 0) || 
//						(thisIndex - 1 == otherIndex && thisIndex % layerLength != 0) || 
//						thisIndex + layerLength == otherIndex || thisIndex - layerLength == otherIndex))
//					{
//						bool swap = CountSameTypeNeighbours (thisIndex, otherIndex, thisCandyType, layer);
////						bool swap = comboScript.FindARealCombo (thisIndex, otherIndex, 
//						//						bool swap = CheckNeighboursForSwap (thisIndex, otherIndex, thisCandyType, layer);
//
//						if (swap)
//						{
//							//							Debug.Log (gameObject.name + "  " + otherName);
//							DragScript.readInput = false;
//							MyInputManager.ReadInput = false;
//							canSwap = true;
//							otherTransform = other.transform;
//
//
//							if (DragScript.SwapOnClick)
//							{
//								MyOnMouseUp ();
//								//								DragScript.SwapOnClick = false;
//								//								SwapElements (other.transform);
//								//								moveToParent = true;
//								//								canSwap = false;
//							}
//						}
//						else
//						{							
//							canSwap = false;
//
//							if (calledFromMouse)
//							{
//								other.GetComponent <DragScript> ().MyOnTriggerEnter (thisCollider, false);
//							}
//							else
//							{
//								// TODO : fake move
//								otherTransform = other.transform;
//								//								fakeSwap = true;
//								FakeSwap (otherTransform);
//							}
//						}
//					}
//					//					else
//					//					{
//					//						canSwap = false;
//					//
//					//						if (calledFromMouse)
//					//						{
//					//							other.GetComponent <DragScript> ().MyOnTriggerEnter (thisCollider, false);
//					//						}
//					//						else
//					//						{
//					//							otherTransform = other.transform;
//					////							fakeSwap = true;
//					//							FakeSwap (otherTransform);
//					//						}
//					//					}
//				}
//				else
//				{
//					canSwap = false;
//
//					if ((thisIndex + 1 == otherIndex && (otherIndex % layerLength != 0) || 
//						(thisIndex - 1 == otherIndex && thisIndex % layerLength != 0) || 
//						thisIndex + layerLength == otherIndex || thisIndex - layerLength == otherIndex))
//					{
//						if (calledFromMouse)
//						{
//							other.GetComponent <DragScript> ().MyOnTriggerEnter (thisCollider, false);
//						}
//						else
//						{
//							otherTransform = other.transform;
//							//						fakeSwap = true;
//							FakeSwap (otherTransform);
//						}
//					}
//				}
//			}
//			else
//			{
//				canSwap = false;
//			}
//		}
//	}

//
//	void FindAndCheckLayers (Collider other, bool calledFromMouse)
//	{
//		bool matchFound = false;
//		CandyPoint parentCPScript = transform.parent.GetComponent <CandyPoint> ();
//		CandyPoint otherParentCPScript = other.transform.parent.GetComponent <CandyPoint> ();
//		int thisLayerIndex = parentCPScript.XLayerIndex;
//		int otherLayerIndex = otherParentCPScript.XLayerIndex;
//		int axisIndex;	
//
//		if ((thisLayerIndex == otherLayerIndex))
//		{
//			axisIndex = 0;
//			matchFound = MyOnTriggerEnter3D (other, calledFromMouse, axisIndex, thisLayerIndex, otherLayerIndex);
//			Debug.Log ("Check X");
//			if (matchFound)
//			{
//				return;
//			}
//		}
//
//		thisLayerIndex = parentCPScript.YLayerIndex;
//		otherLayerIndex = otherParentCPScript.YLayerIndex;
//
//		if ((thisLayerIndex == otherLayerIndex))
//		{
//			axisIndex = 1;
//			matchFound = MyOnTriggerEnter3D (other, calledFromMouse, axisIndex, thisLayerIndex, otherLayerIndex);
//			Debug.Log ("Check Y");
//			if (matchFound)
//			{
//				return;
//			}
//		}
//
//		thisLayerIndex = parentCPScript.ZLayerIndex;
//		otherLayerIndex = otherParentCPScript.ZLayerIndex;
//
//		if ((thisLayerIndex == otherLayerIndex))
//		{
//			Debug.Log ("Check Z");
//			axisIndex = 2;
//			MyOnTriggerEnter3D (other, calledFromMouse, axisIndex, thisLayerIndex, otherLayerIndex);
////			matchFound = MyOnTriggerEnter3D (other, calledFromMouse, axisIndex, thisLayerIndex, otherLayerIndex);
////
////			if (matchFound)
////			{
////				return;
////			}
//		}
//	}
//
//	// swap elements part // modified 
//	bool MyOnTriggerEnter3D (Collider other, bool calledFromMouse, int axisIndex, int thisLayerIndex, int otherLayerIndex)
//	{		
//		if (!MoveToParentPosition)// && inDrag)
//		{
//			if (other.name.Contains ("Cube"))
//			{
//				Transform[] layer = null;
//				int thisCandyType = transform.parent.GetComponent <CandyPoint> ().CandyType;
//				int thisIndex = 0;
//				int otherIndex = 0;
//
////					CandyPoint parentCPScript = transform.parent.GetComponent <CandyPoint> ();
////					CandyPoint otherParentCPScript = other.transform.parent.GetComponent <CandyPoint> ();
////					int thisLayerIndex = parentCPScript.XLayerIndex;
////					int otherLayerIndex = otherParentCPScript.XLayerIndex;
//
////					if (thisLayerIndex == otherLayerIndex)
////					{
////						localSwapAxis = 0; // another 3d layer from ExecuteCandies layers that gonna work
////						layer = candiesStorage.X1Layers[thisLayerIndex];
////						Debug.Log ("X " + thisLayerIndex);
////					}
////					else
////					{
////						thisLayerIndex = parentCPScript.YLayerIndex;
////						otherLayerIndex = otherParentCPScript.YLayerIndex;
////
////						if (thisLayerIndex == otherLayerIndex)
////						{
////							localSwapAxis = 1;
////							layer = candiesStorage.X1Layers[thisLayerIndex];
////							Debug.Log ("Y " + thisLayerIndex);
////						}
////						else
////						{
////							thisLayerIndex = parentCPScript.ZLayerIndex;
////							otherLayerIndex = otherParentCPScript.ZLayerIndex;
////
////							if (thisLayerIndex == otherLayerIndex)
////							{
////								localSwapAxis = 2;
////								layer = candiesStorage.X1Layers[thisLayerIndex];
////								Debug.Log ("Z " + thisLayerIndex);
////							}
////							else
////							{
////								Debug.Log ("000 " + thisLayerIndex);
////								return;
////							}
////						}
////					}
//
//
//				thisIndex = candiesStorage.FindItemIndex (axisIndex, thisLayerIndex, transform.parent);
//				otherIndex = candiesStorage.FindItemIndex (axisIndex, otherLayerIndex, other.transform.parent);
//
//				int layerLength = candiesStorage.XLayerLength;	// WARNING
//
//				if (thisCandyType != other.transform.parent.GetComponent <CandyPoint> ().CandyType)
//				{
//					layer = candiesStorage.GetLayer (axisIndex, thisLayerIndex);
//
//					if ((thisIndex + 1 == otherIndex && (otherIndex % layerLength != 0) || 
//						(thisIndex - 1 == otherIndex && thisIndex % layerLength != 0) || 
//						thisIndex + layerLength == otherIndex || thisIndex - layerLength == otherIndex))
//					{
//						bool swap = CountSameTypeNeighbours (thisIndex, otherIndex, thisCandyType, layer);
//
//						if (swap)
//						{
//							DragScript.readInput = false;
//							MyInputManager.ReadInput = false;
//							canSwap = true;
//							otherTransform = other.transform;
//
//
//							if (DragScript.SwapOnClick)
//							{
//								MyOnMouseUp ();
//							}
//
//							return true;
//						}
//						else
//						{							
//							canSwap = false;
//
//							if (calledFromMouse)
//							{
//								return (other.GetComponent <DragScript> ().MyOnTriggerEnter3D (thisCollider, false, axisIndex, thisLayerIndex, otherLayerIndex));
//							}
//							else
//							{
//								otherTransform = other.transform;
//								FakeSwap (otherTransform);
//								return false;
//							}
//						}
//					}
//					//else
//					return false;
//				}
//				else // same candyType
//				{
//					canSwap = false;
//
//					if ((thisIndex + 1 == otherIndex && (otherIndex % layerLength != 0) || 
//						(thisIndex - 1 == otherIndex && thisIndex % layerLength != 0) || 
//						thisIndex + layerLength == otherIndex || thisIndex - layerLength == otherIndex))
//					{
//						if (calledFromMouse)
//						{
////							return (other.GetComponent <DragScript> ().MyOnTriggerEnter3D (thisCollider, false, axisIndex, thisLayerIndex, otherLayerIndex));
////						}
////						else
////						{
//							otherTransform = other.transform;
//							FakeSwap (otherTransform);
//							return false;
//						}
//					}
//					//else
//					return false;
//				}
//			}
//			else
//			{
//				canSwap = false;
//				return false;
//			}
//		}
//
//		return false;
//	}


	private void FakeSwap (Transform other)
	{
		DragScript.readInput = false;
//		MyInputManager.ReadInput = false;

		fakeParent = other.parent.position;
		other.GetComponent <DragScript> ().FakeSwapOther (transform);
//		thisMaterialScript.FakeSwapAlphaChanges ();
		thisMaterialScript.ChangeOpacity (false, true);
		fakeSwap = true;
//		DragScript otherDS = other.GetComponent <DragScript> ();
//		Transform tmp = transform.parent;
//		transform.parent = other.parent;
//		other.parent = tmp;
//		otherDS.MoveToParent (false);
//		MoveToParent (false);
	}


	public void FakeSwapOther (Transform other)
	{
		DragScript.readInput = false;
//		MyInputManager.ReadInput = false;

		fakeParent = other.parent.position;
//		thisMaterialScript.FakeSwapAlphaChanges ();
		thisMaterialScript.ChangeOpacity (false, true);
		fakeSwap = true;
	}


	private void SwapElements (Transform other)
	{		
		Transform tmp = transform.parent;
//		MyInputManager.ReadInput = false;
		changeCameraLayerVisibilityFlag = false;
		transform.parent = other.parent;
		other.parent = tmp;
		other.GetComponent <DragScript> ().Swapped = true;
		other.GetComponent <DragScript> ().MyOnMouseUp ();
		parentTransform = transform.parent.position;
		CandyPoint thisCP = transform.parent.GetComponent <CandyPoint> ();
		CandyPoint otherCP = other.parent.GetComponent <CandyPoint> ();
		int tmpCT = thisCP.CandyType;
		int tmpCS = thisCP.CandyShape;
		thisCP.CandyType = otherCP.CandyType;
		thisCP.CandyShape = otherCP.CandyShape;
		otherCP.CandyType = tmpCT;
		otherCP.CandyShape = tmpCS;
		Swapped = true;
//		moveToParent = true;
	}

	// line mechanics
	private bool CheckNeighboursForSwap (int itemIndex, int swapItemIndex, int candyType, Transform[] layer)
	{
		bool check; 
		int layerLength = (int) Mathf.Sqrt (layer.Length);

		if (VerticalCheck (itemIndex, swapItemIndex, candyType, layerLength, layer))
		{
			return true;
		}

		if (HorizontalCheck (itemIndex, swapItemIndex, candyType, layerLength, layer))
		{
			return true;
		}

		return false;
	}


	private bool VerticalCheck (int mainIndex, int swapIndex, int candyType, int layerLength, Transform[] layer)
	{
		int sameNeighbourCount = 0;

		if ((swapIndex - layerLength != mainIndex) && (swapIndex - layerLength >= 0))
		{
			if (layer[swapIndex - layerLength].GetComponent <CandyPoint> ().CandyType == candyType)
			{
				++sameNeighbourCount;

				if (swapIndex - 2 * layerLength >= 0)
				{
					if (layer[swapIndex - 2 * layerLength].GetComponent <CandyPoint> ().CandyType == candyType)
					{
						return true;
					}
				}
			}
		}

		if ((swapIndex + layerLength != mainIndex) && (swapIndex + layerLength < layer.Length))
		{
			if (layer[swapIndex + layerLength].GetComponent <CandyPoint> ().CandyType == candyType)
			{
				++sameNeighbourCount;

				if ((1 < sameNeighbourCount) || ((swapIndex + 2 * layerLength < layer.Length) && (layer[swapIndex + 2 * layerLength].GetComponent <CandyPoint> ().CandyType == candyType)))
				{
					return true;
				}				
			}
		}

		return false;
	}


	private bool HorizontalCheck (int mainIndex, int swapIndex, int candyType, int layerLength, Transform[] layer)
	{
		int sameNeighbourCount = 0;

		if ((swapIndex - 1 != mainIndex) && (swapIndex - 1 >= 0) &&((swapIndex - 1) % layerLength != layerLength - 1))
		{
			if (layer[swapIndex - 1].GetComponent <CandyPoint> ().CandyType == candyType)
			{
				++sameNeighbourCount;

				if ((swapIndex - 2 >= 0) && ((swapIndex - 2) % layerLength != layerLength - 1))
				{
					if (layer[swapIndex - 2].GetComponent <CandyPoint> ().CandyType == candyType)
					{
						return true;
					}
				}
			}
		}

		if ((swapIndex + 1 != mainIndex) && (swapIndex + 1 < layer.Length) && ((swapIndex + 1) % layerLength != 0))
		{
			if (layer[swapIndex + 1].GetComponent <CandyPoint> ().CandyType == candyType)
			{
				++sameNeighbourCount;

				if ((1 < sameNeighbourCount) || ((swapIndex + 2 < layer.Length) && ((swapIndex + 2) % layerLength != 0) && (layer[swapIndex + 2].GetComponent <CandyPoint> ().CandyType == candyType)))
				{
					return true;
				}				
			}
		}

		return false;
	}

	// L-shapes mechanics
	// searching only for 2 same elements
	private bool CountSameTypeNeighbours (int itemIndex, int swapItemIndex, int candyType, Transform[] layer)
	{		
		int previousStepCount;
		int centerItemIndex = swapItemIndex;
		int mainItemIndex = itemIndex;
		int layerLength = (int) Mathf.Sqrt (layer.Length);
		List<int> foundedIndexes = new List<int>();

		foundedIndexes.Add (centerItemIndex);

		while (foundedIndexes.Count < 3)
		{
			previousStepCount = foundedIndexes.Count;
			CheckNeighbourItems (foundedIndexes[foundedIndexes.Count - 1], mainItemIndex, candyType, layerLength, layer, foundedIndexes);					

			if (previousStepCount == foundedIndexes.Count)
			{
				return false;
			}
			mainItemIndex = foundedIndexes[foundedIndexes.Count - 2];
		}

		return true;
	}

	// result is bool[4] initialized with false, and 1 item is true (check initial element)
	private void CheckNeighbourItems (int centerItemIndex, int mainItemIndex, int candyType, int layerLength, Transform[] layer, List<int> result)
	{
		if (mainItemIndex != centerItemIndex - 1 && centerItemIndex % layerLength != 0 && centerItemIndex - 1 >= 0)
		{
			if (candyType == layer[centerItemIndex - 1].GetComponent <CandyPoint> ().CandyType)
			{
				result.Add(centerItemIndex - 1);
			}
		}

		if (mainItemIndex != centerItemIndex + 1 && (centerItemIndex + 1) % layerLength != 0 && centerItemIndex + 1 < layer.Length)
		{
			if (candyType == layer[centerItemIndex + 1].GetComponent <CandyPoint> ().CandyType)
			{
				result.Add(centerItemIndex + 1);
			}
		}

		if (mainItemIndex != centerItemIndex - layerLength && centerItemIndex - layerLength >= 0)
		{
			if (candyType == layer[centerItemIndex - layerLength].GetComponent <CandyPoint> ().CandyType)
			{
				result.Add(centerItemIndex - layerLength);
			}
		}

		if (mainItemIndex != centerItemIndex + layerLength && centerItemIndex + layerLength < layer.Length)
		{
			if (candyType == layer[centerItemIndex + layerLength].GetComponent <CandyPoint> ().CandyType)
			{
				result.Add(centerItemIndex + layerLength);
			}
		}
	}
	#endregion
}
