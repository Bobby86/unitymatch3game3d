﻿using UnityEngine;
using System.Collections;

public class ChildCandy : MonoBehaviour 
{
	#region Private References
	private DragScript parentScript;
	#endregion


	#region Unity Callbacks
	void Start () 
	{
		parentScript = GetComponentInParent <DragScript> ();
	}


	void OnMouseDown ()
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

		parentScript.OnMouseDown ();
	}
	#endregion

}
