﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class TopScoreLoader : MonoBehaviour 
{
	public static Text TopScore;


	void Awake ()
	{
		TopScore = GetComponentInChildren <Text> ();
	}


	public static void LoadTopScore ()
	{
		TopScore.text = PlayerPrefs.GetInt ("BestScore").ToString ();
	}
}
