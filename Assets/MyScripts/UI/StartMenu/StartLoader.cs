﻿using UnityEngine;
using System.Collections;

public class StartLoader : MonoBehaviour 
{
	#region Public References
	public static StartLoader Instance;

	public bool ClearPlayerPrefs;
	public bool MobileTest = false;
//	public float UISoundVolume = 1f;
	public float MusicVolume = 1f;
	public float SoundVolume = 1f;
	public AudioSource SceneMusicSource;
	public AudioSource UIClickSound;

	[SerializeField]private GameObject mainPanel;
	[SerializeField]private GameObject tutorialPanel;
	#endregion


//	#region Private References
//
//	#endregion


	#region Unity Callbacks
	void Awake ()
	{
		Instance = this;

		if (Application.isEditor)
		{
			StaticDataAndSettings.isMobile = MobileTest;
		}
	}

	void Start ()
	{
		SMSoundController.Instance.DefaultMusicVolume = MusicVolume;
		SMSoundController.Instance.DefaultSoundVolume = SoundVolume;
		SMSoundController.Instance.MusicSource = SceneMusicSource;
		SMSoundController.Instance.ClickSound = UIClickSound;

		if (PlayerPrefs.HasKey ("NotFirstRun"))
		{
			TurnTutorial (false);
			SMSoundController.Instance.InitializeSound ();
			TopScoreLoader.LoadTopScore ();
		}
		else
		{
			StaticDataAndSettings.isMobile = Application.isMobilePlatform;
//			SMSoundController.DefaultMusicVolume = MusicVolume;
//			SMSoundController.DefaultUISoundVolume = UISoundVolume;
//			SMSoundController.DefaultSoundVolume = SoundVolume;
			TurnTutorial (true);
			PlayerPrefs.SetInt ("NotFirstRun", 0);
			PlayerPrefs.SetInt ("BestScore", 0);
			PlayerPrefs.Save ();

			SMSoundController.Instance.InitializeSound (true);	// 1st init
			// add uisounds etc
		}

		if (ClearPlayerPrefs && Application.isEditor)
		{
			PlayerPrefs.DeleteAll ();
			PlayerPrefs.Save ();
		}
	}	
	#endregion


	#region Public Methods
	public void TurnTutorial (bool enable)
	{
		tutorialPanel.SetActive (enable);
		mainPanel.SetActive (!enable);
	}
	#endregion
}
