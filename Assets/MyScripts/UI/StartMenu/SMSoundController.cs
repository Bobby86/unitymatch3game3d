﻿using UnityEngine;
using System.Collections;


public class SMSoundController : MonoBehaviour 
{
	#region Public References
	public static SMSoundController Instance;

	public float CurrentMusicVolume;
//	public static float CurrentUISoundVolume;
	public float CurrentSoundVolume;
	public float DefaultMusicVolume;
//	public static float DefaultUISoundVolume;
	public float DefaultSoundVolume;
	public AudioSource MusicSource;
	public AudioSource ClickSound;
	public ImageColorAlpha MusicIcon;
	public ImageColorAlpha SoundIcon;
	#endregion

	#region Unity Callbacks
	void Awake ()
	{
		Instance = this;
	}
	#endregion

	#region Public Methods
	public void InitializeSound (bool first = false)
	{
		if (first)
		{
//			PlayerPrefs.SetFloat ("UISoundVolume", DefaultUISoundVolume);
			PlayerPrefs.SetFloat ("MusicVolume", DefaultMusicVolume);
			PlayerPrefs.SetFloat ("SoundVolume", DefaultSoundVolume);
			PlayerPrefs.Save ();
			CurrentMusicVolume = DefaultMusicVolume;
			CurrentSoundVolume = DefaultSoundVolume;
//			CurrentUISoundVolume = DefaultUISoundVolume;
		}
		else
		{
			CurrentMusicVolume = PlayerPrefs.GetFloat  ("MusicVolume");
//			CurrentUISoundVolume = PlayerPrefs.GetFloat  ("UISoundVolume");
			CurrentSoundVolume = PlayerPrefs.GetFloat  ("SoundVolume");

			if (CurrentSoundVolume == 0)
			{
				SoundIcon.InstantFade (false);
			}

			if (CurrentMusicVolume == 0)
			{
				MusicIcon.InstantFade (false);
			}
		}

		MusicSource.volume = CurrentMusicVolume;
		ClickSound.volume = CurrentSoundVolume;
	}

	// on/off music
	public void SetMusicVolume ()
	{
		if (0 == CurrentMusicVolume)
		{			
			CurrentMusicVolume = DefaultMusicVolume;
			MusicIcon.FadeButton (true);
		}
		else
		{
			CurrentMusicVolume = 0f;
			MusicIcon.FadeButton (false);
		}

		MusicSource.volume = CurrentMusicVolume;
		PlayerPrefs.SetFloat ("MusicVolume", CurrentMusicVolume);
		PlayerPrefs.Save ();
	}


	// on/off sound
	public void SetSoundVolume ()
	{
		if (0 == CurrentSoundVolume)
		{			
			CurrentSoundVolume = DefaultSoundVolume;
			SoundIcon.FadeButton (true);
		}
		else
		{
			CurrentSoundVolume = 0f;
			SoundIcon.FadeButton (false);
		}

		ClickSound.volume = CurrentSoundVolume;
		PlayerPrefs.SetFloat ("SoundVolume", CurrentSoundVolume);
		PlayerPrefs.Save ();
	}
	#endregion
}
