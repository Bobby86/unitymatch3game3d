﻿using UnityEngine;
using System.Collections;

public class StartMenuButtons : MonoBehaviour 
{	
	[SerializeField]private float startGameDelay;
	[SerializeField]private ImageColorAlpha sceneEffect;

	#region Buttons Methods
	public void PlayButtonClick ()
	{
		sceneEffect.FadeButton (true);
		Invoke ("LoadGameScene", startGameDelay);
	}


	public void MusicButtonClick ()
	{
		SMSoundController.Instance.SetMusicVolume ();	// turn on/off music
	}


	public void SoundButtonClick ()
	{
		SMSoundController.Instance.SetSoundVolume ();	// turn on/off sounds
	}


	public void QuitButtonClick ()
	{
		//TODO animation
		Application.Quit ();
	}
	#endregion

	private void LoadGameScene ()
	{
		Application.LoadLevel (Application.loadedLevel + 1);
	}
}
