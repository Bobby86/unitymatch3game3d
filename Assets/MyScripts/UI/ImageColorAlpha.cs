﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ImageColorAlpha : MonoBehaviour 
{
	public bool SceneEffect = false;
	public float TransitionDuration = 0.2f;
	public Image ImageComponent;
	public Color StartColor;
	public Color SecondColor;

	private bool changeColor = false;
	private float lerpValue;
	private float step;
//	public Color startColor;
	private Color targetColor;

	void Start ()
	{
////		ImageComponent = GetComponent <Image> ();	
//		startColor = ImageComponent.color;
		if (SceneEffect)
		{
			FadeButton ();
		}
	}

	void LateUpdate ()
	{
		if (changeColor)
		{
			lerpValue += Time.unscaledDeltaTime;
			step = lerpValue / TransitionDuration;
			step = 1f - Mathf.Cos (step * Mathf.PI * 0.5f);

			ImageComponent.color = Color.Lerp (ImageComponent.color, targetColor, step);

			if (ImageComponent.color.a == targetColor.a)
			{
				changeColor = false;
			}
		}
	}

	public void FadeButton (bool activate = false)
	{
		if (activate)
		{
			if (targetColor != StartColor)
			{
				targetColor = StartColor;
			}
		}
		else
		{
			if (targetColor != SecondColor)
			{
				targetColor = SecondColor;
			}
		}

		if (!changeColor)
		{
			lerpValue = 0f;
			changeColor = true;
		}
	}


	public void InstantFade (bool active = false)
	{
		changeColor = false;
		
		if (active)
		{			
			ImageComponent.color = StartColor;
		}
		else
		{
			ImageComponent.color = SecondColor;
		}
	}

	public void InitializeColors (Color mainColor, Color fadedColor, bool activate = false)
	{
		changeColor = false;
//		ImageComponent.color = mainColor;
//		StartColor = mainColor;
//		SecondColor = fadedColor;

		InstantFade (activate);
	}
}
