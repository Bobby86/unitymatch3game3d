﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FloatingTextPointController : MonoBehaviour 
{
//	public Transform[] TwoDCameras;	// 2d cameras of this object
	[HideInInspector] public int Score = 0;
	public Vector3 XRotation = new Vector3 (0f, -90f, 0f);
	public Vector3 YRotation = new Vector3 (90f, 0f, 0f);
	public Vector3 ZRotation = new Vector3 (0f, 0f, 0f);
	public Transform floatingTextObject;

	private int camIndex;
	private int previousCamIndex = -1;
	private Text textField;
	private FloatingTextMovement ftMovementScript;
	private Quaternion[] TwoDCamerasRotations = new Quaternion[3];


	void Start ()
	{
		TwoDCamerasRotations[0] = Quaternion.Euler (XRotation);
		TwoDCamerasRotations[1] = Quaternion.Euler (YRotation);
		TwoDCamerasRotations[2] = Quaternion.Euler (ZRotation);
		ftMovementScript = floatingTextObject.GetComponent <FloatingTextMovement> ();
		textField = floatingTextObject.GetComponentInChildren <Text> ();
//		InvokeRepeating ("FaceTextToCameraAndMove", 1f, 1f);
//		FloatText ();
	}


	public void FloatText () //int score = 100)
	{
		if (Score < 1) return;

		textField.text = "+" + Score;
		Score = 0;
		FaceTextToCameraAndMove ();
//		floatingTextObject.gameObject.SetActive (true);

		// TODO script to move ftgo
		// TODO scrip to lerp text color
	}


	private void FaceTextToCameraAndMove ()
	{			
		camIndex = Random.Range (0, TwoDCamerasRotations.Length);

		if (previousCamIndex == camIndex)
		{
			camIndex = (camIndex + 1) % TwoDCamerasRotations.Length;
		}
//		textField.text = "+" + camIndex; 
		floatingTextObject.rotation = TwoDCamerasRotations[camIndex];
//		camIndex = (camIndex + 1) % TwoDCamerasRotations.Length;
//		ftMovementScript.enabled = true;
		floatingTextObject.gameObject.SetActive (true);
		ftMovementScript.StartMove (camIndex);
		previousCamIndex = camIndex;
	}
}
