﻿using UnityEngine;
using System.Collections;

public class SimpleRotator : MonoBehaviour 
{

	[SerializeField] float ScaleTime = 0.3f;
//	[SerializeField] Vector3 AddScale;
//
//	private bool scaleUp = true;
//	private float currentLerpTime = 0f;
//	private float step;
//	private Vector3 startScale;
//	private Vector3 endScale;


//	void Start ()
//	{
//		startScale = transform.localScale;
//		endScale = startScale + AddScale;
//	}


	void LateUpdate () 
	{
//		currentLerpTime += Time.deltaTime;
//		step = currentLerpTime / ScaleTime;
////		step =  step * step * step * (step * (6f * step - 15f) + 10f);
////		step = Mathf.Sin (Mathf.PI * step * 0.5f);
//
//		if (scaleUp)
//		{
//			transform.localScale = Vector3.Lerp (transform.localScale, endScale, step);
//
//			if (transform.localScale.x >= endScale.x)
//			{
//				scaleUp = false;
//				currentLerpTime = 0f;
//			}
//		}
//		else
//		{
//			transform.localScale = Vector3.Lerp (transform.localScale, startScale, step);
//
//			if (transform.localScale == startScale)
//			{
//				scaleUp = true;
//				currentLerpTime = 0f;
//			}
//		}
		transform.Rotate (Vector3.forward, Time.deltaTime * ScaleTime);
	}
}
