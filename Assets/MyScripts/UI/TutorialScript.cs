﻿using UnityEngine;
using System.Collections;

public class TutorialScript : MonoBehaviour 
{
	[SerializeField]private PauseController controllerScript;

	void Update ()
	{
		if (Input.GetMouseButtonDown (0))
		{
			if (controllerScript == null)
			{
				StartLoader.Instance.TurnTutorial (false);
			}
			else
			{
				controllerScript.TurnTutorial (false);
			}
		}
	}
}
