﻿using UnityEngine;
using System.Collections;

public class BlurControl : MonoBehaviour 
{
	public float MaxBlurSize = 3f;
	public float LerpSpeed = 1f;
	public UnityStandardAssets.ImageEffects.BlurOptimized BlurInst;
	[SerializeField] MonoBehaviour[] standaloneImageEffects;


	private bool turnBlurOn = false;
	private bool turnBlurOff = false;
	private float currentLerpValue;
	private float currentBlurSize;

	void Awake ()
	{
		if (!Application.isMobilePlatform)
		{
			foreach (MonoBehaviour component in standaloneImageEffects)
			{
				component.enabled = true;
			}
		}
	}

	void LateUpdate () 
	{		
		if (turnBlurOn)
		{
			currentLerpValue += Time.unscaledDeltaTime;
			currentBlurSize = Mathf.Lerp (currentBlurSize, MaxBlurSize, currentLerpValue * LerpSpeed);

			if (currentBlurSize >= MaxBlurSize)
			{
				BlurInst.blurSize = MaxBlurSize;
				turnBlurOn = false;
			}

			BlurInst.blurSize = currentBlurSize;
		}
		else if (turnBlurOff)
		{
			currentLerpValue += Time.unscaledDeltaTime;
			currentBlurSize = Mathf.Lerp (currentBlurSize, 0f, currentLerpValue * LerpSpeed);

			if (currentBlurSize < 0.05f)
			{
				BlurInst.blurSize = 0f;
				turnBlurOff = false;
				BlurInst.enabled = false;
				this.enabled = false;
			}

			BlurInst.blurSize = currentBlurSize;
		}
	}


	public void TurnTheBlur (bool turnOn)
	{
		if (turnOn)
		{
			BlurInst.enabled = true;
			turnBlurOff = false;
			turnBlurOn = true;
		}
		else
		{
			turnBlurOn = false;
			turnBlurOff = true;
		}

//		BlurInst.enabled = turnOn;
		currentLerpValue = 0f;
	}
}
