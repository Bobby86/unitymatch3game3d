﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class PausePanelLoader : MonoBehaviour 
{
	#region References
	public Text BestScoreText;
	public GameObject GameOverPanel;
	[SerializeField]private float quitDelay;
	[SerializeField]private ImageColorAlpha sceneEffect;
	#endregion


	#region Unity Callbacks
	void OnEnable ()
	{
		BestScoreText.text = PlayerPrefs.GetInt ("BestScore").ToString ();
//		Invoke ("SetAudioIcones", 0.01f);
		SoundController.Instance.SetIcons ();
	}

	void Start ()
	{
//		Invoke ("SetAudioIcones", 0.01f);
//		SetAudioIcones ();
		GameOverPanel.SetActive (false);
		gameObject.SetActive (false);
	}
	#endregion


	#region Public Methods
	public void GameOver ()
	{
		
		if (PlayerPrefs.GetInt ("BestScore") < ScoreController.ScoreScript.GetCurrentScore ())
		{
			PlayerPrefs.SetInt ("BestScore", ScoreController.ScoreScript.GetCurrentScore ());
			PlayerPrefs.Save ();

			//todo new record message
		}

		Invoke ("TurnOnGameOverPanel", 0.01f);	
	}
	#endregion


	#region PausePanel Buttons
	public void QuitToStartMenuButton ()
	{
		// TODO save everything etc
		Time.timeScale = 1f;
		sceneEffect.FadeButton (true);
		Invoke ("LoadStartMenu", quitDelay);
	}
	#endregion

	#region Private Methods
	private void TurnOnGameOverPanel ()
	{
		GameOverPanel.SetActive (true);
	}

	private void LoadStartMenu ()
	{
		Application.LoadLevel (0);
	}
//	private void SetAudioIcones ()
//	{
//		Debug.Log (0);
//		SoundController.Instance.SetIcons ();	// set audio icons active/enactive
//	}
	#endregion
}
