﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class CountTextScript : MonoBehaviour 
{
	#region Public References
	public static int CountingTextSpeed = 10;
	public static float CountDelay;
	#endregion


	#region Private References
	private bool countIt = false;
	private int currentValue = 0;
	private int totalValue = 0;
	private int difference;
	private int checkDevision;
	private float startCountTimer;
	private Text textScript;
	#endregion


	#region Unity CallBacks
	void Start () 
	{
		textScript = GetComponent <Text> ();
	}


	void LateUpdate () 
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

		if (countIt)
		{
			if (Time.time - startCountTimer > CountDelay)
			{
				if (totalValue == currentValue)
				{
//					currentValue = totalValue;
					countIt = false;
					return;
				}

				difference = totalValue - currentValue;
				checkDevision = difference / 10000;

				if (checkDevision != 0)
				{
					currentValue += (int) (difference * 0.8f);
				}
				else 
				{
					checkDevision = difference / 1000;

					if (checkDevision != 0)
					{
						currentValue += checkDevision * CountingTextSpeed;
					}
					else 
					{
						checkDevision = difference / 100;

						if (checkDevision != 0)
						{
							currentValue += checkDevision * CountingTextSpeed;
						}
						else 
						{
							checkDevision = difference / 10;

							if (checkDevision != 0)
							{
								currentValue += checkDevision;
							}
							else 
							{
								if (difference < 0)
								{
									--currentValue;
								}
								else // here should be only (difference > 0)
								{
									++currentValue;
								}
							}
						}
					}
				}

//				if (totalValue > currentValue)
//				{
//					++currentValue;
//					textScript.text = currentValue.ToString ();
//				}
//				else if (totalValue < currentValue)
//				{
//					--currentValue;
//					textScript.text = currentValue.ToString ();
//				}
//				else
//				{
//					countIt = false;
//					return;
//				}
				textScript.text = currentValue.ToString ();
				startCountTimer = Time.time;
			}
		}
	}
	#endregion


	#region Public Methods
	public void CountValue (int value)
	{
		totalValue = value;

		if (totalValue < 0)
		{
			totalValue = 0;
		}

		if (Time.time - startCountTimer > 0.5f)
		{
			startCountTimer = Time.time;
		}

		countIt = true;
	}
	#endregion
}
