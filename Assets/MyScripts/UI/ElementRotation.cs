﻿using UnityEngine;
using System.Collections;

public class ElementRotation : MonoBehaviour 
{
	public float RotationAngleAdd = 72f;
	public float RotationTime = 0.2f;

	private bool rotate = false;
	private Quaternion targetRotation;
	private float lerpValue;
	private float step;
	private float currentRotationTime;
	private float currentRotationValue;

	void OnEnable ()
	{
		ScoreController.OnScoreAdd += AddOneRotation;
	}

	void OnDisable ()
	{
		ScoreController.OnScoreAdd -= AddOneRotation;
	}

	void Start () 
	{
		targetRotation = transform.rotation;			
		currentRotationValue = targetRotation.eulerAngles.z;
	}
	
	void LateUpdate () 
	{
		if (rotate)
		{
			lerpValue += Time.deltaTime;
			step = lerpValue / currentRotationTime;
			transform.rotation = Quaternion.Slerp (transform.rotation, targetRotation, step);

			if (transform.rotation == targetRotation)
			{
				rotate = false;
			}
			else
			{
				currentRotationTime -= Time.deltaTime;
			}
		}
	}

	public void AddOneRotation ()
	{
		currentRotationValue = (currentRotationValue + RotationAngleAdd);// % 360;
		currentRotationTime += RotationTime;
		targetRotation = Quaternion.Euler (new Vector3 (0f, 0f, currentRotationValue));

		if (!rotate)
		{
			lerpValue = 0f;
		}

		rotate = true;
	}
}
