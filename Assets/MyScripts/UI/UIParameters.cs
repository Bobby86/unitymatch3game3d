﻿using UnityEngine;
using System.Collections;

public class UIParameters : MonoBehaviour 
{
	public static UIParameters Instance;

	//axis buttons
	public float AddScaleOnSelect = 0.2f;
	public float AxisButtonsFadeDuration = 0.2f;
	public UIButtons FullButton;

	public Color FadedColor;
	public Color FullColor;

	// Floating text parameters
	public float FTDisableTime = 0.6f;
	public float FTMoveUpSpeed = 5f;
	public float FTMovementYOffset = 0.5f;
	public float FTAlphaOffTime = 0.6f;


	void Awake ()
	{
		Instance = this;
	}

	void Start ()
	{
		FloatingTextMovement.DisableObjectTime = FTDisableTime;
		FloatingTextMovement.MoveSpeed = FTMoveUpSpeed;
		FloatingTextMovement.MovementOffset = FTMovementYOffset;
		FloatingTextAlpha.AlphaOffTime = FTAlphaOffTime;

//		FloatingTextWaveScore.DisableObjectTime = FTDisableTime;
	}
}
