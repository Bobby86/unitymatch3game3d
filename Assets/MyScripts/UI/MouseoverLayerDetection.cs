﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class MouseoverLayerDetection : MonoBehaviour//, IPointerDownHandler//, IPointerEnterHandler
{
	public static int HoverAxis;
	public static int HoverLayer;

	private int axisNumber;
	private int layerNumber;
	private int buttonAxis;
	private int buttonLayer;
	private MyGameController gameController;
	private CandyPoints2DLayoutModel scriptWithCandiesOpacity;	// ExecuteCandies.cs
	private CameraRotator cameraRotatorScript;
//	private LayersButtonsLogic buttonsScript;


	void Start ()
	{
		string thisName = gameObject.name;

//		if (thisName.Contains ("Back"))
//		{
//			axisNumber = -1;
//		}
//		else 
		if (thisName.Contains ("XL"))
		{
			axisNumber = 0;
			GetLayerNumber (thisName);
		}
		else if (thisName.Contains ("YL"))
		{
			axisNumber = 1;
			GetLayerNumber (thisName);
		}
		else if (thisName.Contains ("ZL"))
		{
			axisNumber = 2;
			GetLayerNumber (thisName);
		}	
		else if (thisName.Contains ("Top"))
		{
			axisNumber = -2;
		}	
//		else if (thisName == "Abutton")
//		{
//			buttonAxis = 0;
//			buttonLayer = 0;
//			layerType = 0;
//		}	
//		else if (thisName == "Bbutton")
//		{
//			buttonAxis = 0;
//			buttonLayer = 1;
//			layerType = 0;
//		}	
//		else if (thisName == "Cbutton")
//		{
//			buttonAxis = 0;
//			buttonLayer = 2;
//			layerType = 0;
//		}	
//		else if (thisName == "Ibutton")
//		{
//			buttonAxis = 1;
//			buttonLayer = 0;
//			layerType = 1;
//		}	
//		else if (thisName == "IIbutton")
//		{
//			buttonAxis = 1;
//			buttonLayer = 1;
//			layerType = 1;
//		}	
//		else if (thisName == "IIIbutton")
//		{
//			buttonAxis = 1;
//			buttonLayer = 2;
//			layerType = 1;
//		}	
//		else if (thisName == "1button")
//		{
//			buttonAxis = 2;
//			buttonLayer = 0;
//			layerType = 2;
//		}	
//		else if (thisName == "2button")
//		{
//			buttonAxis = 2;
//			buttonLayer = 1;
//			layerType = 2;
//		}	
//		else if (thisName == "3button")
//		{
//			buttonAxis = 2;
//			buttonLayer = 2;
//			layerType = 2;
//		}	
//
//		if (thisName.Contains ("button"))
//		{
//			buttonsScript = MyGameController.gameController.gameObject.GetComponent <LayersButtonsLogic> ();
//		}

		gameController = MyGameController.gameController;
		cameraRotatorScript = GameObject.Find ("CenterObject").GetComponent <CameraRotator> ();
		scriptWithCandiesOpacity = gameController.gameObject.GetComponent <CandyPoints2DLayoutModel> ();
	}


	private void GetLayerNumber (string name)
	{
		if (name.Contains ("0"))
		{
			layerNumber = 0;
		}
		else if (name.Contains ("1"))
		{
			layerNumber = 1;
		}
		else if (name.Contains ("2"))
		{
			layerNumber = 2;
		}
	}


	void OnMouseEnter ()	
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

		bool axisChange = false;
		MouseoverLayerDetection.HoverLayer = layerNumber;

		if (MouseoverLayerDetection.HoverAxis != axisNumber)
		{
			MouseoverLayerDetection.HoverAxis = axisNumber;
			axisChange = true;
		}

		if (DragScript.readInput)	// warning !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		{
//			if (axisChange)
//			{
			DragScript.DragLayer = axisNumber;
//			}

			switch (axisNumber)
			{
			case 0:
				CandyPoints2DLayoutModel.X1CurrentLayer = layerNumber;
//				MyInputManager.ReadInput = false;
				break;

			case 1:
				CandyPoints2DLayoutModel.YICurrentLayer = layerNumber;
//				MyInputManager.ReadInput = false;
				break;

			case 2:
				CandyPoints2DLayoutModel.ZACurrentLayer = layerNumber;
//				MyInputManager.ReadInput = false;
				break;

//			case -2:
//				MyInputManager.ReadInput = true;
//				break;

			default:
				break;
			}

//			if (axisChange)
//			{
//				cameraRotatorScript.RotateCamera (axisNumber, layerNumber);
//			}
		}
	}

	// for buttons
//	public void OnPointerEnter (PointerEventData eventData)
//	{
//		if (DragScript.readInput)
//		{
//			if (gameObject.name.Contains ("button"))
//			{
////				gameController.TurnOnCube (layerType);
//				scriptWithCandiesOpacity.ChangeLayersOpacity (layerType, scriptWithCandiesOpacity.GetCurrentLayerByAxis (layerType));
//			}
//		}
//	}


//	public void OnPointerDown (PointerEventData eventData)
//	{
//		if (DragScript.readInput)
//		{
//			if (gameObject.name.Contains ("button"))
//			{
////				gameController.TurnOnButton (buttonAxis, buttonLayer);
//				ChangeLayerView ();
////				scriptWithCandiesOpacity.ChangeLayersOpacity (buttonAxis, buttonLayer);
//			}
//		}
//	}


//	private void ChangeLayerView ()
//	{
//		switch (buttonAxis)
//		{
//		case 0:
//			buttonsScript.XLayerJump (buttonLayer);
//			break;
//
//		case 1:
//			buttonsScript.YLayerJump (buttonLayer);
//			break;
//
//		case 2:
//			buttonsScript.ZLayerJump (buttonLayer);
//			break;
//		}
//	}
}
