﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class FloatingTextMovement : MonoBehaviour 
{
	public static float DisableObjectTime;
	public static float MoveSpeed;
	public static float MovementOffset;	// target

	private bool move = false;
	private float startTime;
	private FloatingTextAlpha ftAlphaScript;
	private Vector3 startPosition;
	private Vector3 targetPosition;


	void Start () 
	{
		startPosition = transform.position;
		ftAlphaScript = GetComponentInChildren <FloatingTextAlpha> ();
	}
	

	void Update () 
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

		if (move)
		{
			if (Time.time - startTime > DisableObjectTime)
			{
				move = false;
				transform.position = startPosition;
				gameObject.SetActive (false);
				return;
//				this.enabled = false;
			}

			transform.position = Vector3.MoveTowards (transform.position, targetPosition, MoveSpeed * Time.deltaTime);
		}
	}

	// axis codes: x - 0, y - 1, z -2
	public void StartMove (int axisCode)
	{
		switch (axisCode)
		{
		case 0:
			targetPosition = transform.position + new Vector3 (0f, MovementOffset, 0f);
			break;

		case 1:
			targetPosition = transform.position + new Vector3 (0f, 0f, MovementOffset);
			break;

		case 2:
			targetPosition = transform.position + new Vector3 (0f, MovementOffset, 0f);
			break;
		}

		startTime = Time.time;

		if (ftAlphaScript == null)
		{
			startPosition = transform.position;
			ftAlphaScript = GetComponentInChildren <FloatingTextAlpha> ();
		}

//		ftAlphaScript.enabled = true;
		ftAlphaScript.TextAlphaOff ();
		move = true;
	}
}
