﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class FloatingTextWaveScore : MonoBehaviour 
{
//	public static float DisableObjectTime;
	public float DisableObjectTime;
	public float MoveSpeed;
	public float MovementYOffset;	// target

	private bool move = false;
	private float startTime;
	private Text ftAlphaText;
	private FloatingTextAlpha ftAlphaScript;
	private Vector3 startPosition;
	private Vector3 targetPosition;


	void Start () 
	{
		startPosition = transform.position;
		targetPosition = startPosition + new Vector3(0f, MovementYOffset, 0f);
		ftAlphaScript = GetComponentInChildren <FloatingTextAlpha> ();
		ftAlphaText = ftAlphaScript.GetComponent <Text> ();
	}
	

	void Update () 
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

		if (move)
		{
			if (Time.time - startTime > DisableObjectTime)
			{
				move = false;
				transform.position = startPosition;
				gameObject.SetActive (false);
				return;
//				this.enabled = false;
			}

			transform.position = Vector3.MoveTowards (transform.position, targetPosition, MoveSpeed * Time.deltaTime);
		}
	}

	// axis codes: x - 0, y - 1, z -2
	public void StartMove (int waveScore)
	{
		startTime = Time.time;

		if (ftAlphaScript == null)
		{
			startPosition = transform.position;
			ftAlphaScript = GetComponentInChildren <FloatingTextAlpha> ();
			ftAlphaText = ftAlphaScript.GetComponent <Text> ();
		}

//		if (move)
//		{
//		}

//		ftAlphaScript.enabled = true;
		ftAlphaText.text = "+" + waveScore.ToString ();
		ftAlphaScript.TextAlphaOff ();
		move = true;
	}
}
