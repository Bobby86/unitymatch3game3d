﻿using UnityEngine;
using System.Collections;


public class ReplayButton : MonoBehaviour 
{
	[SerializeField]private float startGameDelay;
	[SerializeField]private ImageColorAlpha sceneEffect;

	public void ReplayClick ()
	{
		Time.timeScale = 1f;

		sceneEffect.FadeButton (true);
		Invoke ("RestartGame", startGameDelay);
	}

	private void RestartGame ()
	{
		Application.LoadLevel (Application.loadedLevel);
	}
}
