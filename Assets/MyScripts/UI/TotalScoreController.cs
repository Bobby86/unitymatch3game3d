﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class TotalScoreController : MonoBehaviour 
{
	public int TotalScoreCountSpeed = 10;
	public FloatingTextWaveScore[] FloatingWaveScoreTextMovement;

	private int currentFTWSindex = 0;
	private int currentScore = 0;
	private int totalScore = 0;
	private int checkDevision;
	private int currentCountSpeedLessHundred;
	private int currentCountSpeedLessTen;
	private Text totalScoreText;
//	private Text[] floatingWaveScoreText;


	void Start () 
	{
		totalScoreText = GetComponent <Text> ();
//		floatingWaveScoreText = new Text[FloatingWaveScoreTextMovement.Length];

		for (int i = 0; i < FloatingWaveScoreTextMovement.Length; ++i)
		{
//			floatingWaveScoreText[i] = GetComponentInChildren <Text> ();
//			floatingWaveScoreText[i].text = "";
			FloatingWaveScoreTextMovement[i].gameObject.SetActive (false);
		}
//		FloatingWaveScoreTextMovementFront = GetComponentInChildren <FloatingTextWaveScore> ();

		totalScoreText.text = "0";
		currentCountSpeedLessHundred = TotalScoreCountSpeed / 10;
		currentCountSpeedLessTen = currentCountSpeedLessHundred / 10;
	}


	void LateUpdate ()
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

		if (totalScore != currentScore)
		{
			checkDevision = (totalScore - currentScore) / 10000; // for any score > 100k

			if (checkDevision > 0)
			{
				currentScore += (int)((totalScore - currentScore) * 0.8f);
			}
			else 
			{
				checkDevision = (totalScore - currentScore) / 1000; // for any score > 10k etc.

				if (checkDevision > 0)
				{
					currentScore += checkDevision * TotalScoreCountSpeed;
				}
				else 
				{
					checkDevision = (totalScore - currentScore) / 100;

					if (checkDevision > 0)
					{
						currentScore += checkDevision * TotalScoreCountSpeed;
					}
					else 
					{
						checkDevision = (totalScore - currentScore) / 10;

						if (checkDevision > 0)
						{
							currentScore += checkDevision;// * currentCountSpeedLessHundred;
						}
						else 
						{
							checkDevision = totalScore - currentScore;

							if (checkDevision > 0)
							{
								currentScore += 1;
							}
						}
					}
				}
			}

			totalScoreText.text = currentScore.ToString ();
		}
	}


	public void AddScore (int waveScore)
	{
//		return;	// INDEV

		totalScore += waveScore;
//		floatingWaveScoreText[currentFTWSindex].text = "+" + waveScore.ToString ();
		FloatingWaveScoreTextMovement[currentFTWSindex].gameObject.SetActive (true);
		FloatingWaveScoreTextMovement[currentFTWSindex].StartMove (waveScore);
		currentFTWSindex = (currentFTWSindex + 1) % FloatingWaveScoreTextMovement.Length;
	}
}
