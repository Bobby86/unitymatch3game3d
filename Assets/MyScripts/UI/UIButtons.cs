﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIButtons : MonoBehaviour 
{
	#region Private References
//	private static UIButtons axisSelected;	// currently pressed buttun with full opacity
	private static UIButtons layerSelected;

	private int axis;
	private int layer;
	private LayersOpacityController opacityScript;

	private bool changeColor = false;
	private float lerpValue;
	private float fadeDuration;
	private float step;
	private Color startColor;
	private Color fullColor;
	private Color targetColor;
	private Vector3 addScaleOnSelect;
	private Image buttonImgRef;
	private UIButtons fullButton;
	private UIButtons parentUIButtons;
	private UIButtons[] childrenUIButtons;
	#endregion


	#region Unity Callbacks
	void Start () 
	{
		fullButton = UIParameters.Instance.FullButton;
		fadeDuration = UIParameters.Instance.AxisButtonsFadeDuration;
		addScaleOnSelect = new Vector3 (UIParameters.Instance.AddScaleOnSelect, UIParameters.Instance.AddScaleOnSelect, 0f);
		buttonImgRef = GetComponent <Image> ();
		startColor = buttonImgRef.color;
		fullColor = startColor;
		fullColor.a = 1f;
//		axisSelected = null;
		layerSelected = null;
		opacityScript = LayersOpacityController.Instance;
		GetButtonType ();
	}

	void LateUpdate ()
	{
		if (changeColor)
		{
			lerpValue += Time.deltaTime;
			step = lerpValue / fadeDuration;
			buttonImgRef.color = Color.Lerp (buttonImgRef.color, targetColor, step);

			if (buttonImgRef.color.a == targetColor.a)
			{
				changeColor = false;
			}
		}
	}
	#endregion


	#region Public Methods
	public void ButtonDown ()
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

//		Debug.Log (axis + " " + layer);
//		if (layerSelected != null &&  axis != layerSelected.GetAxis ())
//		{
//			opacityScript.SetLayersOpacity (axis, layer);
//		}

		SelectButton ();
//		switch (axis)
//		{
//		case 0:
//			opacityScript.ChangeXLayerOpacity (layer);
//			break;
//
//		case 1:
//			opacityScript.ChangeYLayerOpacity (layer);
//			break;
//
//		case 2:
//			opacityScript.ChangeZLayerOpacity (layer);
//			break;
//		}
	}

	private void SelectButton ()
	{
//		if (this == axisSelected)
//		{
//			if (layerSelected != null)
//			{				
//				layerSelected.UnselectButton ();
//				layerSelected = null;
//			}
//
//			this.UnselectButton ();
//			axisSelected = fullButton;
//			fullButton.PSelectButton ();
//			opacityScript.SetLayersOpacity (-1);	// set to full
//		}
//		else 
//		if (this.layer < 0)	// axis button tapped
//		{
//			//todo replace with image fade
////			if (axisSelected != null)
////			{
////				axisSelected.UnselectButton ();
////			}
//
//
//			if (layerSelected != null)
//			{
//				layerSelected.UnselectButton ();
//			}
//
//			this.PSelectButton ();
////			axisSelected = this;
//
//			//select layer
//			switch (axis)
//			{
//			case -1:
//				break;
//
//			case 0:
//				childrenUIButtons[opacityScript.ZLayerIndexWithFullOpacity].PSelectButton ();
//				layerSelected = childrenUIButtons[opacityScript.ZLayerIndexWithFullOpacity];
//				break;
//
//			case 1:
//				childrenUIButtons[opacityScript.YLayerIndexWithFullOpacity].PSelectButton ();
//				layerSelected = childrenUIButtons[opacityScript.YLayerIndexWithFullOpacity];
//				break;
//
//			case 2:
//				childrenUIButtons[opacityScript.XLayerIndexWithFullOpacity].PSelectButton ();
//				layerSelected = childrenUIButtons[opacityScript.XLayerIndexWithFullOpacity];
//				break;
//			}
//		}
//		else 
		if (axis < 0)	// full button
		{
			if (layerSelected != null)
			{
				layerSelected.UnselectButton (true);
				layerSelected = null;
				PSelectButton ();
				opacityScript.SetLayersOpacity (-1);	// set to full
			}
		}
		else if (this != layerSelected)
		{
			if (layerSelected != null)
			{
					layerSelected.UnselectButton (true);
					this.PSelectButton (true);
			}
			else
			{
				fullButton.UnselectButton ();
				this.PSelectButton (true);
			}

//			if (axisSelected != null)
//			{
//				if (axisSelected.GetAxis () != axis)
//				{
//					axisSelected.UnselectButton ();
//
//					if (parentUIButtons != null)
//					{
//						axisSelected = parentUIButtons;
//						parentUIButtons.PSelectButton ();
//					}
//				}
//			}
//			else
//			{
//				if (parentUIButtons != null)
//				{
//					axisSelected = parentUIButtons;
//					parentUIButtons.PSelectButton ();
//				}
//			}

//			if (sameAxis)
//			{
//				this.PSelectButton (false);
//			}
//			else
//			{
//				this.PSelectButton (true);
//			}			

			opacityScript.SetLayersOpacity (axis, layer);	// set to full
			layerSelected = this;
		}
		else
		{
			layerSelected = null;
			this.UnselectButton (true);
			fullButton.PSelectButton ();
			opacityScript.SetLayersOpacity (-1);	// set to full
		}
	}

	public void PSelectButton (bool selectAxis = false)
	{
		targetColor = fullColor;

		if (!changeColor)
		{
			lerpValue = 0f;
			changeColor = true;
		}

		transform.localScale += addScaleOnSelect;

		if (selectAxis)
		{
			GetComponentInParent <ImageColorAlpha> ().FadeButton (false);	// reversed
		}
	}

	public void UnselectButton (bool unselectAxis = false)
	{
		targetColor = startColor;

		if (!changeColor)
		{
			lerpValue = 0f;
			changeColor = true;
		}

		transform.localScale -= addScaleOnSelect;

		if (unselectAxis)
		{
			GetComponentInParent <ImageColorAlpha> ().FadeButton (true);	// reversed
		}
	}

	public int GetAxis ()
	{
		return axis;
	}
	#endregion


	#region Private Methods
	// get and set the axis and layer numbers
	private void GetButtonType ()
	{
		string name = gameObject.name;

		if (name.Contains ("F"))	// X Axis big button
		{
			axis = -1;
			layer = -1;
//			axisSelected = this;
			addScaleOnSelect.Set (addScaleOnSelect.x / 2, addScaleOnSelect.y / 2, 0f);

			PSelectButton ();
		}
		else if (name.Contains ("XA"))	// X Axis big button
		{
			axis = 0;
			layer = -1;
			InitChildren ();
		}
		else if (name.Contains ("YA"))	// Y Axis big button
		{
			axis = 1;
			layer = -1;
			InitChildren ();
		}
		else if (name.Contains ("ZA"))	// Z Axis big button
		{
			axis = 2;
			layer = -1;
			InitChildren ();
		}
		// small layers buttons
		else if (name.Contains ("X"))
		{
			axis = 0;
		}
		else if (name.Contains ("Y"))
		{
			axis = 1;
			// debug
//			if (name.Contains ("1"))
//			{
//				layer = 2;
//			}
//			else if (name.Contains ("2"))
//			{
//				layer = 1;
//			}
//			else if (name.Contains ("3"))
//			{
//				layer = 0;
//			}
//			return;
		}
		else if (name.Contains ("Z"))
		{
			axis = 2;
		}

		if (name.Contains ("1"))
		{
			layer = 0;
			parentUIButtons = transform.parent.GetComponent <UIButtons> ();
		}
		else if (name.Contains ("2"))
		{
			layer = 1;
			parentUIButtons = transform.parent.GetComponent <UIButtons> ();
		}
		else if (name.Contains ("3"))
		{
			layer = 2;
			parentUIButtons = transform.parent.GetComponent <UIButtons> ();
		}
	}

	private void InitChildren ()
	{
		var _childrenUIButtons = GetComponentsInChildren <UIButtons> ();
		childrenUIButtons = new UIButtons[3];	// number of layers

		for (int i = 0; i < childrenUIButtons.Length; ++i)
		{
			childrenUIButtons[i] = _childrenUIButtons[i + 1];
		}

//		foreach (UIButtons child in childrenUIButtons)
//		{
//			Debug.Log (child.gameObject.name);
//		}
	}
	#endregion
}
