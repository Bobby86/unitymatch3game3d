﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class FloatingTextAlpha : MonoBehaviour 
{
	public static float AlphaOffTime;

	private bool changeAlpha = false;
	private float currentLerpTime;
	private float colorLerpStep;
	private Color startColor;
	private Color transparentColor;
	private Text textScript;


	void Start () 
	{
		textScript = GetComponent <Text> ();
		startColor = textScript.color;
		transparentColor = textScript.color;
		transparentColor.a = 0f;
	}
	

	void LateUpdate () 
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

		if (changeAlpha)
		{
			currentLerpTime += Time.deltaTime;

			if (currentLerpTime > AlphaOffTime)
			{
				changeAlpha = false;
//				textScript.enabled = false;
//				this.enabled = false;
			}

			colorLerpStep = currentLerpTime / AlphaOffTime;
//			colorLerpStep = 1f - Mathf.Cos (colorLerpStep * Mathf.PI); 
			colorLerpStep *= colorLerpStep;
			textScript.color = Color.Lerp (textScript.color, transparentColor, colorLerpStep);
		}
	}


	public void TextAlphaOff ()
	{
		if (textScript == null)
		{
			textScript = GetComponent <Text> ();
			startColor = textScript.color;
			transparentColor = textScript.color;
			transparentColor.a = 0f;
		}

		currentLerpTime = 0;
//		textScript.enabled = true;
		textScript.color = startColor;
		changeAlpha = true;
	}


//	public void TextDisable ()
//	{
//		changeAlpha = false;
//		textScript.enabled = false;
//		this.enabled = false;
//	}
}
