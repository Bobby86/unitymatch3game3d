﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Buttons images and 3d-view images controller, it is changing active/enactive pictures
public class ImageController : MonoBehaviour 
{
	public Sprite inactive;
	public Sprite active;
	public Vector3 AddButtonScale;

	private Vector3 normalButtonScale;
	private RectTransform thisTransform;
	private Image thisImage;


	void Start ()
	{		
		thisImage = GetComponent <Image> ();
		thisTransform = GetComponent <RectTransform> ();
		normalButtonScale = thisTransform.localScale;
	}


	public void SetActiveImage (bool setActive)
	{
		if (setActive)
		{
			thisImage.sprite = active;
			thisTransform.localScale += AddButtonScale;
		}	
		else
		{
			thisImage.sprite = inactive;
			thisTransform.localScale = normalButtonScale;
		}
	}
}
