﻿using UnityEngine;
using System.Collections;

public class DestroyParticles : MonoBehaviour 
{
	void Start () 
	{
		Destroy (gameObject, GetComponent <ParticleSystem> ().duration);
	}
}
