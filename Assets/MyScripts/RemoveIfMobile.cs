﻿using UnityEngine;
using System.Collections;

public class RemoveIfMobile : MonoBehaviour 
{
	void Awake ()
	{
		if (Application.isMobilePlatform)
		{
			Destroy (gameObject);
		}
	}
}
