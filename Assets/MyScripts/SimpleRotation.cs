﻿using UnityEngine;
using System.Collections;

public class SimpleRotation : MonoBehaviour 
{
	public float RotationSpeed = 30f;


	void LateUpdate ()
	{
		if (MyGameController.GState == GameState.Pause)
		{
			return;
		}

		// rotation
		transform.Rotate (Vector3.up * Time.smoothDeltaTime * RotationSpeed);
	}
}
